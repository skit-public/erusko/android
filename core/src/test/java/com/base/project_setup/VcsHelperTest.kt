package com.base.project_setup

import base.base_app.core.helpers.VcsHelper
import base.base_app.core.helpers.Version
import org.junit.Test

class VcsHelperTest {

    @Test
    fun testVersionParser() {
        val versionsTestMap: Map<String, Version> = mapOf(
                "1.0.0" to Version(1, 0, 0),
                "1.5.10" to Version(1, 5, 10),
                "111.011.011" to Version(111, 11, 11),
                "02.07.1" to Version(2, 7, 1)
        )

        versionsTestMap.forEach { (key, value) ->
            assert(VcsHelper.parseVersion(key) == value)
        }
    }

    @Test
    fun testNewVersionAvailable() {
        val versionsTestMap: Map<Version, Version> = mapOf(
                Version(1, 0, 0) to Version(1, 1, 0),
                Version(1, 0, 0) to Version(1, 0, 1),
                Version(1, 1, 1) to Version(1, 1, 2),
                Version(1, 1, 1) to Version(1, 2, 0),
                Version(0, 1, 0) to Version(1, 0, 1)
        )

        versionsTestMap.forEach { (installedVersion, latestVersion) ->
            assert(VcsHelper.isNewVersionAvailable(installedVersion, latestVersion))
        }
    }

    @Test
    fun testVersionsEquals() {
        val versionsTestMap: Map<Version?, Version?> = mapOf(
                Version(1, 0, 0) to Version(1, 0, 0),
                Version(1, 1, 0) to Version(1, 1, 0),
                Version(1, 0, 1) to Version(1, 0, 1),

                Version(0, 1, 1) to Version(0, 1, 1),
                Version(0, 0, 1) to Version(0, 0, 1),
                Version(0, 0, 0) to Version(0, 0, 0),

                null to Version(0, 0, 0),
                Version(0, 0, 0) to null,
                null to null
        )

        versionsTestMap.forEach { (installedVersion, latestVersion) ->
            val result = VcsHelper.isNewVersionAvailable(installedVersion, latestVersion)
            if (result) {
                println("Test Failed: $installedVersion $latestVersion")
            }
            assert(!result)
        }
    }

    @Test
    fun testNewVersionNotAvailable() {
        val versionsTestMap: Map<Version?, Version?> = mapOf(
                Version(1, 0, 0) to Version(0, 1, 0),
                Version(1, 0, 0) to Version(0, 1, 1),
                Version(1, 0, 0) to Version(0, 10, 50),
                Version(1, 0, 0) to Version(1, 0, 0),
                Version(1, 0, 0) to Version(0, 10000, 100000),
                Version(1, 10000, 0) to Version(0, 10000, 100000),
                Version(1, 10000, 0) to Version(1, 100, 100),

                null to Version(0, 0, 0),
                Version(0, 0, 0) to null,
                null to null
        )

        versionsTestMap.forEach { (installedVersion, latestVersion) ->
            val result = VcsHelper.isNewVersionAvailable(installedVersion, latestVersion)
            if (result) {
                println("Test Failed: $installedVersion $latestVersion")
            }
            assert(!result)
        }
    }

}

