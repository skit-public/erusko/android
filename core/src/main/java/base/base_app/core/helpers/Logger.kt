package base.base_app.core.helpers

import base.base_app.core.BuildConfig
import com.pandulapeter.beagle.Beagle
import timber.log.Timber

object Logger {

    fun init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(object : Timber.DebugTree() {
                override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                    super.log(priority, tag, message, t)
                    Beagle.log("[$tag] $message", "Timber", t?.stackTraceToString())
                }

                override fun createStackElementTag(element: StackTraceElement): String? {
                    with(element) {
                        return "($fileName:$lineNumber)"
                    }
                }
            })
        }
    }

    inline fun em(message: () -> String): Logger {
        CrashlyticsHelper.logMessage(message())
        Timber.e(message())
        return this
    }

    inline fun et(throwable: () -> Throwable): Logger {
        CrashlyticsHelper.logCustomCrash(throwable())
        Timber.e(throwable())
        return this
    }

    inline fun dm(message: () -> String): Logger {
        Timber.d(message())
        return this
    }

    inline fun dt(throwable: () -> Throwable): Logger {
        Timber.d(throwable())
        return this
    }
}