package base.base_app.core.helpers.app_helper

import base.base_app.core.entities.config.static_config.ConfigProvider
import com.pandulapeter.beagle.common.configuration.Text
import com.pandulapeter.beagle.common.contracts.BeagleListItemContract

data class AppHelperEnvironment(
        override val title: Text,
        val environment: ConfigProvider.Environment
) : BeagleListItemContract

data class SimpleItem(
        override val title: Text,
        val message: String
) : BeagleListItemContract