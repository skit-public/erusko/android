package base.base_app.core.helpers.app_helper

import android.app.Application
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import base.base_app.core.BuildConfig
import base.base_app.core.R
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.helpers.Logger
import com.google.gson.GsonBuilder
import com.pandulapeter.beagle.Beagle
import com.pandulapeter.beagle.common.configuration.Behavior
import com.pandulapeter.beagle.common.configuration.Text
import com.pandulapeter.beagle.common.configuration.toText
import com.pandulapeter.beagle.log.BeagleLogger
import com.pandulapeter.beagle.modules.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonObject
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

/**
 *  list of available modules
 *  https://github.com/pandulapeter/beagle/tree/master/common/src/main/java/com/pandulapeter/beagle/modules
 */

class AppHelper @Inject constructor(
        private val configProvider: Provider<ConfigProvider>,
        private val persistentAppPreferences: Provider<PersistentAppPreferences>
) {

    companion object {
        const val ANALYTICS_LABEL = "analytics_tracker"
        const val VERIFICATION_CODES_LABEL = "verification_tracker"
    }

    fun addAnalyticsLog(log: String) {
        if (!BuildConfig.DEBUG) {
            return
        }

        Beagle.log(log, ANALYTICS_LABEL)
    }

    fun setupAppHelper(application: Application) {
        if (!BuildConfig.DEBUG) {
            return
        }

        Beagle.initialize(application, behavior = Behavior(
                logBehavior = Behavior.LogBehavior(
                        loggers = listOf(BeagleLogger),
                )
        ))
        Beagle.set(
                HeaderModule(
                        title = R.string.app_name.toText(),
                        subtitle = "${application.packageManager.getPackageInfo(application.packageName, 0).versionName}  (${application.packageManager.getPackageInfo(application.packageName, 0).versionCode})".toText(),
                        text = "${application.packageName}\n${BuildConfig.BUILD_TYPE}".toText()
                ),
                SingleSelectionListModule(
                        title = "Environment",
                        initiallySelectedItemId = persistentAppPreferences.get().environment?.title
                                ?: ConfigProvider.Environment.getDefault().title,
                        items = listOf(
                                persistentAppPreferences.get().environment
                                        ?: ConfigProvider.Environment.getDefault()
                        ).map {
                            AppHelperEnvironment(Text.CharSequence(it.title), it)
                        },
                        onSelectionChanged = {
                            it?.let { env ->
                                configProvider.get().switchEnvironment(env.environment)
                                persistentAppPreferences.get().environment = env.environment
                                showResetAppToast(application)
                            } ?: Logger.em { "Environment $it not found" }
                        }
                ),
                DividerModule(),

                TextModule("Tools", type = TextModule.Type.SECTION_HEADER),
                BugReportButtonModule(),
                KeylineOverlaySwitchModule(),
                SwitchModule("Show debug worker notifications", initialValue = persistentAppPreferences.get().showWorkerDebugNotifications, onValueChanged = {
                    persistentAppPreferences.get().showWorkerDebugNotifications = it
                }),
                SwitchModule("Show String Keys", initialValue = persistentAppPreferences.get().showStringKeysInsteadOfValues, onValueChanged = {
                    showResetAppToast(application)
                    persistentAppPreferences.get().showStringKeysInsteadOfValues = it
                }),
                AnimationDurationSwitchModule(),
                LogListModule(),
                LogListModule("Analytics".toText(), label = ANALYTICS_LABEL),
                ScreenCaptureToolboxModule(),
                PaddingModule(),
                ItemListModule("Config", getConfigItems(), onItemSelected = { item ->
                    getConfigItems().firstOrNull {
                        it.title == item.title
                    }?.let { currentItem ->
                        Beagle.currentActivity?.let {
                            AlertDialog.Builder(it)
                                    .setTitle(currentItem.title.toString())
                                    .setMessage(currentItem.message)
                                    .setPositiveButton("Close") { dialog, _ -> dialog.dismiss() }
                                    .show()
                        }
                    }
                }),
                DividerModule(),

                TextModule("Verification code", type = TextModule.Type.SECTION_HEADER),
                TextModule("Generate", type = TextModule.Type.BUTTON, onItemSelected = {
                    generateCode(application)
                }),
                LogListModule("Verification codes".toText(), label = VERIFICATION_CODES_LABEL),
                DividerModule(),

                TextModule("Info", type = TextModule.Type.SECTION_HEADER),
                DeviceInfoModule(),
                PaddingModule(),
                AppInfoButtonModule(),
                DeveloperOptionsButtonModule(),
                ForceCrashButtonModule(type = TextModule.Type.BUTTON)
        )
    }

    private fun showResetAppToast(application: Application) {
        Toast.makeText(application, "Reštartujte aplikaciu aby sa prejavili všetky zmeny", Toast.LENGTH_LONG).show()
    }

    private fun getConfigItems(): List<SimpleItem> {
        val gson = GsonBuilder().serializeNulls().setPrettyPrinting().create()
        val config = configProvider.get().provide()
        return config.javaClass.declaredFields.mapNotNull { field ->
            try {
                field.isAccessible = true

                val fieldValue = field.get(config)

                val internalValue = (fieldValue as? () -> Any?)?.invoke()

                val realValue = (internalValue ?: fieldValue)?.let {
                    gson.toJson(it)
                }

                realValue?.let {
                    SimpleItem(title = field.name.toText(), message = it)
                }
            } catch (e: Exception) {
                Logger.em { "Cannot convert to json: ${field.name} , ${e.localizedMessage}" }
                null
            }
        }
    }

    private fun generateCode(application: Application) {
        GlobalScope.launch(context = Dispatchers.IO) {
            var connection: HttpURLConnection? = null
            try {
                connection = URL(configProvider.get().provide().exposuresServerConfig().verificationCodeGenerationUrl).openConnection() as HttpURLConnection
                connection.requestMethod = "POST"
                connection.addRequestProperty("Accept", "application/json")
                connection.addRequestProperty("Content-Type", "application/json")
                connection.addRequestProperty("X-Api-Key", configProvider.get().provide().exposuresServerConfig().verificationCodeGeneratorApiKey)
                val symptomsDate = LocalDate.now().minusDays(3).format(DateTimeFormatter.ofPattern("YYYY-MM-dd"))
                val testDate = LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("YYYY-MM-dd"))
                val data = "{\"symptomDate\": \"$symptomsDate\",\"testDate\": \"$testDate\",\"testType\": \"confirmed\",\"tzOffset\": 0}"
                connection.doInput = true
                connection.doOutput = true

                connection.outputStream.use { os ->
                    val input: ByteArray = data.toByteArray(Charsets.UTF_8)
                    os.write(input, 0, input.size)
                }

                connection.inputStream.use {
                    val scanner = Scanner(it)
                    scanner.useDelimiter("\\A")
                    val hasInput: Boolean = scanner.hasNext()
                    if (hasInput) {
                        val response = scanner.next()
                        Logger.dm { response }
                        Json.parseToJsonElement(response).jsonObject.let {
                            Beagle.log("code: ${it["code"]}\nexpiresAt:${it["expiresAt"]}", VERIFICATION_CODES_LABEL)
                            it["code"]?.let {
                                val formattedCode = it.toString().replace("\"", "")
                                (application.getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager)?.setPrimaryClip(ClipData.newPlainText("Exposure sms code", formattedCode))
                                withContext(Dispatchers.Main) {
                                    Toast.makeText(application, "$formattedCode coppied to clipboad!", Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                Logger.et { e }
            } finally {
                connection?.disconnect()
            }
        }
    }
}