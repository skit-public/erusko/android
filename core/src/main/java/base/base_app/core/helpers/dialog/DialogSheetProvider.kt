package base.base_app.core.helpers.dialog

import android.view.View
import androidx.annotation.LayoutRes
import base.base_app.core.R

interface DialogSheetProvider {
    fun provideDialogSheet(@LayoutRes layoutResId: Int = R.layout.bottom_sheet_dialog): View?
    fun provideBottomInset(): Int?
}