package base.base_app.core.helpers

import com.google.firebase.crashlytics.FirebaseCrashlytics

object CrashlyticsHelper {

    fun logCustomCrash(t: Throwable) {
        ignoreExceptions {
            FirebaseCrashlytics.getInstance().recordException(t)
        }
    }

    fun logMessage(message: String) {
        ignoreExceptions {
            FirebaseCrashlytics.getInstance().log(message)
        }
    }

}