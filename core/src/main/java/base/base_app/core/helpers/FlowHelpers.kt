package base.base_app.core.helpers

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

fun <T> Flow<T>?.renderViewState(scope: LifecycleCoroutineScope, onNext: (T) -> Unit = {}) {
    this?.let { stream ->
        scope.launch {
            try {
                stream.collect {
                    onNext(it)
                }
            } catch (e: Throwable) {
                if (e !is CancellationException) {
                    Logger.et { e }
                }
            }
        }
    }
}

fun <T> Flow<T>.throttleFirst(windowDurationMillis: Long): Flow<T> = flow {
    var lastEmissionTime = 0L
    collect { upstream ->
        val currentTime = System.currentTimeMillis()
        val mayEmit = currentTime - lastEmissionTime > windowDurationMillis
        if (mayEmit) {
            lastEmissionTime = currentTime
            emit(upstream)
        }
    }
}