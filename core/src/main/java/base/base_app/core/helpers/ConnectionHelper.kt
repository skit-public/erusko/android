package base.base_app.core.helpers

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

object ConnectionHelper {

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager?
        return if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.let { capabilities ->
                    when {
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN) -> true
                        else -> false
                    }
                } ?: false
            } else {
                ignoreExceptions {
                    val activeNetworkInfo = connectivityManager.activeNetworkInfo
                    activeNetworkInfo != null && activeNetworkInfo.isConnected
                } ?: false
            }
        } else {
            false
        }
    }

}