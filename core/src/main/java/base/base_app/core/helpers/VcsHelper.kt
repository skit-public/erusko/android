package base.base_app.core.helpers

import android.content.Context
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("version")
data class Version(
        @SerialName("major") val major: Int,
        @SerialName("minor") val minor: Int,
        @SerialName("hotfix") val hotfix: Int
) {
    fun toVersionString() = "$major.$minor.$hotfix"
}

object VcsHelper {

    fun getInstalledAppVersion(context: Context): Version? {
        return context.packageManager.getPackageInfo(context.packageName, 0).versionName.let { appVersion ->
            parseVersion(appVersion)
        }
    }

    fun parseVersion(version: String): Version? {
        val versionNumbers = version.split(".", ignoreCase = true)
                .mapNotNull {
                    it.toIntOrNull()
                }
        return if (versionNumbers.size >= 3) {
            Version(versionNumbers[0], versionNumbers[1], versionNumbers[2])
        } else {
            null
        }
    }

    fun isNewVersionAvailable(installedVersion: Version?, latestVersion: Version?): Boolean {
        return if (installedVersion == null || latestVersion == null) {
            false
        } else {
            when {
                latestVersion.major > installedVersion.major -> true
                latestVersion.minor > installedVersion.minor && latestVersion.major == installedVersion.major -> true
                latestVersion.hotfix > installedVersion.hotfix && latestVersion.major == installedVersion.major && latestVersion.minor == installedVersion.minor -> true
                else -> false
            }
        }
    }

}