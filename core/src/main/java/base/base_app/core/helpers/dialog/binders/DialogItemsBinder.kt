package base.base_app.core.helpers.dialog.binders

import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import base.base_app.core.R
import base.base_app.core.helpers.StringResource
import base.base_app.core.helpers.getString
import base.base_app.core.ui.EmptyBinderHolder
import base.base_app.core.ui.show


class DialogItemsBinder : EmptyBinderHolder<DialogItem>(R.layout.item_main_symptoms, DialogItem::class.java) {
    override fun bind(position: Int, item: DialogItem) {
        super.bind(position, item)
        itemView.apply {
            findViewById<ImageView>(R.id.iv_symptoms)?.let { iv ->
                item.imageRes?.let {
                    iv.setImageResource(it)
                }
                iv.show(item.imageRes != null)
            }

            findViewById<TextView>(R.id.tv_main_symptoms)?.setText(item.title.getString(itemView.context))
        }
    }
}

data class DialogItem(@DrawableRes val imageRes: Int?, val title: StringResource)

