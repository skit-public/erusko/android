package base.base_app.core.helpers

import android.content.Context
import androidx.annotation.StringRes
import java.io.Serializable

sealed class StringResource : Serializable {
    class StringIdResource(@StringRes val stringId: Int, vararg val params: Any) : StringResource() {
        override fun hashCode(): Int {
            var result = stringId
            result = 31 * result + params.contentHashCode()
            return result
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as StringIdResource

            if (stringId != other.stringId) return false
            if (!params.contentEquals(other.params)) return false

            return true
        }

        override fun toString(): String {
            return "StringIdResource(messageResId=${(stringId)}, params=${params.contentToString()})"
        }
    }

    data class StringValueResource(val string: String?) : StringResource()
}

fun StringResource.getString(context: Context): String? {
    return ignoreExceptions {
        when (this) {
            is StringResource.StringIdResource -> {
                val fixedParams = this.params.map { if (it is StringResource) it.getString(context).orEmpty() else it }.toTypedArray()
                try {
                    context.resources.getString(this.stringId, *fixedParams)
                } catch (e: Exception) {
                    try {
                        context.resources.getString(this.stringId)
                    } catch (e: Exception) {
                        null
                    }
                }
            }
            is StringResource.StringValueResource -> this.string
        }
    }
}