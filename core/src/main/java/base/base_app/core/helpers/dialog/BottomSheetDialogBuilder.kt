package base.base_app.core.helpers.dialog

import android.content.Context
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import base.base_app.core.R
import base.base_app.core.base.BaseActivity
import base.base_app.core.helpers.StringResource
import base.base_app.core.helpers.getString
import base.base_app.core.ui.BetterAdapter
import base.base_app.core.ui.hide
import base.base_app.core.ui.setDataWithDiff
import base.base_app.core.ui.show
import base.base_app.core.ui.views.AppButton
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel


class BottomSheetDialogBuilder(private val view: View?, private val activityContext: Context, private val bottomInset: Int?) {

    private var built: Boolean = false
    private var itemDecorator: RecyclerView.ItemDecoration? = null
    private var adapter: BetterAdapter? = null
    private var data: List<Any> = emptyList()
    private var imageSet: Boolean = false
    private var dialog: BottomSheetDialog?

    constructor(baseActivity: BaseActivity) : this(baseActivity.provideDialogSheet(R.layout.bottom_sheet_dialog), baseActivity, baseActivity.provideBottomInset())

    init {
        dialog = view?.let { view ->
            view.findViewById<RecyclerView>(R.id.rv_dialog_list)?.apply {
                layoutManager = LinearLayoutManager(view.context)
                setHasFixedSize(true)
            }

            view.findViewById<ConstraintLayout>(R.id.cl_dialog_parent)?.let {
                it.updatePadding(bottom = bottomInset ?: it.paddingBottom)
            }

            BottomSheetDialog(activityContext)
                    .also { dialog ->
                        view.findViewById<ImageView>(R.id.iv_close_btn)?.setOnClickListener {
                            dialog.dismiss()
                        }
                        dialog.setContentView(view)
                        dialog.behavior.apply {
                            peekHeight = 1000

                            addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                                override fun onStateChanged(bottomSheet: View, newState: Int) {
                                    if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                                        val newMaterialShapeDrawable: MaterialShapeDrawable = createMaterialShapeDrawable(bottomSheet)
                                        ViewCompat.setBackground(bottomSheet, newMaterialShapeDrawable)
                                    }
                                }

                                override fun onSlide(bottomSheet: View, slideOffset: Float) {}
                            })
                        }

                        dialog.setOnShowListener { dialogInterface ->
                            val sheetDialog = dialogInterface as? BottomSheetDialog

                            val bottomSheet = sheetDialog?.findViewById<FrameLayout>(
                                    com.google.android.material.R.id.design_bottom_sheet
                            )

                            bottomSheet?.let {
                                BottomSheetBehavior.from(it).apply {
                                    // any other behavior modification here
                                    state = BottomSheetBehavior.STATE_EXPANDED
                                    skipCollapsed = true
                                }
                            }
                        }
                    }
        }
    }

    private fun createMaterialShapeDrawable(bottomSheet: View): MaterialShapeDrawable {
        val shapeAppearanceModel: ShapeAppearanceModel = ShapeAppearanceModel.builder(activityContext, 0, R.style.ShapeAppearanceOverlay)
                .build()

        val currentMaterialShapeDrawable = bottomSheet.background as MaterialShapeDrawable
        val newMaterialShapeDrawable = MaterialShapeDrawable(shapeAppearanceModel)

        newMaterialShapeDrawable.initializeElevationOverlay(activityContext)
        newMaterialShapeDrawable.fillColor = currentMaterialShapeDrawable.fillColor
        newMaterialShapeDrawable.tintList = currentMaterialShapeDrawable.tintList
        newMaterialShapeDrawable.elevation = currentMaterialShapeDrawable.elevation
        newMaterialShapeDrawable.strokeWidth = currentMaterialShapeDrawable.strokeWidth
        newMaterialShapeDrawable.strokeColor = currentMaterialShapeDrawable.strokeColor
        return newMaterialShapeDrawable
    }


    fun setTitle(@StringRes titleResId: Int): BottomSheetDialogBuilder {
        view?.findViewById<TextView>(R.id.tv_dialog_title)?.setText(titleResId)
        return this
    }

    fun setTitle(title: StringResource?): BottomSheetDialogBuilder {
        view?.findViewById<TextView>(R.id.tv_dialog_title)?.text = title?.getString(activityContext)
        return this
    }

    fun setDialogDismissListener(listener: BottomSheetDialogDismissListener): BottomSheetDialogBuilder {
        dialog?.setOnDismissListener { listener.onDialogDismissed() }
        return this
    }

    fun setSubtitle(subtitle: StringResource?): BottomSheetDialogBuilder {
        view?.findViewById<TextView>(R.id.tv_subtitle)?.let {
            it.text = subtitle?.getString(activityContext)
            it.show()
        }
        return this
    }

    fun setSubtitle(@StringRes subtitleResId: Int): BottomSheetDialogBuilder {
        view?.findViewById<TextView>(R.id.tv_subtitle)?.let {
            it.setText(subtitleResId)
            it.show()
        }
        return this
    }

    fun setImageResId(@DrawableRes id: Int): BottomSheetDialogBuilder {
        view?.findViewById<ImageView>(R.id.iv_dialog_top_image)?.let {
            it.setImageResource(id)
            imageSet = true
            it.show()
        }
        return this
    }

    fun setDialogDismissListener(listener: () -> Unit): BottomSheetDialogBuilder {
        return setDialogDismissListener(BottomSheetDialogDismissListener { listener() })
    }

    fun setCloseButton(@StringRes title: Int, onClickAction: () -> Unit): BottomSheetDialogBuilder {
        return setupButton(R.id.btn_close, title, onClickAction)
    }

    private fun setupButton(buttonId: Int, @StringRes title: Int, onClickAction: () -> Unit): BottomSheetDialogBuilder {
        view?.let {
            it.findViewById<AppButton>(buttonId)?.let {
                it.show()
                it.setText(title)
                it.setOnClickListener {
                    onClickAction()
                    dialog?.dismiss()
                }
            }
        }
        return this
    }

    fun setOkButton(@StringRes title: Int, onClickAction: () -> Unit): BottomSheetDialogBuilder {
        return setupButton(R.id.btn_ok_action, title, onClickAction)
    }

    fun build(): BottomSheetDialogBuilder {
        buildDialog()
        return this
    }

    fun buildDialog(): BottomSheetDialog? {
        built = true
        if (!imageSet) {
            view?.findViewById<ImageView>(R.id.iv_close_btn)?.show( this@BottomSheetDialogBuilder.adapter != null)
            if (this@BottomSheetDialogBuilder.adapter != null) {
                view?.findViewById<ConstraintLayout>(R.id.cl_dialog_parent)?.let {
                    val constraintSet = ConstraintSet()
                    constraintSet.clone(it)
                    constraintSet.connect(R.id.tv_dialog_title, ConstraintSet.END, R.id.iv_close_btn, ConstraintSet.START, 20)
                    constraintSet.applyTo(it)
                }
            }
            view?.findViewById<TextView>(R.id.tv_dialog_title)?.gravity = Gravity.START
            view?.findViewById<TextView>(R.id.tv_subtitle)?.gravity = Gravity.START
            view?.findViewById<RecyclerView>(R.id.rv_dialog_list)?.also { rv ->
                itemDecorator?.let {
                    rv.addItemDecoration(it)
                }
                this@BottomSheetDialogBuilder.adapter?.let { adapter ->
                    rv.adapter = adapter
                    adapter.setDataWithDiff(data)
                    rv.show()
                } ?: run {
                    rv.hide()
                }
            }
        }
        return dialog
    }

    fun show() {
        if (built) {
            showLocal()
        } else {
            build().showLocal()
        }
    }

    private fun showLocal() {
        dialog?.let {
            it.show()
            if (it.window != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val window: Window? = it.window
                window?.findViewById<FrameLayout>(com.google.android.material.R.id.container)?.fitsSystemWindows = false
                // dark navigation bar icons
                window?.decorView?.let { decorView ->
                    decorView.systemUiVisibility = decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
                }
            }
        }
        built = false
    }

    fun setAdapterAndData(adapter: BetterAdapter, data: List<Any>): BottomSheetDialogBuilder {
        this.adapter = adapter
        this.data = data
        return this
    }

    fun setAdapterAndData(adapter: BetterAdapter, data: List<Any>, decorator: RecyclerView.ItemDecoration): BottomSheetDialogBuilder {
        this.itemDecorator = decorator
        return setAdapterAndData(adapter, data)
    }
}


fun interface BottomSheetDialogDismissListener {
    fun onDialogDismissed()
}