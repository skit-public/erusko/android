package base.base_app.core.helpers

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.PowerManager
import android.provider.Settings
import base.base_app.core.entities.activity_manager.AppActivityManager
import com.judemanutd.autostarter.AutoStartPermissionHelper
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject


interface BatterySaverManager {
    fun isAutoStartPermissionsAvailable(): Boolean
    fun getAutoStartPermission(): Boolean
    fun isBatterySaverMode(): Boolean
    fun openBatterySettings(): Boolean
}


class BatterySaverManagerImpl @Inject constructor(
        private val appActivityManager: AppActivityManager,
        @ApplicationContext val context: Context
) : BatterySaverManager {

    override fun isAutoStartPermissionsAvailable(): Boolean = AutoStartPermissionHelper.getInstance().isAutoStartPermissionAvailable(context)

    override fun getAutoStartPermission(): Boolean {
        return appActivityManager.getCurrentActivity()?.let {
            openAppSettings()
        } ?: false
    }

    private fun openAppSettings(): Boolean {
        return try {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.data = Uri.parse("package:" + context.packageName)
            appActivityManager.getCurrentActivity()?.startActivity(intent) != null
        } catch (e: Exception) {
            Logger.et { e }
            false
        }
    }

    override fun isBatterySaverMode(): Boolean {
        return (context.getSystemService(Context.POWER_SERVICE) as? PowerManager)?.isPowerSaveMode
                ?: false
    }

    override fun openBatterySettings(): Boolean {
        return openAppSettings()
    }
}
