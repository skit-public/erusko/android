package base.base_app.core.helpers

import base.base_app.core.base.BaseActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

sealed class PlayServicesResult {
    object Success : PlayServicesResult()
    object Error : PlayServicesResult()
}

object PlayServicesHelper {

    suspend fun getPlayServicesResolver(activity: BaseActivity?): PlayServicesResult {
        return withContext(Dispatchers.Main) {
            if (activity == null) {
                return@withContext PlayServicesResult.Success
            }

            val playInstance = GoogleApiAvailability.getInstance()

            return@withContext when (playInstance.isGooglePlayServicesAvailable(activity)) {
                ConnectionResult.SUCCESS -> {
                    PlayServicesResult.Success
                }
                else -> PlayServicesResult.Error
            }
        }
    }

    suspend fun makePlayServicesAvailable(baseActivity: BaseActivity) = suspendCoroutine<Unit> { cont ->
        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(baseActivity)
                .addOnSuccessListener {
                    cont.resume(Unit)
                }.addOnFailureListener {
                    cont.resumeWithException(it)
                }
    }
}