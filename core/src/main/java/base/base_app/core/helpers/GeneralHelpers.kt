package base.base_app.core.helpers

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.util.Base64
import base.base_app.core.BuildConfig
import com.google.android.gms.nearby.exposurenotification.DailySummary
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import retrofit2.Converter
import sk.slovenskoit.locale_change_core.LocaleManager
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.security.SecureRandom
import kotlin.random.Random


/**
 * Helper method for safer handling exceptions for release builds
 * @param action action to be performed when no exception occurs
 */
fun <T> ignoreExceptions(action: () -> T): T? {
    return ignoreExceptions(action, null)
}

/**
 * Helper method for safer handling exceptions for release builds
 * @param action action to be performed when no exception occurs
 * @param finally action to be performed as finally block
 */
fun <T> ignoreExceptions(action: () -> T, finally: (() -> Unit)? = null): T? {
    return when (BuildConfig.DEBUG) {
        true -> action.invoke()
        else -> {
            try {
                action.invoke()
            } catch (e: Exception) {
                Logger.et { e }.em { "ignored exception" }
                null
            } finally {
                finally?.let {
                    ignoreExceptions {
                        it.invoke()
                    }
                }
            }
        }
    }
}

private fun ByteArray.toHexConverter(): String {
    val hexCharts = "0123456789ABCDEF".toCharArray()
    val result = StringBuilder()
    forEach {
        val octet = it.toInt()
        val firstIndex = (octet and 0xF0).ushr(4)
        val secondIndex = octet and 0x0F
        result.append(hexCharts[firstIndex])
        result.append(hexCharts[secondIndex])
    }
    return result.toString()
}

fun generateKey(key: String): String {
    return key.toByteArray().toHexConverter()
}

fun Boolean?.orFalse(): Boolean {
    return this ?: false
}

fun Boolean?.orTrue(): Boolean {
    return this ?: true
}

fun Boolean?.isNull(): Boolean {
    return this == null
}

fun Activity.openPlayStore() {
    try {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
            setPackage("com.android.vending")
        }
        startActivity(intent)
    } catch (t: Throwable) {
        Logger.et { t }
    }
}

fun InputStream.copyAndCloseTo(outputStream: OutputStream) {
    use {
        outputStream.use {
            this.copyTo(it)
        }
    }
}

fun InputStream.copyAndCloseTo(file: File) {
    copyAndCloseTo(file.outputStream())
}

fun Int.daysToMillis() = this * 24 * 60 * 60 * 1000

fun Long.hoursToMillis() = this * 60 * 60 * 1000

fun DailySummary?.toFormattedDate(): String? {
    return this?.let {
        LocalDate.ofEpochDay(it.daysSinceEpoch.toLong())
                .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(LocaleManager.getCurrentLocale()))
    }
}


fun newRandomPadding(): String {
    return ByteArray(Random.nextInt(50, 400)).let {
        SecureRandom().nextBytes(it)
        it.encodeBase64()
    }
}

fun ByteArray.encodeBase64(): String {
    return Base64.encodeToString(this, Base64.NO_WRAP)
}

fun String.decodeBase64(): ByteArray {
    return Base64.decode(this, Base64.NO_WRAP)
}

fun String.removeAllSpaces(): String = this.replace("\\s".toRegex(), "")

fun Boolean.toActiveInactive(): String = if (this) "Active" else "Inactive"

fun Json.asJsonConverterFactory(): Converter.Factory {
    return this.asConverterFactory("application/json".toMediaType())
}