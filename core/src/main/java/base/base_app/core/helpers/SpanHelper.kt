package base.base_app.core.helpers

import android.graphics.RectF
import android.text.Selection
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.BackgroundColorSpan
import android.text.style.ClickableSpan
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import base.base_app.core.R
import timber.log.Timber

class SpanHelper {

    class SpanParam(vararg val objects: Any, val flag: Int = Spannable.SPAN_INCLUSIVE_EXCLUSIVE)

    class OnClickSpan(textView: TextView, private val clickListener: () -> Unit, private val drawUnderLine: Boolean = true, private val clickThrottleMillis: Long = 500, biggerArea: Boolean = true, movementMethod: LinkMovementMethod = AppLinkMovementMethod()) : ClickableSpan() {

        private var lastClick = 0L

        init {
            if (biggerArea) {
                textView.movementMethod = movementMethod
                textView.isSaveEnabled = false
                textView.linksClickable = true
                textView.highlightColor = ContextCompat.getColor(textView.context, R.color.transparent)
            }
        }

        override fun onClick(view: View) {
            view.cancelPendingInputEvents()
            val time = System.currentTimeMillis()
            if (time - lastClick >= clickThrottleMillis) {
                clickListener()
                lastClick = time
            }
        }

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = drawUnderLine
        }
    }

    companion object {
        private const val StartingPattern: String = "["
        private const val EndingPattern: String = "]"

        fun replace(text: CharSequence, vararg params: SpanParam): Spannable {
            val ssb = SpannableStringBuilder(text)
            for (param in params) {
                try {
                    val start = ssb.toString()
                            .indexOf(StartingPattern)
                    ssb.delete(start, start + 1)
                    val end = ssb.toString()
                            .indexOf(EndingPattern)
                    ssb.delete(end, end + 1)
                    for (o in param.objects) {
                        ssb.setSpan(o, start, end, param.flag)
                    }
                } catch (e: Exception) {
                    Timber.d("Cannot find replacement position for param: $param")
                }
            }
            return ssb
        }

        fun replaceFullText(text: CharSequence, vararg params: SpanParam): Spannable {
            val ssb = SpannableStringBuilder(text)
            for (param in params) {

                for (o in param.objects) {
                    ssb.setSpan(o, 0, ssb.length, param.flag)
                }
            }
            return ssb
        }
    }
}

class AppLinkMovementMethod : LinkMovementMethod() {

    private val touchedLineBounds = RectF()
    private var isUrlHighlighted: Boolean = false
    private var clickableSpanUnderTouchOnActionDown: ClickableSpan? = null
    private var activeTextViewHashcode: Int = 0

    override fun onTouchEvent(textView: TextView, text: Spannable, event: MotionEvent): Boolean {
        if (activeTextViewHashcode != textView.hashCode()) {
            activeTextViewHashcode = textView.hashCode()
            textView.autoLinkMask = 0
        }

        val clickableSpanUnderTouch = findClickableSpanUnderTouch(textView, text, event)
        val touchStartedOverALink = clickableSpanUnderTouchOnActionDown != null

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (clickableSpanUnderTouch != null) {
                    highlightUrl(textView, clickableSpanUnderTouch, text)
                }
                clickableSpanUnderTouchOnActionDown = clickableSpanUnderTouch
                return touchStartedOverALink
            }

            MotionEvent.ACTION_UP -> {
                if (touchStartedOverALink && clickableSpanUnderTouch === clickableSpanUnderTouchOnActionDown) {
                    dispatchUrlClick(textView, clickableSpanUnderTouch)
                }
                cleanupOnTouchUp(textView)
                return touchStartedOverALink
            }

            MotionEvent.ACTION_CANCEL -> {
                cleanupOnTouchUp(textView)
                return false
            }

            MotionEvent.ACTION_MOVE -> {
                if (clickableSpanUnderTouch != null) {
                    highlightUrl(textView, clickableSpanUnderTouch, text)
                } else {
                    removeUrlHighlightColor(textView)
                }
                return touchStartedOverALink
            }

            else -> return false
        }
    }

    private fun cleanupOnTouchUp(textView: TextView) {
        removeUrlHighlightColor(textView)
    }

    private fun findClickableSpanUnderTouch(textView: TextView, text: Spannable, event: MotionEvent): ClickableSpan? {
        var touchX = event.x.toInt()
        var touchY = event.y.toInt()

        touchX -= textView.totalPaddingLeft
        touchY -= textView.totalPaddingTop

        touchX += textView.scrollX
        touchY += textView.scrollY

        val layout = textView.layout
        val touchedLine = layout.getLineForVertical(touchY)
        val touchOffset = layout.getOffsetForHorizontal(touchedLine, touchX.toFloat())

        touchedLineBounds.left = layout.getLineLeft(touchedLine)
        touchedLineBounds.top = layout.getLineTop(touchedLine).toFloat()
        touchedLineBounds.right = layout.getLineWidth(touchedLine) + touchedLineBounds.left
        touchedLineBounds.bottom = layout.getLineBottom(touchedLine).toFloat()

        return if (touchedLineBounds.contains(touchX.toFloat(), touchY.toFloat())) {
            val spans = text.getSpans(touchOffset, touchOffset, ClickableSpan::class.java)
            spans.firstOrNull { it is ClickableSpan }
        } else {
            null
        }
    }

    private fun highlightUrl(textView: TextView, clickableSpan: ClickableSpan, text: Spannable) {
        if (isUrlHighlighted) {
            return
        }
        isUrlHighlighted = true

        val spanStart = text.getSpanStart(clickableSpan)
        val spanEnd = text.getSpanEnd(clickableSpan)
        text.setSpan(BackgroundColorSpan(textView.highlightColor), spanStart, spanEnd, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        textView.text = text

        Selection.setSelection(text, spanStart, spanEnd)
    }

    private fun removeUrlHighlightColor(textView: TextView) {
        if (!isUrlHighlighted) {
            return
        }
        isUrlHighlighted = false

        val text = textView.text as Spannable

        val highlightSpans = text.getSpans(0, text.length, BackgroundColorSpan::class.java)
        for (highlightSpan in highlightSpans) {
            text.removeSpan(highlightSpan)
        }

        textView.text = text

        Selection.removeSelection(text)
    }

    private fun dispatchUrlClick(textView: TextView, clickableSpan: ClickableSpan?) {
        clickableSpan?.onClick(textView)
    }
}
