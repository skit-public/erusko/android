package base.base_app.core.helpers

import base.base_app.core.R
import base.base_app.core.base.BaseFragment
import base.base_app.core.entities.error_handler.ErrorData
import base.base_app.core.helpers.dialog.BottomSheetDialogBuilder

fun ErrorData?.show(fragment: BaseFragment) {
    if (this != null && !this.isEmptyError()) {
        fragment.getBaseActivity()?.let {
            BottomSheetDialogBuilder(it)
                    .setTitle(this.title)
                    .setImageResId(R.drawable.ic_dialog_alert)
                    .setSubtitle(this.message)
                    .setCloseButton(R.string.general_back) {}
                    .build()
                    .show()
        }
    }
}