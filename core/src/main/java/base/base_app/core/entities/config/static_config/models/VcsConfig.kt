package base.base_app.core.entities.config.static_config.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// version check
@Serializable
data class VcsConfig(
        @SerialName("latestVersion") val latestVersion: String = "1.0.0",
        @SerialName("mandatoryVersion") val mandatoryVersion: String = "1.0.0"
)