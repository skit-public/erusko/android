package base.base_app.core.entities.preferences

enum class IntroProgress(val progress: Int) {
    WELCOME(0),
    PERMISSIONS(1),
    TERMS_AND_CONDITIONS(2)
}