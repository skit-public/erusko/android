package base.base_app.core.entities.config.static_config

import base.base_app.core.entities.config.static_config.models.*
import base.base_app.core.entities.config.static_config.remote_config.RemoteConfigKeys
import base.base_app.core.entities.config.static_config.remote_config.RemoteConfigManager
import base.base_app.core.entities.config.static_config.remote_config.getValue

@Deprecated(message = "Use ConfigProvider")
data class AppConfig(
        val generalConfig: () -> GeneralConfig,
        val networkConfig: () -> NetworkConfig,
        val exposuresDataConfig: () -> ExposuresDataConfig,
        val exposuresServerConfig: () -> ExposuresServerConfig,
        val vcsConfig: () -> VcsConfig,
        val localizations: () -> Localizations
)

fun getDevConfig(remoteConfigManager: RemoteConfigManager): AppConfig = getProdConfig(remoteConfigManager).let { prodConfig ->
    prodConfig.copy(
            networkConfig = {
                prodConfig.networkConfig().copy(isChuckerEnabled = true)
            },
            generalConfig = {
                prodConfig.generalConfig().copy(
                        remoteConfigFetchIntervalSeconds = 300,
                        disableScreenshots = false
                )
            }
    )
}

/**
 * Tu sú defaultne všetky konfigurácie pre apku
 * Keď je potreba niečo upraviť pre daný environment, je to možné zmeniť cez copy data classy
 */
fun getProdConfig(remoteConfigManager: RemoteConfigManager): AppConfig = AppConfig(
        generalConfig = {
            GeneralConfig(
                    disableScreenshots = true,
            )
        },
        networkConfig = {
            NetworkConfig()
        },
        exposuresServerConfig = {
            remoteConfigManager.getValue(RemoteConfigKeys.ExposuresServerKey, ExposuresServerConfig()
            )
        },
        exposuresDataConfig = {
            remoteConfigManager.getValue(RemoteConfigKeys.ExposuresDataKey, ExposuresDataConfig())
        },
        vcsConfig = {
            remoteConfigManager.getValue(RemoteConfigKeys.VcsConfigKey, VcsConfig())
        },
        localizations = {
            Localizations(
                    isRemoteLocalizationEnabled = remoteConfigManager.getValue(RemoteConfigKeys.RemoteLocalizationsEnabled, true),
                    localizations = remoteConfigManager.getValue(RemoteConfigKeys.LocalizationsKey, listOf())
            )
        }
)