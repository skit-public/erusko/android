package base.base_app.core.entities.error_handler

import base.base_app.core.features.exposure.models.key_sever.KeyServerResponseException
import base.base_app.core.features.exposure.models.key_sever.UploadKeysErrorEnum
import base.base_app.core.features.exposure.models.verification_server.VerificationResponseException
import base.base_app.core.features.exposure.models.verification_server.VerifyCodeErrorEnum
import com.google.android.gms.common.api.ApiException

fun interface ErrorPredicate {
    fun matches(throwable: Throwable): Boolean
}

class ExposureNotifCommonType(private val statusCode: Int) : ErrorPredicate {
    override fun matches(throwable: Throwable): Boolean {
        return throwable is ApiException && throwable.status.statusCode == statusCode
    }
}

class VerificationErrorCommonType(private val error: VerifyCodeErrorEnum) : ErrorPredicate {
    override fun matches(throwable: Throwable): Boolean {
        return throwable is VerificationResponseException && throwable.errorResponse.errorCode == error
    }
}

class UploadKeysErrorCommonType(private val error: UploadKeysErrorEnum) : ErrorPredicate {
    override fun matches(throwable: Throwable): Boolean {
        return throwable is KeyServerResponseException && throwable.errorResponse.errorCode == error
    }
}

class ExposureNotifConnectionType(private val statusCode: Int) : ErrorPredicate {
    override fun matches(throwable: Throwable): Boolean {
        return throwable is ApiException && throwable.status.connectionResult?.errorCode == statusCode
    }
}

class ClassType(private val clazz: Class<*>) : ErrorPredicate {
    override fun matches(throwable: Throwable): Boolean = clazz.isInstance(throwable)
}