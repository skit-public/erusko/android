package base.base_app.core.entities.error_handler

import android.content.Context
import androidx.activity.result.IntentSenderRequest
import base.base_app.core.R
import base.base_app.core.entities.error_handler.ErrorHandler.Companion.getGeneralError
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.features.exposure.ExposureResolutionRequestHandler
import base.base_app.core.helpers.StringResource
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.nearby.exposurenotification.ExposureNotificationStatusCodes
import dagger.hilt.android.qualifiers.ApplicationContext
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

data class ErrorData(
        val title: StringResource? = null,
        val message: StringResource? = null,
        val type: Type = Type.GENERAL
) {

    enum class Type {
        GENERAL,
        INTERNET
    }

    companion object {
        val EMPTY = ErrorData()
    }

    fun isEmptyError() = title == null && message == null
}

interface ErrorHandler {
    fun mapError(error: Throwable, vararg customMapping: Pair<ErrorPredicate, (Throwable) -> ErrorData>): ErrorData

    companion object {
        fun getGeneralError(): ErrorData = ErrorData(StringResource.StringIdResource(R.string.general_error_title), StringResource.StringIdResource(R.string.general_error_message), type = ErrorData.Type.GENERAL)
        fun getInternetError(): ErrorData = ErrorData(StringResource.StringIdResource(R.string.connection_error_title), StringResource.StringIdResource(R.string.connection_error_message), type = ErrorData.Type.INTERNET)
    }
}

class ErrorHandlerImpl @Inject constructor(
        @ApplicationContext val context: Context,
        private val exposureResolutionRequestHandler: ExposureResolutionRequestHandler,
        private val eventTracker: EventTracker
) : ErrorHandler {

    override fun mapError(error: Throwable, vararg customMapping: Pair<ErrorPredicate, (Throwable) -> ErrorData>): ErrorData {
        error.printStackTrace()
        eventTracker.trackEvent(Events.Error(error.message ?: "unknown"))
        return customMapping
                .toMap()
                .plus(getDefaultMapping())
                .plus(getExposureNotificationsErrorMapping())
                .filter { it.key.matches(error) }
                .map { it.value.invoke(error) }
                .firstOrNull() ?: getGeneralError()
    }

    private fun getExposureNotificationsErrorMapping(): Map<ErrorPredicate, (Throwable) -> ErrorData> = mapOf(
            ExposureNotifCommonType(CommonStatusCodes.SUCCESS_CACHE) to { ErrorData.EMPTY },
            ExposureNotifCommonType(CommonStatusCodes.SUCCESS) to { ErrorData.EMPTY },
            ExposureNotifCommonType(CommonStatusCodes.SIGN_IN_REQUIRED) to { ErrorData(StringResource.StringIdResource(R.string.exposure_error_sign_in_required_title), StringResource.StringIdResource(R.string.exposure_error_sign_in_required_message)) },
            ExposureNotifCommonType(CommonStatusCodes.INVALID_ACCOUNT) to { ErrorData(StringResource.StringIdResource(R.string.exposure_error_invalid_account_title), StringResource.StringIdResource(R.string.exposure_error_invalid_account_message)) },
            ExposureNotifCommonType(CommonStatusCodes.RESOLUTION_REQUIRED) to {
                if (it is ApiException && it.status.hasResolution()) {
                    it.status.resolution?.intentSender?.let { intentSender ->
                        exposureResolutionRequestHandler.getLauncher()?.launch(IntentSenderRequest.Builder(intentSender).build())
                    }
                    ErrorData.EMPTY
                } else {
                    getGeneralError()
                }
            },
            ExposureNotifCommonType(CommonStatusCodes.NETWORK_ERROR) to { ErrorData(StringResource.StringIdResource(R.string.connection_error_title), StringResource.StringIdResource(R.string.connection_error_message)) },
            ExposureNotifCommonType(CommonStatusCodes.INTERNAL_ERROR) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.DEVELOPER_ERROR) to { ErrorData(StringResource.StringIdResource(R.string.exposure_error_developer_error_title), StringResource.StringIdResource(R.string.exposure_error_developer_error_message)) },
            ExposureNotifCommonType(CommonStatusCodes.ERROR) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.INTERRUPTED) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.TIMEOUT) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.CANCELED) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.API_NOT_CONNECTED) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.REMOTE_EXCEPTION) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.CONNECTION_SUSPENDED_DURING_CALL) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.RECONNECTION_TIMED_OUT_DURING_UPDATE) to { getGeneralError() },
            ExposureNotifCommonType(CommonStatusCodes.RECONNECTION_TIMED_OUT) to { getGeneralError() },

            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED) to { getGeneralError() },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_ALREADY_STARTED) to { ErrorData.EMPTY },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_NOT_SUPPORTED) to { ErrorData(StringResource.StringIdResource(R.string.exposure_error_failed_not_supported_title), StringResource.StringIdResource(R.string.exposure_error_failed_not_supported_message)) },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_REJECTED_OPT_IN) to { ErrorData.EMPTY },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_SERVICE_DISABLED) to { ErrorData(StringResource.StringIdResource(R.string.exposure_error_failed_service_disabled_title), StringResource.StringIdResource(R.string.exposure_error_failed_service_disabled_message)) },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_BLUETOOTH_DISABLED) to { ErrorData(StringResource.StringIdResource(R.string.exposure_error_failed_bluetooth_disabled_title), StringResource.StringIdResource(R.string.exposure_error_failed_bluetooth_disabled_message)) },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_TEMPORARILY_DISABLED) to { ErrorData(StringResource.StringIdResource(R.string.exposure_error_failed_temporarily_disabled_title), StringResource.StringIdResource(R.string.exposure_error_failed_temporarily_disabled_message)) },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_DISK_IO) to { ErrorData(StringResource.StringIdResource(R.string.exposure_error_failed_disk_io_title), StringResource.StringIdResource(R.string.exposure_error_failed_disk_io_message)) },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_UNAUTHORIZED) to { getGeneralError() },
            ExposureNotifConnectionType(ExposureNotificationStatusCodes.FAILED_RATE_LIMITED) to { getGeneralError() }
    )

    private fun getDefaultMapping(): Map<ErrorPredicate, (Throwable) -> ErrorData> = mapOf(
            ClassType(UnknownHostException::class.java) to { ErrorData(StringResource.StringIdResource(R.string.connection_error_title), StringResource.StringIdResource(R.string.connection_error_message), type = ErrorData.Type.INTERNET) },
            ClassType(ConnectException::class.java) to { ErrorData(StringResource.StringIdResource(R.string.connection_error_title), StringResource.StringIdResource(R.string.connection_error_message), type = ErrorData.Type.INTERNET) },
            ClassType(SocketException::class.java) to { ErrorData(StringResource.StringIdResource(R.string.connection_error_title), StringResource.StringIdResource(R.string.connection_error_message), type = ErrorData.Type.INTERNET) },
            ClassType(SocketTimeoutException::class.java) to { ErrorData(StringResource.StringIdResource(R.string.connection_error_title), StringResource.StringIdResource(R.string.connection_error_message), type = ErrorData.Type.INTERNET) },
    )
}