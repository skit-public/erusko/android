package base.base_app.core.entities.events

import android.content.Context
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.app_helper.AppHelper
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

interface EventTracker {
    fun trackEvent(event: Events)
    fun trackFragmentScreen(fragment: String)
    fun setCustomProperty(properties: CustomProperties, value: String)
}

class EventTrackerImpl @Inject constructor(
        @ApplicationContext private val context: Context,
        private val appHelper: AppHelper
) : EventTracker {

    private val firebaseAnalytics by lazy { FirebaseAnalytics.getInstance(context) }

    override fun trackEvent(event: Events) {
        appHelper.addAnalyticsLog("${event.eventName} ${event.segmentation ?: ""}")
        Logger.dm { "Tracking event: ${event.eventName} ${event.segmentation}" }
        firebaseAnalytics.logEvent(event.eventName.take(39)) {
            event.segmentation.orEmpty().forEach { (key, value) ->
                param(key, value)
            }
        }
    }

    override fun setCustomProperty(properties: CustomProperties, value: String) {
        appHelper.addAnalyticsLog("${properties.name}: $value")
        Logger.dm { "Tracking custom property ${properties.name} $value" }
        firebaseAnalytics.setUserProperty(properties.name, value)
    }

    /**
     * Track fragment screen, its bugged in the SDK and not showing any fragments
     * https://github.com/firebase/firebase-android-sdk/issues/1976
     *
     * @param fragment
     */
    @Deprecated("currently bugged and not working")
    override fun trackFragmentScreen(fragment: String) {
        Logger.dm { "Tracking screen: $fragment" }
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, fragment)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, fragment)
        }
    }
}