package base.base_app.core.entities.chucker

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.RetentionManager
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

interface Chucker {
    fun getCollector(): ChuckerCollector

    fun log(throwable: Throwable? = null, message: String? = null, tag: String? = null)
}

class ChuckerImpl @Inject constructor(
        @ApplicationContext private val context: Context,
) : Chucker {

    override fun getCollector(): ChuckerCollector {
        return ChuckerCollector(
                context = context,
                showNotification = true,
                retentionPeriod = RetentionManager.Period.ONE_HOUR
        )
    }

    override fun log(t: Throwable?, message: String?, tag: String?) {
        getCollector().onError("eRúško $tag",
                ChuckerThrowable("$message ${t?.let { "\n + throwable = $it" }.orEmpty()}"))
    }

    private class ChuckerThrowable(message: String) : RuntimeException(message)
}