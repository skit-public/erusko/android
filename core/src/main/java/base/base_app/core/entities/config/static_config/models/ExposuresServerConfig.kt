package base.base_app.core.entities.config.static_config.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ExposuresServerConfig(
        /**
         * Základná Url pre server kľúčov
         */
        @SerialName("keyServerUrl") val keyServerUrl: String = "",

        /**
         * Základná Url pre server Verifikácie kódu.
         */
        @SerialName("verificationServerUrl") val verificationServerUrl: String = "",

        /**
         * Url odkiaľ sa majú sťahovať súbory zip, obsahujúce “nákazlivé kľuče”.
         */
        @SerialName("keyExportUrl") val keyExportUrl: String = "",

        /**
         * Česi to posielali na verifikašný server, predpoklad package name ktoré má povolené od googlu používať Exposure Notifications API.
         */
        @SerialName("healthAuthorityID") val healthAuthorityID: String = "",

        /**
         * Api kluč pre verifikačný server
         */
        @SerialName("verificationServerApiKey") val verificationServerApiKey: String = "",

        @SerialName("verificationCodeGenerationUrl") val verificationCodeGenerationUrl: String = "",
        @SerialName("verificationCodeGeneratorApiKey") val verificationCodeGeneratorApiKey: String = "",
)