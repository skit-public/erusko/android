package base.base_app.core.entities.preferences

import base.base_app.core.entities.config.static_config.ConfigProvider
import sk.it.security_utils.preferences.EncryptedSharedPrefModel
import com.chibatching.kotpref.enumpref.enumValuePref
import com.chibatching.kotpref.enumpref.nullableEnumValuePref
import javax.inject.Inject

interface PersistentAppPreferences {
    var numberOfNewFiles: Int
    var isFirstActivationFlow: Boolean
    var environment: ConfigProvider.Environment?
    var showWorkerDebugNotifications: Boolean
    var showStringKeysInsteadOfValues: Boolean

    @Deprecated("User setProgress(IntroProgress) instead")
    var introProgress: IntroProgress

    fun setProgress(currentProgress: IntroProgress)
}

class PersistentAppPreferencesImpl @Inject constructor(
) : EncryptedSharedPrefModel("8vf1sc95wef9nf15"), PersistentAppPreferences {
    override var numberOfNewFiles: Int by intPref(0)
    override var isFirstActivationFlow: Boolean by booleanPref(default = true)
    override var environment: ConfigProvider.Environment? by nullableEnumValuePref()
    override var showWorkerDebugNotifications: Boolean by booleanPref(default = false)
    override var showStringKeysInsteadOfValues: Boolean by booleanPref(default = false)
    override var introProgress: IntroProgress by enumValuePref(IntroProgress.WELCOME)

    override fun setProgress(currentProgress: IntroProgress) {
        if (this.introProgress.progress < currentProgress.progress) {
            this.introProgress = currentProgress
        }
    }
}