package base.base_app.core.entities.config.static_config.remote_config

import base.base_app.core.entities.config.static_config.models.ExposuresDataConfig
import base.base_app.core.entities.config.static_config.models.ExposuresServerConfig
import base.base_app.core.entities.config.static_config.models.VcsConfig
import sk.slovenskoit.locale_change_core.models.Translations

sealed class RemoteConfigKeys<T : Any>(val key: String) {
    object VcsConfigKey : RemoteConfigKeys<VcsConfig>("versionControl")
    object LocalizationsKey : RemoteConfigKeys<List<Translations>>("localizations")
    object ExposuresServerKey : RemoteConfigKeys<ExposuresServerConfig>("exposuresServer")
    object ExposuresDataKey : RemoteConfigKeys<ExposuresDataConfig>("exposuresData")
    object RemoteLocalizationsEnabled : RemoteConfigKeys<Boolean>("remoteLocalizationsEnabled")
}