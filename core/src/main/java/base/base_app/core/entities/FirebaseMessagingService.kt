package base.base_app.core.entities

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import base.base_app.core.R
import base.base_app.core.helpers.Logger
import base.base_app.core.ui.startup.StartupActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class AppMessagingService : FirebaseMessagingService() {

    companion object {
        private const val CHANNEL_ID = "default_notifications_channel"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        remoteMessage.notification?.let { notification ->
            (getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager?)?.let { notificationManager ->

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannel(CHANNEL_ID, this.getString(R.string.notifications_channel_default), NotificationManager.IMPORTANCE_DEFAULT).also {
                        (this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(it)
                    }
                }

                val pendingIntent: PendingIntent = PendingIntent.getActivity(
                        this,
                        0,
                        Intent(this, StartupActivity::class.java).apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) },
                        PendingIntent.FLAG_ONE_SHOT)

                val notificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.app_icon))
                        .setSmallIcon(R.drawable.app_icon)
                        .setTicker(notification.title)
                        .setContentTitle(notification.title)
                        .setContentText(notification.body)
                        .setAutoCancel(true)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pendingIntent)

                notificationManager.notify(System.currentTimeMillis().toString(), 0, notificationBuilder.build())
            }
        }
    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        Logger.dm { "New Push Token: $newToken" }
    }
}