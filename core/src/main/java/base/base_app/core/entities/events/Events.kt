package base.base_app.core.entities.events

sealed class Events(val eventName: String, val segmentation: Map<String, String>? = null) {
    data class ScreenResumed(val screenName: String) : Events("screen", mapOf(
            "screen_name" to screenName
    ))

    data class MandatoryUpdateError(val currentVersion: String, val mandatoryVersion: String) : Events("app_mandatory_update_error", mapOf(
            "current_version" to currentVersion,
            "mandatory_version" to mandatoryVersion
    ))

    object InitialActivationStarted : Events("app_activation_started")
    object PlayServicesError : Events("app_play_version_error")
    object InitialActivationFinished : Events("app_activation_finished")
    object InitialActivationPermissionDenied : Events("app_activation_permission_denied")

    object VerificationDeeplinkCodeUsed : Events("app_vc_deeplink_opened")
    object VerificationCodeSubmitted : Events("app_vc_manually_typed")
    data class VerificationServerError(val error: String) : Events("app_vc_verification_error", mapOf(
            "error_code" to error
    ))

    object VerificationServerCodeVerified : Events("app_vc_verification_success")
    object ShareKeysDenied : Events("app_diagnostic_keys_permission_denied")
    object KeyServerError : Events("app_diagnostic_keys_error")
    object DiagnosisKeysSharedSuccess : Events("app_diagnostic_keys_shared")
    object UserExposedNotificationShown : Events("app_exposure_detected")
    object ShareAppClicked : Events("app_share_app_clicked")
    data class DownloadKeysFinished(val numberOfNewFiles: Int) : Events("app_exposure_batch_download_finished", mapOf(
            "new_files" to numberOfNewFiles.toString()
    ))

    object DownloadKeysFailure : Events("app_exposure_batch_download_failed")
    object DownloadKeysStarted : Events("app_exposure_batch_download_started")

    data class Error(val message: String) : Events("error", mapOf(
            "reason" to message
    ))
}


sealed class CustomProperties(val name: String) {
    object ExposureScanningState : CustomProperties("app_exposure_state")
}