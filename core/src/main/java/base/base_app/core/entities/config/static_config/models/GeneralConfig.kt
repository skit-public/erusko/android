package base.base_app.core.entities.config.static_config.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GeneralConfig(
        @SerialName("disableScreenshots") val disableScreenshots: Boolean = false,
        @SerialName("remoteConfigFetchIntervalSeconds") val remoteConfigFetchIntervalSeconds: Long = 1800
)