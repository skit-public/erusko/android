package base.base_app.core.entities.config.static_config

import base.base_app.core.entities.config.static_config.ConfigProvider.Environment.PROD
import base.base_app.core.entities.config.static_config.ConfigProvider.Environment.TEST
import base.base_app.core.entities.config.static_config.remote_config.RemoteConfigManager

interface ConfigProvider {
    fun provide(): AppConfig
    fun switchEnvironment(environment: Environment)
    fun resetEnvironment()
    fun getCurrentEnvironment(): Environment

    enum class Environment(val title: String) {
        TEST("test"),
        PROD("prod");

        companion object {
            fun getDefault(): Environment = TEST
        }
    }
}

class ConfigProviderImpl constructor(
        private val remoteConfigManager: RemoteConfigManager,
        private val defaultEnvironment: ConfigProvider.Environment
) : ConfigProvider {

    init {
        switchEnvironment(defaultEnvironment)
    }

    @Volatile
    private lateinit var appConfig: AppConfig

    @Volatile
    private lateinit var currentEnvironment: ConfigProvider.Environment

    @Synchronized
    override fun getCurrentEnvironment(): ConfigProvider.Environment {
        checkInitialized()
        return currentEnvironment
    }

    @Synchronized
    override fun provide(): AppConfig {
        checkInitialized()
        return appConfig
    }

    @Synchronized
    override fun switchEnvironment(environment: ConfigProvider.Environment) {
        this.appConfig = when (environment) {
            TEST -> getDevConfig(remoteConfigManager)
            PROD -> getProdConfig(remoteConfigManager)
        }
        this.currentEnvironment = environment
    }

    @Synchronized
    override fun resetEnvironment() {
        checkInitialized()
        switchEnvironment(defaultEnvironment)
    }

    private fun checkInitialized() {
        if (!::appConfig.isInitialized || !::currentEnvironment.isInitialized) {
            switchEnvironment(defaultEnvironment)
        }
    }
}