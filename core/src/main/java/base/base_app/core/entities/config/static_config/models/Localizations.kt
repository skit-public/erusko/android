package base.base_app.core.entities.config.static_config.models

import sk.slovenskoit.locale_change_core.models.Translations

data class Localizations(
        val isRemoteLocalizationEnabled: Boolean = true,
        val localizations: List<Translations> = listOf()
)