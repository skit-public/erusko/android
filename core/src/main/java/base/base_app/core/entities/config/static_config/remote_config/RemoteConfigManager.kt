package base.base_app.core.entities.config.static_config.remote_config

import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.helpers.Logger
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Provider
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

interface RemoteConfigManager {
    suspend fun updateConfig()
    fun getRemoteConfig(): FirebaseRemoteConfig
    fun <T : Any> logConfig(key: RemoteConfigKeys<*>, value: T?, default: T)
    fun isAllowedToFetchNewConfig(): Boolean
    val json: Json
    fun hasSuccessFetch(): Boolean
}

class RemoteConfigManagerImpl @Inject constructor(
        private val configProvider: Provider<ConfigProvider>,
        override val json: Json
) : RemoteConfigManager {

    companion object {
        const val logConfig = false
    }

    override fun isAllowedToFetchNewConfig(): Boolean {
        return System.currentTimeMillis() - getRemoteConfig().info.fetchTimeMillis > configProvider.get().provide().generalConfig().remoteConfigFetchIntervalSeconds * 1000
    }

    override fun hasSuccessFetch(): Boolean = getRemoteConfig().info.fetchTimeMillis > 0

    override fun getRemoteConfig(): FirebaseRemoteConfig = firebaseRemoteConfig

    private val firebaseRemoteConfig: FirebaseRemoteConfig by lazy { FirebaseRemoteConfig.getInstance() }

    override suspend fun updateConfig() = suspendCoroutine<Unit> { sc ->
        Logger.dm { "Updating remote config started" }
        getRemoteConfig().fetch(configProvider.get().provide().generalConfig().remoteConfigFetchIntervalSeconds)
                .continueWithTask {
                    getRemoteConfig().activate()
                }.addOnSuccessListener {
                    Logger.dm { "Remote Config updated $it" }
                    sc.resume(Unit)
                }.addOnFailureListener {
                    Logger.dt { it }
                    sc.resume(Unit)
                }
    }

    override fun <T : Any> logConfig(key: RemoteConfigKeys<*>, value: T?, default: T) {
        if (logConfig) {
            Logger.dm { "Providing config for key: ${key.key}\nvalue: ${value ?: "ERROR RETRIEVING FROM REMOTE CONFIG"}\ndefault: $default" }
        }
    }
}

inline fun <reified T : Any> RemoteConfigManager.getValue(key: RemoteConfigKeys<T>, default: T): T {
    return when (T::class) {
        Boolean::class -> getBoolean(key, default as Boolean) as? T? ?: default
        ByteArray::class -> getByteArray(key, default as ByteArray) as? T? ?: default
        Double::class -> getDouble(key, default as Double) as? T? ?: default
        Long::class -> getLong(key, default as Long) as? T? ?: default
        String::class -> getString(key, default as String) as? T? ?: default
        BigDecimal::class -> getBigDecimal(key, default as BigDecimal) as? T? ?: default
        else -> getObject(key, default) as? T? ?: default
    }
}

inline fun <reified T : Any> RemoteConfigManager.getBoolean(key: RemoteConfigKeys<T>, default: Boolean): Boolean = getValue(key, default) { getRemoteConfig().getValue(key.key).checkSource().asBoolean() }
inline fun <reified T : Any> RemoteConfigManager.getByteArray(key: RemoteConfigKeys<T>, default: ByteArray): ByteArray = getValue(key, default) { getRemoteConfig().getValue(key.key).checkSource().asByteArray() }
inline fun <reified T : Any> RemoteConfigManager.getDouble(key: RemoteConfigKeys<T>, default: Double): Double = getValue(key, default) { getRemoteConfig().getValue(key.key).checkSource().asDouble() }
inline fun <reified T : Any> RemoteConfigManager.getLong(key: RemoteConfigKeys<T>, default: Long): Long = getValue(key, default) { getRemoteConfig().getValue(key.key).checkSource().asLong() }
inline fun <reified T : Any> RemoteConfigManager.getString(key: RemoteConfigKeys<T>, default: String): String = getValue(key, default) { getRemoteConfig().getValue(key.key).checkSource().asString() }
inline fun <reified T : Any> RemoteConfigManager.getBigDecimal(key: RemoteConfigKeys<T>, default: BigDecimal): BigDecimal = getValue(key, default) { getRemoteConfig().getValue(key.key).checkSource().asString().toBigDecimal() }
inline fun <reified T : Any> RemoteConfigManager.getObject(key: RemoteConfigKeys<T>, default: T): T = getValue(key, default) { json.decodeFromString(getRemoteConfig().getValue(key.key).checkSource().asString()) }

fun FirebaseRemoteConfigValue.checkSource(): FirebaseRemoteConfigValue {
    return if (this.source == FirebaseRemoteConfig.VALUE_SOURCE_STATIC) {
        throw RuntimeException("No default value for requested key")
    } else {
        this
    }
}

fun <T : Any> RemoteConfigManager.getValue(key: RemoteConfigKeys<*>, default: T, getter: () -> T): T {
    return try {
        getter.invoke().also {
            logConfig(key, it, default)
        }
    } catch (e: Throwable) {
        logConfig(key, null, default)
        default
    }
}