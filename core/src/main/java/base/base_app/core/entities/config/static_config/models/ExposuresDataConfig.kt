package base.base_app.core.entities.config.static_config.models

import com.google.android.gms.nearby.exposurenotification.ReportType
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ExposuresDataConfig(
        /**
         * Počet hodín ako často sa majú sťahovať kľúče.
         */
        @SerialName("diagnosisKeysPeriodicFrameHours") val diagnosisKeysPeriodicFrameHours: Long = 6,

        /**
         * Počet hodín po koľkých sú všetky kľúče nevalídne, vyuzité vtedy keď stiahnutie kazdých X (diagnosisKeysPeriodicFrameHours) bolo vypnuté / niečo zlyhalo.
         */
        @SerialName("keyImportDataOutdatedHours") val keyImportDataOutdatedHours: Int = 24,

        /**
         * Počet dní po koľkých sa majú znova nastaviť DiagnosisKeysDataMapping, pri intervale menej ako 7 dní sa nič nezmení.
         */
        @SerialName("diagnosisKeysDataMappingLimitDays") val diagnosisKeysDataMappingLimitDays: Int = 7,

        /**
         * Zoznam 28 dní predstavujúcich záznam nákazlivosti od stretu s nakazou,
         * int NONE = 0;
         * int STANDARD = 1;
         * int HIGH = 2;
         */
        @SerialName("daysSinceOnsetToInfectiousness") val daysSinceOnsetToInfectiousness: List<Int> = listOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),

        /**
         * Základná hodnota keď stiahnuté kľúče nemajú typ pri posielaní.
         * int UNKNOWN = 0;
         * int CONFIRMED_TEST = 1;
         * int CONFIRMED_CLINICAL_DIAGNOSIS = 2;
         * int SELF_REPORT = 3;
         * int RECURSIVE = 4;
         * int REVOKED = 5;
         */
        @ReportType @SerialName("reportTypeWhenMissing") val reportTypeWhenMissing: Int = 1,

        /**
         * Váha pridaná každému typu verifikovaných zdieľaných TEK kľúčov pre
         * int CONFIRMED_TEST = 1;
         * int CONFIRMED_CLINICAL_DIAGNOSIS = 2;
         * int SELF_REPORT = 3;
         * int RECURSIVE = 4;
         */
        @SerialName("reportTypeWeights") val reportTypeWeights: List<Double> = listOf(1.0, 1.0, 1.0, 1.0),

        /**
         * Prah sily spojenia Bluetooth pri styku, - okamžitý, blízky a stredný prah. (0 < X < 255)
         */
        @SerialName("attenuationBucketThresholdDb") val attenuationBucketThresholdDb: List<Int> = listOf(55, 63, 75),
        @SerialName("attenuationBucketWeights") val attenuationBucketWeights: List<Double> = listOf(1.5, 1.0, 0.16, 0.0),

        /**
         * Váha pre hodnotu nákazlivosti.  (0 < X < 2.5)
         * int NONE = 0;
         * int STANDARD = 1;
         * int HIGH = 2;
         */
        @SerialName("infectiousnessWeights") val infectiousnessWeights: List<Double> = listOf(1.0, 1.0, 1.0),

        /**
         * Hodnota ktorá filtruje výpočet hodnoty nákazlivosti stretnutia podľa algoritmu.
         * príklad : HIGH infectiousness x 100% x Váha nákazlivosti x Cas v Minutach x Sila bluetooth spojenia (Blízkosť stretnutia)
         * 2 x 100% x 1.0 x 5min x 1.5 (blizke stretnutie) = 1500.0
         */
        @SerialName("minimumWindowScore") val minimumWindowScore: Double = 850.0,

        /**
         * Perióda ako často sa má prezerať nastavenie aplikácie či je ExposureNotificationsAPI povolené + či sa kľúče stiahli ta posledných (keyImportDataOutdatedHours) hodín. - obe udalosti sprevádzajú lokálne notifikácie - OUTDATED_DATA, EXPOSURE_NOT_RUNNING.
         */
        @SerialName("selfCheckerPeriodHours") val selfCheckerPeriodHours: Long = 4,

        /**
         * Hodiny od kedy do kedy sa môžu zobrazovať notifikácie o stave aplikácie.
         */
        @SerialName("selfCheckerHours") val selfCheckerHours: List<Int> = listOf(8, 20)
)