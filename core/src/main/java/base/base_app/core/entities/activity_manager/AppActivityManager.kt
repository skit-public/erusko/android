package base.base_app.core.entities.activity_manager

import android.app.Activity
import android.app.Application
import android.os.Bundle
import base.base_app.core.base.BaseActivity
import java.lang.ref.WeakReference
import javax.inject.Inject

interface AppActivityManager {
    fun getCurrentActivity(): BaseActivity?
    fun setCurrentActivity(activity: BaseActivity?)
    fun registerActivityLifecycleHelper(application: Application)
}

class AppActivityManagerImpl @Inject constructor(

) : AppActivityManager {

    private var currentActivity: WeakReference<BaseActivity?>? = null

    override fun getCurrentActivity(): BaseActivity? {
        return currentActivity?.get()
    }

    override fun setCurrentActivity(activity: BaseActivity?) {
        this.currentActivity = WeakReference(activity)
    }

    override fun registerActivityLifecycleHelper(application: Application) {
        application.registerActivityLifecycleCallbacks(object :
                Application.ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity) {}

            override fun onActivityStarted(activity: Activity) {
                setCurrentActivity(activity as? BaseActivity?)
            }

            override fun onActivityDestroyed(activity: Activity) {}

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

            override fun onActivityStopped(activity: Activity) {}

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                setCurrentActivity(activity as? BaseActivity?)
            }

            override fun onActivityResumed(activity: Activity) {
                setCurrentActivity(activity as? BaseActivity?)
            }

        })
    }
}