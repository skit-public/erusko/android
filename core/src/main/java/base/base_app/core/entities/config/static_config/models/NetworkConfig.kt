package base.base_app.core.entities.config.static_config.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkConfig(
        @SerialName("networkConnectionSpecSecure") val networkConnectionSpecSecure: Boolean = true,
        @SerialName("isChuckerEnabled") val isChuckerEnabled: Boolean = false,
        @SerialName("networkConnectTimeout") val networkConnectTimeout: Long = 30000,
        @SerialName("networkReadTimeout") val networkReadTimeout: Long = 30000,
        @SerialName("networkWriteTimeout") val networkWriteTimeout: Long = 30000
)