package base.base_app.core.features.exposure.models.verification_server

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class VerifyCodeResponse(
        @SerialName("padding")
        val padding: String?,
        @SerialName("testtype")
        private val testTypeLowerCase: String? = null,
        @SerialName("testType")
        private val testTypeUpper: String? = null,
        @SerialName("symptomDate")
        val symptomDate: String?,
        @SerialName("token")
        val token: String?
) {
    val testTypePublic = testTypeLowerCase ?: testTypeUpper
}