package base.base_app.core.features.exposure.models.key_sever

import base.base_app.core.helpers.newRandomPadding
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ExposureRequest(
        @SerialName("temporaryExposureKeys")
        val temporaryExposureKeys: List<TemporaryExposureKeyDto>,
        @SerialName("verificationPayload")
        val verificationPayload: String?,
        @SerialName("hmackey")
        val hmackey: String?,
        @SerialName("symptomOnsetInterval")
        val symptomOnsetInterval: Int?,
        @SerialName("revisionToken")
        val revisionToken: String?,
        @SerialName("padding")
        val padding: String = newRandomPadding(),
        @SerialName("traveler")
        val traveler: Boolean = false,
        @SerialName("healthAuthorityID")
        val healthAuthorityID: String
)

@Serializable
data class TemporaryExposureKeyDto(
        @SerialName("key")
        val key: String,
        @SerialName("rollingStartNumber")
        val rollingStartNumber: Int,
        @SerialName("rollingPeriod")
        val rollingPeriod: Int,
        @SerialName("transmissionRisk")
        val transmissionRisk: Int
)

@Serializable
data class ExposureResponse(
        @SerialName("revisionToken")
        val revisionToken: String? = null,
        @SerialName("insertedExposures")
        val insertedExposures: Int? = null,
)