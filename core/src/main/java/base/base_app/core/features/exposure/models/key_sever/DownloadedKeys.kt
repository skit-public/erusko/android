package base.base_app.core.features.exposure.models.key_sever

import java.io.File

data class DownloadedKeys(
        val keys: List<KeyData>
)

data class KeyData(
        val file: File,
        val tag: String?
)