package base.base_app.core.features.workers

interface ExposuresWorkerScheduler {
    fun scheduleWorkers()
}