package base.base_app.core.features.exposure

import android.content.Context
import com.google.android.gms.nearby.exposurenotification.DailySummary
import com.google.android.gms.nearby.exposurenotification.ExposureWindow

interface ExposuresInteractor {
    suspend fun start()
    suspend fun stop()
    suspend fun isEnabled(): Boolean
    suspend fun getDailySummaries(): List<DailySummary>
    suspend fun getExposureWindows(): List<ExposureWindow>
    suspend fun reportExposureWithVerification(code: String): Int
    suspend fun isEligibleToDownloadKeys(): Boolean
    suspend fun downloadKeysAndReportExposures()
    fun notifyAboutNewExposures(context: Context)
    fun deviceSupportsLocationlessScanning(): Boolean
    fun deleteFiles()
}