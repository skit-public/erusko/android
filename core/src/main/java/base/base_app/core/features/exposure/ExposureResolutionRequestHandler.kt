package base.base_app.core.features.exposure

import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.ActivityResultRegistry
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import javax.inject.Inject

interface ExposureResolutionRequestHandler {
    fun registerListener(registry: ActivityResultRegistry, resultListener: (ActivityResult) -> Unit): LifecycleObserver
    fun getLauncher(): ActivityResultLauncher<IntentSenderRequest>?
}

class ExposureResolutionRequestHandlerImpl @Inject constructor() : ExposureResolutionRequestHandler {

    private var exposureResolutionListener: ExposureResolutionListener? = null

    override fun registerListener(registry: ActivityResultRegistry, resultListener: (ActivityResult) -> Unit): LifecycleObserver {
        return ExposureResolutionListener(registry, resultListener).also {
            exposureResolutionListener = it
        }
    }

    override fun getLauncher() = exposureResolutionListener?.getLauncher()
}

class ExposureResolutionListener(private val registry: ActivityResultRegistry, private val resultListener: (ActivityResult) -> Unit) : DefaultLifecycleObserver {
    private var registeredHandler: ActivityResultLauncher<IntentSenderRequest>? = null

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        registeredHandler = registry.register("ResolutionResult", ActivityResultContracts.StartIntentSenderForResult()) {
            resultListener.invoke(it)
        }
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        registeredHandler?.unregister()
        registeredHandler = null
    }

    fun getLauncher() = registeredHandler
}