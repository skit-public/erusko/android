package base.base_app.core.features.exposure.models.verification_server

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class VerifyCertificateRequest(
        @SerialName("token")
        val token: String,
        @SerialName("ekeyhmac")
        val ekeyhmac: String
)