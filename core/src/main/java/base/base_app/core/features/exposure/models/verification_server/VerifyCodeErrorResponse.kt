package base.base_app.core.features.exposure.models.verification_server

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.io.IOException


open class VerificationResponseException(val errorResponse: VerifyCodeErrorResponse) : IOException() {
    override val message: String?
        get() = "Error response: $errorResponse\n${super.message}"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (errorResponse.errorCode != (other as? VerificationResponseException)?.errorResponse?.errorCode) return false
        if (errorResponse.error != (other as? VerificationResponseException)?.errorResponse?.error) return false
        return true
    }

    override fun hashCode(): Int {
        var result = errorResponse.errorCode.hashCode()
        result = 31 * result + errorResponse.error.hashCode()
        return result
    }
}


@Serializable
data class VerifyCodeErrorResponse(
        @SerialName("error")
        val error: String?,
        @SerialName("errorCode")
        val errorCode: VerifyCodeErrorEnum?,
)

@Serializable
enum class VerifyCodeErrorEnum {
    @SerialName("unparsable_request")
    UNPARSABLE_REQUEST,

    @SerialName("code_invalid")
    CODE_INVALID,

    @SerialName("code_expired")
    CODE_EXPIRED,

    @SerialName("code_not_found")
    CODE_NOT_FOUND,

    @SerialName("invalid_test_type")
    INVALID_TEST_TYPE,

    @SerialName("missing_date")
    MISSING_DATE,

    @SerialName("unsupported_test_type")
    UNSUPPORTED_TEST_TYPE,

    @SerialName("token_invalid")
    TOKEN_INVALID,

    @SerialName("token_expired")
    TOKEN_EXPIRED,

    @SerialName("hmac_invalid")
    HMAC_INVALID
}