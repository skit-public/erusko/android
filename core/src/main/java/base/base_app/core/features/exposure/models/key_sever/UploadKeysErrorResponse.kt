package base.base_app.core.features.exposure.models.key_sever

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.io.IOException

open class KeyServerResponseException(val errorResponse: UploadKeysErrorResponse) : IOException() {
    override val message: String?
        get() = "Error response: $errorResponse\n${super.message}"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (errorResponse.errorCode != (other as? KeyServerResponseException)?.errorResponse?.errorCode) return false
        if (errorResponse.error != (other as? KeyServerResponseException)?.errorResponse?.error) return false
        return true
    }

    override fun hashCode(): Int {
        var result = errorResponse.errorCode.hashCode()
        result = 31 * result + errorResponse.error.hashCode()
        return result
    }
}


@Serializable
data class UploadKeysErrorResponse(
        @SerialName("error")
        val error: String?,
        @SerialName("code")
        val errorCode: UploadKeysErrorEnum?,
)

@Serializable
enum class UploadKeysErrorEnum {
    // Error Code definitions.
    // ErrorUnknownHealthAuthorityID indicates that the health authority was not found.
    @SerialName("unknown_health_authority_id")
    UNKNOWN_HEALTH_AUTHORITY_ID,

    // ErrorUnableToLoadHealthAuthority indicates a retryable error loading the configuration.
    @SerialName("unable_to_load_health_authority")
    UNABLE_TO_LOAD_HEALTH_AUTHORITY,

    // ErrorHealthAuthorityMissingRegionConfiguration indicautes the request can not accepted because
    // the specified health authority is not configured correctly.
    @SerialName("health_authority_missing_region_config")
    HEALTH_AUTHORITY_MISSING_REGION_CONFIG,

    // ErrorVerificationCertificateInvalid indicates a problem with the verification certificate.
    @SerialName("health_authority_verification_certificate_invalid")
    HEALTH_AUTHORITY_VERIFICATION_CERTIFICATE_INVALID,

    // ErrorBadRequest indicates that the client sent a request that couldn't be parsed correctly
    // or otherwise contains invalid data, see the extended ErrorMessage for details.
    @SerialName("bad_request")
    BAD_REQUEST,

    // ErrorInternalError
    @SerialName("internal_error")
    INTERNAL_ERROR,

    // ErrorMissingRevisionToken indicates no reivison token passed when one is needed
    @SerialName("missing_revision_token")
    MISSING_REVISION_TOKEN,

    // ErrorInvalidRevisionToken indicates a revision token was passed, but is missing a
    // key or has invalid metadata.
    @SerialName("invalid_revision_token")
    INVALID_REVISION_TOKEN,

    // ErrorKeyAlreadyRevised indicates one of the uploaded TEKs was marked for
    // revision, but it has already been revised.
    @SerialName("key_already_revised")
    KEY_ALREADY_REVISED,

    // ErrorInvalidReportTypeTransition indicates an uploaded TEK tried to
    // transition to an invalid state (like "positive" -> "likely").
    @SerialName("invalid_report_type_transition")
    INVALID_REPORT_TYPE_TRANSITION,

    // ErrorPartialFailure indicates that some exposure keys in the publish
    // request had invalid data (size, timing metadata) and were dropped. Other
    // keys were saved.
    @SerialName("partial_failure")
    PARTIAL_FAILURE,
}