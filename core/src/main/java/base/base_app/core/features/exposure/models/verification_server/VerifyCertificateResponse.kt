package base.base_app.core.features.exposure.models.verification_server

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class VerifyCertificateResponse(
        @SerialName("padding")
        val padding: String? = null,
        @SerialName("certificate")
        val certificate: String
)