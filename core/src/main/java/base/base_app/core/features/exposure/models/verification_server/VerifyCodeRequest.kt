package base.base_app.core.features.exposure.models.verification_server

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class VerifyCodeRequest(
        @SerialName("code")
        val code: String,
        @SerialName("padding")
        val padding: String?
)