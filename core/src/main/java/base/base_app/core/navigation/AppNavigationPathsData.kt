package base.base_app.core.navigation

import base.base_app.core.base.AppNavigationPaths

sealed class AppNavigationPathsData : AppNavigationPaths {
    object Start : AppNavigationPathsData()
    object StartWithPrompt : AppNavigationPathsData()
}