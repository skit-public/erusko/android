package base.base_app.core.di

import base.base_app.core.entities.activity_manager.AppActivityManager
import base.base_app.core.entities.activity_manager.AppActivityManagerImpl
import base.base_app.core.entities.config.static_config.remote_config.RemoteConfigManager
import base.base_app.core.entities.config.static_config.remote_config.RemoteConfigManagerImpl
import base.base_app.core.entities.error_handler.ErrorHandler
import base.base_app.core.entities.error_handler.ErrorHandlerImpl
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.EventTrackerImpl
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.entities.preferences.PersistentAppPreferencesImpl
import base.base_app.core.features.exposure.ExposureResolutionRequestHandler
import base.base_app.core.features.exposure.ExposureResolutionRequestHandlerImpl
import base.base_app.core.helpers.BatterySaverManager
import base.base_app.core.helpers.BatterySaverManagerImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class CoreAppModule {

    @Singleton
    @Provides
    fun provideRemoteConfigManager(impl: RemoteConfigManagerImpl): RemoteConfigManager = impl

    @Singleton
    @Provides
    fun provideErrorHandler(impl: ErrorHandlerImpl): ErrorHandler = impl

    @Singleton
    @Provides
    fun provideAppActivityManager(impl: AppActivityManagerImpl): AppActivityManager = impl

    @Singleton
    @Provides
    fun providePersistentAppPreferences(impl: PersistentAppPreferencesImpl): PersistentAppPreferences = impl

    @Singleton
    @Provides
    fun provideBatterySaverHelper(impl: BatterySaverManagerImpl): BatterySaverManager = impl
}

@Module
@InstallIn(ApplicationComponent::class)
abstract class CoreAppBindingModule {

    @Singleton
    @Binds
    abstract fun provideExposureResolutionRequestHandler(impl: ExposureResolutionRequestHandlerImpl): ExposureResolutionRequestHandler

}

@Module
@InstallIn(ApplicationComponent::class)
abstract class AnalyticsModule {
    @Singleton
    @Binds
    abstract fun provideEventTracker(eventTrackerImpl: EventTrackerImpl): EventTracker
}