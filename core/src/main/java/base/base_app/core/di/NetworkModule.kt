package base.base_app.core.di

import android.content.Context
import base.base_app.core.BuildConfig
import base.base_app.core.entities.chucker.Chucker
import base.base_app.core.entities.chucker.ChuckerImpl
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.helpers.Logger
import com.chuckerteam.chucker.api.ChuckerInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideChucker(impl: ChuckerImpl): Chucker = impl

    @Singleton
    @Provides
    fun provideBaseOkHttp(
            @ApplicationContext context: Context,
            configProvider: ConfigProvider,
            chucker: Chucker
    ): OkHttpClient {
        return OkHttpClient.Builder()
                .connectionSpecs(if (configProvider.provide().networkConfig().networkConnectionSpecSecure) listOf(ConnectionSpec.MODERN_TLS) else listOf(ConnectionSpec.CLEARTEXT, ConnectionSpec.MODERN_TLS))
                .apply {
                    addInterceptor(HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                        override fun log(message: String) {
                            Logger.dm { message }
                        }
                    }).apply { level = HttpLoggingInterceptor.Level.BODY })
                }
                .apply {
                    //Leave this as is because of -no-op Chucker for release has strange constructor errors
                    if (BuildConfig.DEBUG && configProvider.provide().networkConfig().isChuckerEnabled) {
                        addNetworkInterceptor(
                                ChuckerInterceptor(
                                        context,
                                        chucker.getCollector(),
                                        250000L,
                                        mutableSetOf<String>())
                        )
                    }
                }
                .connectTimeout(configProvider.provide().networkConfig().networkConnectTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(configProvider.provide().networkConfig().networkReadTimeout, TimeUnit.MILLISECONDS)
                .writeTimeout(configProvider.provide().networkConfig().networkWriteTimeout, TimeUnit.MILLISECONDS)
                .build()
    }
}