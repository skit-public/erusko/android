package base.base_app.core.ui.startup

import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import base.base_app.core.base.AppNavigator
import base.base_app.core.base.BaseActivity
import base.base_app.core.base.vm.renderCustomEvents
import base.base_app.core.base.vm.renderViewState
import base.base_app.core.helpers.PlayServicesResult
import base.base_app.core.navigation.AppNavigationPathsData
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class StartupActivity : BaseActivity() {

    @Inject
    lateinit var appNavigator: AppNavigator

    private val viewModel: StartViewModel by viewModels()

    override fun getLayoutResId(): Int? = null

    override fun init(savedInstanceState: Bundle?) {
        viewModel.renderCustomEvents<StartEvents>(lifecycleScope) {
            when (it) {
                is StartEvents.NavigateToDashboard -> appNavigator.navigateTo(AppNavigationPathsData.Start, true)
                is StartEvents.NavigateToFirstActivation -> appNavigator.navigateTo(AppNavigationPathsData.Start, true)
            }
        }

        viewModel.renderViewState(lifecycleScope) {
            when {
                it.showRemoteConfigIssue -> appNavigator.navigateTo(AppNavigationPathsData.StartWithPrompt, true)
                it.showPlayServicesDialog == PlayServicesResult.Error -> appNavigator.navigateTo(AppNavigationPathsData.StartWithPrompt, true)
                it.showMandatory -> appNavigator.navigateTo(AppNavigationPathsData.StartWithPrompt, true)
            }
        }

        viewModel.refreshStates()
    }
}