package base.base_app.core.ui.main

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import base.base_app.core.R
import base.base_app.core.base.BaseActivity
import base.base_app.core.base.IntentKeys
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.preferences.IntroProgress
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.ui.makeStatusBarTransparent
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    @Inject
    lateinit var persistentAppPreferences: PersistentAppPreferences

    @Inject
    lateinit var eventTracker: EventTracker

    override fun getLayoutResId(): Int? = R.layout.activity_main

    override fun getNavHostFragmentId(): Int? = R.id.main_navHostFragment

    override fun init(savedInstanceState: Bundle?) {
        initNavigation()
        makeStatusBarTransparent()
    }

    private fun initNavigation() {
        val hostFragment = supportFragmentManager.findFragmentById(R.id.main_navHostFragment) as NavHostFragment?
                ?: return

        val graphInflater = hostFragment.navController.navInflater
        val navGraph = graphInflater.inflate(R.navigation.main_nav_graph)
        val navController = hostFragment.navController

        val startDestination = when {
            intent.getBooleanExtra(IntentKeys.SHOW_PROMPT, false) -> {
                R.id.startPromptFragment
            }
            persistentAppPreferences.isFirstActivationFlow -> {
                when (persistentAppPreferences.introProgress) {
                    IntroProgress.WELCOME -> R.id.introFragment
                    IntroProgress.PERMISSIONS -> R.id.activationFragment
                    IntroProgress.TERMS_AND_CONDITIONS -> R.id.privacyFragment
                }
            }
            else -> {
                R.id.dashboardFragment
            }
        }
        navGraph.startDestination = startDestination
        navController.graph = navGraph
    }
}