package base.base_app.core.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

fun BetterAdapter.setDataWithDiff(newList: List<Any>) {
    DiffUtil.calculateDiff(object : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = data[oldItemPosition] == newList[newItemPosition]
        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = data[oldItemPosition] == newList[newItemPosition]
        override fun getOldListSize() = data.size
        override fun getNewListSize() = newList.size
    }).dispatchUpdatesTo(this)
    data = newList.toMutableList()
}

open class BetterAdapter : RecyclerView.Adapter<BetterAdapter.BetterViewHolder<*, Holder<*>>> {

    private val calculateShowedItems: Boolean
    private val defaultBinders: List<Binder<*, Holder<*>>>
    private val creators = mutableListOf<() -> Binder<*, Holder<*>>>()
    private val itemsShowed = mutableSetOf<Any>()

    var data: MutableList<Any> = mutableListOf()
        get() = synchronized(this) { field }
        set(value) {
            synchronized(this) {
                field.clear()
                field.addAll(value)
            }
        }

    constructor(
            vararg binderCreators: () -> Binder<*, Holder<*>>,
            calculateShowed: Boolean = false
    ) : this(binders = *binderCreators.map { it() }.toTypedArray(), calculateShowed = calculateShowed) {
        creators.addAll(binderCreators)
    }

    @Deprecated("use secondary constructor to avoid cloning of the binders")
    constructor(
            vararg binders: Binder<*, Holder<*>>,
            calculateShowed: Boolean = false
    ) : super() {
        calculateShowedItems = calculateShowed
        defaultBinders = binders.toList()
    }

    override fun onBindViewHolder(holder: BetterViewHolder<*, Holder<*>>, position: Int) {
        if (calculateShowedItems) {
            itemsShowed.add(data[position])
        }
        holder.bind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BetterViewHolder<*, Holder<*>> {
        val clonedBinder = defaultBinders[viewType].cloneHolder()
        val holder = (creators.getOrNull(viewType)?.invoke()
                ?: clonedBinder) as Binder<*, *>
        val view = LayoutInflater.from(parent.context)
                .inflate(holder.getLayoutResId(), parent, false)
                .also { holder.onInflate(it) }
        return BetterViewHolder(view = view, holder = holder as Holder<*>).also { holder.initHolder(view) }
    }

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int {
        val obj = data[position]

        for (index in defaultBinders.indices) {
            if (!defaultBinders[index].isMyType(obj)) continue
            return index
        }

        throw BinderException(position, obj::class.java)
    }

    override fun onViewRecycled(holder: BetterViewHolder<*, Holder<*>>) {
        holder.itemView.clearAnimation()
        super.onViewRecycled(holder)
    }

    open inner class BetterViewHolder<T, out H : Holder<T>>(
            view: View,
            internal val holder: H
    ) : RecyclerView.ViewHolder(view) {

        fun bind(position: Int) {
            getItem(position)?.also { holder.bind(position, it) }
        }

        open fun getItem(position: Int): T? {
            return data.getOrNull(position) as T?
        }
    }
}

/**
 * Binder is used for providing layout and creating holder for loaded view
 */
interface Binder<in T, out H : Holder<T>> {
    fun getLayoutResId(): Int
    fun cloneHolder(): H
    fun initHolder(itemView: View)
    fun isMyType(obj: Any): Boolean
    fun onInflate(view: View)
}

/**
 * Holder is used for finding, storing view and binding its specific type of item to this views
 */
interface Holder<in T> {
    fun bind(position: Int, item: T)
    fun onViewRecycled()
}

/**
 * Holder in which initialization of views is wanted
 */
abstract class EmptyBinderHolder<T>(
        private val layoutResId: Int,
        private val clazz: Class<T>
) : Holder<T>, Binder<T, EmptyBinderHolder<T>>, Cloneable {

    lateinit var itemView: View

    override fun onInflate(view: View) {}
    override fun getLayoutResId(): Int = layoutResId
    override fun isMyType(obj: Any): Boolean = obj.javaClass.isClassInstance(clazz)
    override fun bind(position: Int, item: T) {}

    override fun cloneHolder(): EmptyBinderHolder<T> {
        return clone() as EmptyBinderHolder<T>
    }

    override fun initHolder(itemView: View) {
        this.itemView = itemView
        init()
    }

    protected open fun init() {}

    override fun onViewRecycled() {}

    private fun Class<*>.isClassInstance(clazz: Class<*>): Boolean {
        return when {
            this == clazz -> true
            this == Object::class.java -> false
            else -> superclass?.isClassInstance(clazz) ?: false
        }
    }
}

class BinderException(position: Int, clazz: Class<*>) : RuntimeException("No binder found for position: $position, type: $clazz")