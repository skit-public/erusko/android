package base.base_app.core.ui.webview

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.navigation.fragment.navArgs
import base.base_app.core.R
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.ui.hide
import base.base_app.core.ui.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_webview.*

@AndroidEntryPoint
class WebViewFragment : BaseFragment() {

    private val args: WebViewFragmentArgs by navArgs()

    override fun getStaticContentResId(): Int = R.layout.fragment_webview

    override fun getToolbarConfig(): ToolbarConfig? = null

    override fun init(view: View, savedInstanceState: Bundle?) {
        wv_content.webViewClient = AppWebViewClient()
        wv_content.loadUrl(args.url)
    }

    override fun render() {}

    inner class AppWebViewClient : WebViewClient() {
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            if (url == args.url) {
                pb_loading.show()
            }
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            pb_loading.hide()
        }
    }

}