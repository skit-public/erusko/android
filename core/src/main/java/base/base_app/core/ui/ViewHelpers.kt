package base.base_app.core.ui

import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.view.View
import android.view.WindowManager
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import base.base_app.core.base.BaseActivity
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.throttleFirst
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import ru.ldralighieri.corbind.view.clicks


/**
 * Set visible state of View between VISIBLE and GONE
 *
 * @property show true for VISIBLE, false for GONE
 */
fun View.show(show: Boolean = true) {
    visibility = if (show) View.VISIBLE else View.GONE
}

/**
 * Set visible state of View between GONE and VISIBLE
 *
 * @property hide true for GONE, false for VISIBLE
 */
fun View.hide(hide: Boolean = true) {
    show(!hide)
}

/**
 * Set visible state of View between INVISIBLE and VISIBLE
 *
 * @property invisible true for INVISIBLE, false for VISIBLE
 */
fun View.invisible(invisible: Boolean = true) {
    visibility = if (invisible) View.INVISIBLE else View.VISIBLE
}

/**
 * Set visible state of View between VISIBLE and INVISIBLE
 *
 * @property visible true for VISIBLE, false for INVISIBLE
 */
fun View.visible(visible: Boolean = true) {
    invisible(!visible)
}

fun View.clicksThrottled(scope: CoroutineScope, windowDurationMillis: Long = 300, onClick: () -> Unit) {
    this.clicks()
            .throttleFirst(windowDurationMillis)
            .onEach {
                onClick()
            }.launchIn(scope)
}

fun View.clicksThrottledNextButton(scope: CoroutineScope, onClick: () -> Unit) {
    this.clicksThrottled(scope, 500, onClick)
}

fun BaseActivity.makeStatusBarTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    }
}


/**
 * Convert dp value to px value
 * e.g. 10.dp is 10px on MDPI but 20px on XHDPI
 */
val Int.dpToPx: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun ImageView.setImageResWithAnimation(@DrawableRes res: Int) {
    val newDrawable = ResourcesCompat.getDrawable(resources, res, context.theme)
    if (this.drawable != null) {
        this.animate()
                .scaleX(0.8f)
                .scaleY(0.8f)
                .setInterpolator(DecelerateInterpolator())
                .withEndAction {
                    this.setImageDrawable(newDrawable)
                    this.animate()
                            .scaleX(1f)
                            .scaleY(1f)
                            .setDuration(150)
                            .setInterpolator(AccelerateInterpolator())
                            .start()
                }
                .setDuration(150)
                .start()
    } else {
        this.setImageDrawable(newDrawable)
    }
}


fun TextView.setPartlyBoldText(originalString: String, boldSurrounding: String = "$$") {
    this.text = try {
        SpannableStringBuilder(originalString).apply {
            setSpan(StyleSpan(Typeface.BOLD), originalString.indexOf(boldSurrounding), originalString.lastIndexOf(boldSurrounding), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //replace both $$ occurrences
            var hasDollars = contains(boldSurrounding)
            while (hasDollars) {
                replace(indexOf(boldSurrounding), indexOf(boldSurrounding) + boldSurrounding.length, "")
                hasDollars = contains(boldSurrounding)
            }
        }
    } catch (e: Exception) {
        Logger.et { e }
        originalString
    }
}