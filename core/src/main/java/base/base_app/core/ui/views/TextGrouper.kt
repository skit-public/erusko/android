package base.base_app.core.ui.views

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import base.base_app.core.helpers.ignoreExceptions
import base.base_app.core.helpers.removeAllSpaces

class TextGrouper(private val input: EditText, private val groupCount: Int = 4) : TextWatcher {
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun afterTextChanged(s: Editable) {
        val newText = s.toString().removeAllSpaces().chunked(groupCount).joinToString(separator = " ")
        input.removeTextChangedListener(this)
        input.setText(newText)
        ignoreExceptions { input.setSelection(newText.length) }
        input.addTextChangedListener(this)
    }
}