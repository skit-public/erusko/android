package base.base_app.core.ui.startup

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.OneTimeEvents
import base.base_app.core.base.vm.*
import base.base_app.core.entities.activity_manager.AppActivityManager
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.config.static_config.remote_config.RemoteConfigManager
import base.base_app.core.entities.error_handler.ErrorHandler
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.helpers.*
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import sk.slovenskoit.locale_change_core.LocaleManager

sealed class StartEvents : OneTimeEvents {
    data class NavigateToFirstActivation(val withRestart: Boolean) : StartEvents()
    data class NavigateToDashboard(val withRestart: Boolean) : StartEvents()
}

data class StartViewState(
        val showMandatory: Boolean = false,
        val showPlayServicesDialog: PlayServicesResult = PlayServicesResult.Success,
        val showRemoteConfigIssue: Boolean = false,
        val isLoading: Boolean = false
)

class StartViewModel @ViewModelInject constructor(
        @ApplicationContext private val context: Context,
        private val remoteConfigManager: RemoteConfigManager,
        private val configProvider: ConfigProvider,
        private val activityManager: AppActivityManager,
        private val eventTracker: EventTracker,
        private val errorHandler: ErrorHandler,
        private val persistentAppPreferences: PersistentAppPreferences
) : BaseViewModel<StartViewState>(StartViewState()) {

    private var shouldRestart: Boolean = false

    init {
        registerCustomEvent<StartEvents>()
    }

    fun refreshStates() {
        checkAppAndDeviceState()
    }

    private fun checkAppAndDeviceState() {
        launchOnIo {
            kotlin.runCatching {
                emitViewState {
                    it.copy(isLoading = true)
                }
                if (remoteConfigManager.isAllowedToFetchNewConfig() && ConnectionHelper.isNetworkAvailable(context)) {
                    remoteConfigManager.updateConfig()
                    configProvider.provide().localizations().localizations.forEach {
                        LocaleManager.storeCustomLocales(it)
                        shouldRestart = true
                    }
                }
                if (isRemoteConfigError()) {
                    emitViewState { StartViewState(showRemoteConfigIssue = true) }
                } else {
                    checkConstrains()
                }

            }.onFailure {
                Logger.et { it }
                emitDefaultEvent(DefaultOneTimeEvents.Error(errorHandler.mapError(it)))
            }
        }
    }

    fun resolvePlayServicesError() {
        launchOnIo {
            activityManager.getCurrentActivity()?.let {
                kotlin.runCatching {
                    withContext(Dispatchers.Main) {
                        PlayServicesHelper.makePlayServicesAvailable(it)
                    }
                }.onSuccess {
                    refreshStates()
                }.onFailure {
                    Logger.et { it }
                    emitDefaultEvent(DefaultOneTimeEvents.Error(errorHandler.mapError(it)))
                }
            }
        }
    }

    private fun isRemoteConfigError(): Boolean {
        return !remoteConfigManager.hasSuccessFetch()
    }

    private suspend fun checkConstrains() {
        val playServicesDialog = PlayServicesHelper.getPlayServicesResolver(activityManager.getCurrentActivity())

        val isNewVersionAvailable = VcsHelper.isNewVersionAvailable(
                VcsHelper.getInstalledAppVersion(context),
                VcsHelper.parseVersion(configProvider.provide().vcsConfig().mandatoryVersion)
        )

        if (playServicesDialog != PlayServicesResult.Success) {
            eventTracker.trackEvent(Events.PlayServicesError)
        }

        if (isNewVersionAvailable) {
            eventTracker.trackEvent(Events.MandatoryUpdateError(
                    currentVersion = VcsHelper.getInstalledAppVersion(context)?.toVersionString()
                            ?: "unknown",
                    mandatoryVersion = VcsHelper.parseVersion(configProvider.provide().vcsConfig().mandatoryVersion)?.toVersionString()
                            ?: "unknown"
            ))
        }

        if (!isNewVersionAvailable && playServicesDialog == PlayServicesResult.Success) {
            navigateToApp()
        } else {
            emitViewState {
                StartViewState(
                        showPlayServicesDialog = playServicesDialog,
                        showMandatory = isNewVersionAvailable
                )
            }
        }
    }

    private suspend fun navigateToApp() {
        if (persistentAppPreferences.isFirstActivationFlow) {
            emitCustomEvent(StartEvents.NavigateToFirstActivation(shouldRestart))
        } else {
            emitCustomEvent(StartEvents.NavigateToDashboard(shouldRestart))
        }
    }
}