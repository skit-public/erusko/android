package base.base_app.core.ui.views.spans

import android.content.Context
import android.text.TextPaint
import android.text.style.CharacterStyle

abstract class BaseSpan(val context: Context) : CharacterStyle() {

    abstract fun getForegroundSkinColor(): Int

    override fun updateDrawState(tp: TextPaint) {
        tp.color = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            context.resources.getColor(getForegroundSkinColor(), context.theme)
        } else {
            context.resources.getColor(getForegroundSkinColor())
        }
    }
}