package base.base_app.core.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.core.widget.NestedScrollView

class AppScrollView : NestedScrollView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
}