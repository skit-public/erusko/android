package base.base_app.core.ui.views.spans

import android.content.Context
import base.base_app.core.R

class SpanV1(context: Context) : BaseSpan(context) {
    override fun getForegroundSkinColor(): Int = R.color.clickable_text_v1
}