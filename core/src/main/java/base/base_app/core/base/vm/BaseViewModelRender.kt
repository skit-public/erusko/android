package base.base_app.core.base.vm

import androidx.lifecycle.LifecycleCoroutineScope
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.OneTimeEvents
import base.base_app.core.helpers.renderViewState

inline fun <reified T : Any> BaseViewModel<T>.renderViewState(scope: LifecycleCoroutineScope, noinline onNext: (T) -> Unit = {}) {
    return findViewStateStreamByType<T>().renderViewState(scope, onNext)
}

inline fun <reified T : Any> BaseViewModel<*>.renderCustomViewState(scope: LifecycleCoroutineScope, noinline onNext: (T) -> Unit = {}) {
    return findViewStateStreamByType<T>().renderViewState(scope, onNext)
}

fun BaseViewModel<*>.renderDefaultEvents(scope: LifecycleCoroutineScope, onNext: (DefaultOneTimeEvents) -> Unit = {}) {
    return findEventByType<DefaultOneTimeEvents>().renderViewState(scope, onNext)
}

inline fun <reified T : OneTimeEvents> BaseViewModel<*>.renderCustomEvents(scope: LifecycleCoroutineScope, noinline onNext: (T) -> Unit = {}) {
    return findEventByType<T>().renderViewState(scope, onNext)
}