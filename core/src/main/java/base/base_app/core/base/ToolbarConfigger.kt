package base.base_app.core.base

import androidx.annotation.DrawableRes
import base.base_app.core.helpers.StringResource

data class ToolbarConfig(
        val title: StringResource? = null,
        @DrawableRes val imageResource: Int? = null,
        @DrawableRes val backgroundResource: Int? = null,
        val infoText: StringResource? = null,
        val infoTextClickCallback: (() -> Unit)? = null,
        val isToolbarMotionEnabled: Boolean? = null
)