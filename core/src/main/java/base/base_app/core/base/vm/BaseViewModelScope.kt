package base.base_app.core.base.vm

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*

fun BaseViewModel<*>.launchOnIo(jobStart: CoroutineStart = CoroutineStart.DEFAULT, job: suspend (CoroutineScope) -> Unit): Job {
    return viewModelScope.launch(start = jobStart) {
        withContext(getIoContext()) {
            job(this)
        }
    }
}