package base.base_app.core.base

import base.base_app.core.entities.error_handler.ErrorData

interface OneTimeEvents

sealed class DefaultOneTimeEvents : OneTimeEvents {
    data class Error(val error: ErrorData?) : DefaultOneTimeEvents()
    data class NavigateNext(val data: Any? = null) : DefaultOneTimeEvents()
    data class Loading(val isLoading: Boolean) : DefaultOneTimeEvents()
}
