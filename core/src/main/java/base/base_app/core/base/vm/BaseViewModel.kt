package base.base_app.core.base.vm

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.ViewModel
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.EmitterCollector
import base.base_app.core.base.OneTimeEvents
import base.base_app.core.helpers.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import org.jetbrains.annotations.TestOnly
import java.util.concurrent.ConcurrentHashMap
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

abstract class BaseViewModel<VIEWSTATE : Any> constructor(initialState: VIEWSTATE, private val isLoggingEnabled: Boolean = false) : ViewModel() {

    private var emitterCollector: EmitterCollector? = null
    private var ioContext: CoroutineContext = Dispatchers.IO
    val dataFlows: MutableMap<KClass<*>, MutableStateFlow<Any>> = ConcurrentHashMap()
    val oneTimeEvents: MutableMap<KClass<*>, MutableSharedFlow<OneTimeEvents>> = ConcurrentHashMap()

    init {
        registerCustomViewState(initialState)
        registerCustomEvent<DefaultOneTimeEvents>()
    }

    fun log(message: String) {
        if (isLoggingEnabled) {
            Logger.dm { message }
        }
    }

    fun getEmitterCollector() = emitterCollector

    fun getIoContext() = ioContext

    @TestOnly
    @VisibleForTesting
    fun setIoContext(context: CoroutineContext) {
        this.ioContext = context
    }

    @TestOnly
    @VisibleForTesting
    fun setEmitterCollector(collector: EmitterCollector?) {
        this.emitterCollector = collector
        getEmitterCollector()?.let { emitter ->
            dataFlows.values.forEach {
                emitter.collect(it.value)
            }
        }
    }
}