package base.base_app.core.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import base.base_app.core.R
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.helpers.SpanHelper
import base.base_app.core.helpers.dialog.DialogSheetProvider
import base.base_app.core.helpers.getString
import base.base_app.core.helpers.ignoreExceptions
import base.base_app.core.helpers.orTrue
import base.base_app.core.ui.clicksThrottled
import base.base_app.core.ui.hide
import base.base_app.core.ui.show
import base.base_app.core.ui.views.spans.SpanV1
import kotlinx.android.synthetic.main.fragment_base.*
import sk.slovenskoit.locale_change_core.LocaleManager
import javax.inject.Inject


abstract class BaseFragment : Fragment(), DialogSheetProvider {

    @Inject
    lateinit var configProvider: ConfigProvider

    @Inject
    lateinit var eventTracker: EventTracker

    @LayoutRes
    open fun getScrollContentResId(): Int? = null

    @LayoutRes
    open fun getStaticContentResId(): Int? = null

    @LayoutRes
    open fun getTopStaticContentResId(): Int? = null

    abstract fun getToolbarConfig(): ToolbarConfig?

    abstract fun init(view: View, savedInstanceState: Bundle?)

    abstract fun render()

    protected open fun disableScreenshots(): Boolean = false

    @SuppressLint("NewApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_base, container, false).apply {
            setUpToolbar(getToolbarConfig(), this)
            val scrollView = this.findViewById<ViewGroup>(R.id.scroll)
            val scrollContent = this.findViewById<ViewGroup>(R.id.scroll_content)
            val staticView = this.findViewById<ViewGroup>(R.id.static_content)
            val staticViewTop = this.findViewById<ViewGroup>(R.id.static_content_top)
            val contentContainer = this.findViewById<MotionLayout>(R.id.cl_container)

            scrollView.hide(getScrollContentResId() == null)
            staticView.hide(getStaticContentResId() == null)
            staticViewTop.hide(getTopStaticContentResId() == null)

            getScrollContentResId()?.let {
                inflater.inflate(it, scrollContent, true)
                (scrollView as? NestedScrollView?)?.apply {
                    isNestedScrollingEnabled = true
                    setOnScrollChangeListener { _, _, scrollY, _, _ ->
                        val progress = if (scrollY <= 500) {
                            ((scrollY.toFloat() - 0f) * 100f) / (500f - 0f) / 100f
                        } else {
                            1f
                        }
                        if (getToolbarConfig()?.isToolbarMotionEnabled.orTrue()) {
                            contentContainer?.setInterpolatedProgress(progress)
                        }
                    }
                }
            }

            getTopStaticContentResId()?.let { inflater.inflate(it, staticViewTop, true) }

            getStaticContentResId()?.let {
                inflater.inflate(it, staticView, true)
                if (getScrollContentResId() == null) {
                    val constraintSet = ConstraintSet()
                    constraintSet.clone(contentContainer)
                    constraintSet.connect(R.id.static_content, ConstraintSet.TOP, R.id.static_content_top, ConstraintSet.BOTTOM, 0)
                    constraintSet.constrainDefaultHeight(R.id.static_content, ConstraintSet.MATCH_CONSTRAINT)
                    constraintSet.constrainHeight(R.id.static_content, ConstraintSet.MATCH_CONSTRAINT)
                    constraintSet.applyTo(contentContainer)
                }
            }
            enableMotion()
        }
    }

    private fun enableMotion() {
        view?.let {
            val motion = it.findViewById<MotionLayout>(R.id.cl_container)

            if (motion != null) {
                val isEnabled = getScrollContentResId() != null && getToolbarConfig()?.isToolbarMotionEnabled.orTrue()
                motion.definedTransitions.orEmpty().forEach {
                    it.setEnable(isEnabled)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        render()
        init(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        LocaleManager.fragmentOnResume(requireContext())
        refreshToolbar()
        eventTracker.trackEvent(Events.ScreenResumed(this::class.java.simpleName))

        if (disableScreenshots() && configProvider.provide().generalConfig().disableScreenshots) {
            activity?.window?.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
        } else {
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
        }
    }

    override fun onGetLayoutInflater(savedInstanceState: Bundle?): LayoutInflater {
        val wrappedContext = LocaleManager.fragmentWrapContext(requireContext())
        return wrappedContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }


    override fun provideDialogSheet(layoutResId: Int): View? {
        return (activity as? DialogSheetProvider?)?.provideDialogSheet(layoutResId)
    }

    override fun provideBottomInset(): Int? {
        return getBaseActivity()?.provideBottomInset()
    }

    fun getBaseActivity(): BaseActivity? = activity as? BaseActivity

    protected fun refreshToolbar() {
        setUpToolbar(getToolbarConfig())
    }

    protected fun showLoading(show: Boolean) {
        pb_loading.show(show)
    }

    private fun setUpToolbar(config: ToolbarConfig?, view: View? = null) {
        context?.let { context ->
            (view ?: getView())?.findViewById<TextView>(R.id.tb_title)?.apply {
                text = config?.title?.getString(context)
                show(config?.title != null)
            }

            (view ?: getView())?.findViewById<ImageView>(R.id.tb_image)?.apply {
                config?.imageResource?.let { this.setImageResource(it) }
                show(config?.imageResource != null)
            }

            (view ?: getView())?.findViewById<ImageView>(R.id.tb_background_image)?.apply {
                config?.backgroundResource?.let { this.setImageResource(it) }
                show(config?.backgroundResource != null)
            }

            (view ?: getView())?.findViewById<TextView>(R.id.tv_info)?.apply {
                text = config?.infoText?.getString(context)
                config?.infoTextClickCallback?.let { infoClickCallback ->
                    text = SpanHelper.replaceFullText(this.text, SpanHelper.SpanParam(SpanHelper.OnClickSpan(this, {
                        infoClickCallback()
                    }, drawUnderLine = false), SpanV1(context)))
                }
                show(config?.infoText != null)
            }

            val backText = (view ?: getView())?.findViewById<View>(R.id.tb_back_text)
            val backIcon = (view ?: getView())?.findViewById<View>(R.id.tb_back)
            val backContainer = (view ?: getView())?.findViewById<ViewGroup>(R.id.back_container)

            val hideBack = ignoreExceptions { findNavController() }?.let {
                it.previousBackStackEntry == null
            } ?: false

            backText?.hide(hideBack)
            backIcon?.hide(hideBack)

            backContainer?.clicksThrottled(lifecycleScope) {
                getBaseActivity()?.onBackPressed()
            }

            enableMotion()
        }
    }
}