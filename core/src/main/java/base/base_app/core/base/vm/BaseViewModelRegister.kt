package base.base_app.core.base.vm

import base.base_app.core.base.OneTimeEvents
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow

fun BaseViewModel<*>.registerCustomViewState(initialData: Any) {
    log("Registering data flow for type: ${initialData::class.java.simpleName}, in: ${this::class.java.simpleName}")
    dataFlows[initialData::class] = (MutableStateFlow(initialData))
}

inline fun <reified T : OneTimeEvents> BaseViewModel<*>.registerCustomEvent() {
    log("Registering one time event for type: ${T::class.java.simpleName} in  ${this::class.java.simpleName}")
    oneTimeEvents[T::class] = MutableSharedFlow()
}