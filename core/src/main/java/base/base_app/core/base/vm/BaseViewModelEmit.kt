package base.base_app.core.base.vm

import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.OneTimeEvents

inline fun <reified T : Any> BaseViewModel<T>.emitViewState(newState: (T) -> T) {
    emitCustomViewState<T>(newState)
}

inline fun <reified T : Any> BaseViewModel<*>.emitCustomViewState(newState: (T) -> T) {
    findViewStateStreamByType<T>()?.also {
        val nextState = newState(it.value)
        log("Previous state:\n${it.value}\nEmitted state:\n${nextState}")
        it.value = nextState
        getEmitterCollector()?.collect(nextState)
    } ?: log("Skipping emitted state:\nregistered:${dataFlows}\n${T::class.java.simpleName}}")
}

suspend fun BaseViewModel<*>.emitDefaultEvent(event: DefaultOneTimeEvents) {
    emitCustomEvent(event)
}

suspend inline fun <reified T : OneTimeEvents> BaseViewModel<*>.emitCustomEvent(event: T) {
    findEventByType<T>()?.also {
        log("Posting one time event: $event")
        it.emit(event)
        getEmitterCollector()?.collect(event)
    } ?: log("Skipping one time event:\nregistered:$oneTimeEvents\nemitting:${T::class}")
}