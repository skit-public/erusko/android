package base.base_app.core.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle

interface BaseNavigator<NAVIGATION_PATHS> {
    fun navigateTo(navigationPath: NAVIGATION_PATHS, finishAll: Boolean = false)

    fun startActivity(context: Context, intent: Intent, finishAll: Boolean, bundle: Bundle? = null) {
        if (finishAll) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        context.startActivity(intent, bundle)
    }

    fun startActivityForResult(activity: Activity, intent: Intent, requestCode: Int) {
        activity.startActivityForResult(intent, requestCode)
    }
}