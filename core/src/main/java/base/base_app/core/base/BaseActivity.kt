package base.base_app.core.base

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.dialog.DialogSheetProvider
import sk.slovenskoit.locale_change_core.LocaleManager

abstract class BaseActivity : AppCompatActivity(), DialogSheetProvider {

    @LayoutRes
    protected abstract fun getLayoutResId(): Int?

    @IdRes
    open fun getNavHostFragmentId(): Int? = null

    protected abstract fun init(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getLayoutResId()?.let {
            setContentView(it)
            findViewById<View>(android.R.id.content).filterTouchesWhenObscured = true
        }

        init(savedInstanceState)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.activityWrapContext(newBase))
    }

    override fun getResources(): Resources {
        return LocaleManager.activityWrapResources(baseContext)
    }

    override fun provideDialogSheet(layoutResId: Int): View? {
        return try {
            layoutInflater.inflate(layoutResId, null)
        } catch (e: Throwable) {
            Logger.et { e }
            null
        }
    }

    override fun provideBottomInset(): Int? {
        return ViewCompat.getRootWindowInsets(findViewById(android.R.id.content))?.systemWindowInsetBottom
    }
}