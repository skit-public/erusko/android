package base.base_app.core.base

/**
 * Emitter collector used for testing purposes to collect all the events and viewStates
 *
 * @constructor Create empty Emitter collector
 */
interface EmitterCollector {
    fun collect(state: Any)
}