package base.base_app.core.base.vm

inline fun <reified T : Any> BaseViewModel<*>.getLastState(): T? {
    return findViewStateStreamByType<T>()?.value
}