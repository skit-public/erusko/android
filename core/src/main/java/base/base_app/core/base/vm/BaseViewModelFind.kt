package base.base_app.core.base.vm

import base.base_app.core.base.OneTimeEvents
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow

inline fun <reified T : Any> BaseViewModel<*>.findViewStateStreamByType(): MutableStateFlow<T>? {
    return (this.dataFlows.toList()
            .firstOrNull {
                it.first.java.isAssignableFrom(T::class.java) || it.first == T::class
            }?.second as? MutableStateFlow<T>?)
            .also {
                if (it == null) {
                    log("ViewStateStream for type ${T::class.java.simpleName} not found")
                }
            }
}

inline fun <reified T : OneTimeEvents> BaseViewModel<*>.findEventByType(): MutableSharedFlow<T>? {
    return (this.oneTimeEvents.toList()
            .firstOrNull {
                it.first.java.isAssignableFrom(T::class.java) || it.first == T::class
            }?.second as? MutableSharedFlow<T>?)
            .also {
                if (it == null) {
                    log("EventStream for type ${T::class.java.simpleName} not found")
                }
            }
}