package base.base_app.core.base

import java.io.Serializable

interface AppNavigator : BaseNavigator<AppNavigationPaths>

interface AppNavigationPaths : Serializable

object IntentKeys {
    const val SHOW_PROMPT = "show_prompt_main_activity"
}