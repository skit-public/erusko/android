package sk.erusko.feature_dashboard

import androidx.hilt.lifecycle.ViewModelInject
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.OneTimeEvents
import base.base_app.core.base.vm.*
import base.base_app.core.entities.error_handler.ErrorHandler
import base.base_app.core.entities.events.CustomProperties
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.features.exposure.ExposuresInteractor
import base.base_app.core.helpers.removeAllSpaces
import base.base_app.core.helpers.toActiveInactive
import base.base_app.core.helpers.toFormattedDate
import kotlinx.coroutines.delay

data class DashboardViewState(
        val isExposureNotificationsEnabled: Boolean? = null,
        val lastExposureDate: String? = null
)

sealed class DashboardEvents() : OneTimeEvents {
    object OpenSubmitWithVerificationCode : DashboardEvents()
    object ShowExposureDisabledError : DashboardEvents()
}

class DashboardViewModel @ViewModelInject constructor(
        private val exposuresInteractor: ExposuresInteractor,
        private val errorHandler: ErrorHandler,
        private val eventTracker: EventTracker
) : BaseViewModel<DashboardViewState>(DashboardViewState()) {

    private var isDeeplinkHandled: Boolean = false

    init {
        registerCustomEvent<DashboardEvents>()
        refreshState()
    }

    fun handleVerificationCodeDeeplink(verificationCode: String?) {
        launchOnIo {
            verificationCode?.let { code ->
                kotlin.runCatching {
                    if (exposuresInteractor.isEnabled()) {
                        if (!isDeeplinkHandled) {
                            isDeeplinkHandled = true
                            delay(1000)
                            emitCustomEvent(DashboardEvents.OpenSubmitWithVerificationCode)
                        }
                    } else {
                        emitCustomEvent(DashboardEvents.ShowExposureDisabledError)
                    }
                }
            }
        }
    }

    fun toggleExposures() {
        launchOnIo {
            kotlin.runCatching {
                if (exposuresInteractor.isEnabled()) {
                    exposuresInteractor.stop()
                } else {
                    exposuresInteractor.start()
                }
            }.onSuccess {
                refreshState()
            }.onFailure {
                emitDefaultEvent(DefaultOneTimeEvents.Error(errorHandler.mapError(it)))
            }
        }
    }

    fun exposureRequirementsResolved() {
        launchOnIo {
            kotlin.runCatching {
                exposuresInteractor.start()
            }.onSuccess {
                refreshState()
            }.onFailure {
                emitDefaultEvent(DefaultOneTimeEvents.Error(errorHandler.mapError(it)))
            }
        }
    }

    private fun refreshState() {
        launchOnIo {
            kotlin.runCatching {
                val isExpoNotifEnabled = exposuresInteractor.isEnabled()
                val lastRiskyExposureDate = exposuresInteractor.getDailySummaries().maxByOrNull { it.daysSinceEpoch }.toFormattedDate()?.removeAllSpaces()

                eventTracker.setCustomProperty(CustomProperties.ExposureScanningState, isExpoNotifEnabled.toActiveInactive())

                emitViewState {
                    it.copy(
                            isExposureNotificationsEnabled = isExpoNotifEnabled,
                            lastExposureDate = lastRiskyExposureDate
                    )
                }
            }.onFailure {
                emitDefaultEvent(DefaultOneTimeEvents.Error(errorHandler.mapError(it)))
            }
        }
    }

    fun onResume() {
        refreshState()
    }
}