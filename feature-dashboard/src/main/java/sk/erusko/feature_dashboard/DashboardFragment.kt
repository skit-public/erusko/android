package sk.erusko.feature_dashboard

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.base.vm.renderCustomEvents
import base.base_app.core.base.vm.renderDefaultEvents
import base.base_app.core.base.vm.renderViewState
import base.base_app.core.entities.events.Events
import base.base_app.core.features.exposure.ExposureResolutionRequestHandler
import base.base_app.core.helpers.*
import base.base_app.core.helpers.dialog.BottomSheetDialogBuilder
import base.base_app.core.ui.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_dashboard.*
import javax.inject.Inject

@AndroidEntryPoint
class DashboardFragment : BaseFragment() {

    @Inject
    lateinit var exposureResolutionRequestHandler: ExposureResolutionRequestHandler

    private val args: DashboardFragmentArgs by navArgs()

    private val dashboardViewModel: DashboardViewModel by viewModels()

    override fun getStaticContentResId(): Int = R.layout.fragment_dashboard

    override fun getToolbarConfig(): ToolbarConfig? = ToolbarConfig(
            title = StringResource.StringIdResource(R.string.dashboard_toolbar_title),
            infoText = StringResource.StringIdResource(R.string.dashboard_share),
            infoTextClickCallback = {
                eventTracker.trackEvent(Events.ShareAppClicked)
                Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, getString(R.string.dashboard_share_text))
                    type = "text/plain"
                }.also {
                    startActivity(Intent.createChooser(it, getString(R.string.dashboard_share)))
                }
            }
    )

    override fun onResume() {
        super.onResume()
        dashboardViewModel.onResume()
        activity?.let {
            lifecycle.addObserver(exposureResolutionRequestHandler.registerListener(it.activityResultRegistry) {
                if (it.resultCode == Activity.RESULT_OK) {
                    dashboardViewModel.exposureRequirementsResolved()
                }
            })
        }
    }

    override fun init(view: View, savedInstanceState: Bundle?) {
        cl_banner_positive.clicksThrottled(lifecycleScope) {
            findNavController().navigate(DashboardFragmentDirections.actionDashboardFragmentToEncountersFragment())
        }
        cl_banner_no_encounters.clicksThrottled(lifecycleScope) {
            findNavController().navigate(DashboardFragmentDirections.actionDashboardFragmentToEncountersFragment())
        }

        btn_exposures_toggle.clicksThrottled(lifecycleScope, windowDurationMillis = 3000) {
            dashboardViewModel.toggleExposures()
        }

        dashboardViewModel.handleVerificationCodeDeeplink(args.verificationCode)
    }

    override fun render() {
        dashboardViewModel.renderCustomEvents<DashboardEvents>(lifecycleScope) {
            when (it) {
                DashboardEvents.OpenSubmitWithVerificationCode -> findNavController().navigate(DashboardFragmentDirections.actionDashboardFragmentToSubmitFragment(args.verificationCode))
                DashboardEvents.ShowExposureDisabledError -> showDisabledExposuresError()
            }
        }

        dashboardViewModel.renderDefaultEvents(lifecycleScope) {
            when (it) {
                is DefaultOneTimeEvents.Error -> it.error.show(this)
            }
        }

        dashboardViewModel.renderViewState(lifecycleScope) {
            val isScanningEnabled = it.isExposureNotificationsEnabled.orFalse()

            tv_title.text = if (isScanningEnabled) {
                getString(R.string.dashboard_title_enabled)
            } else {
                getString(R.string.dashboard_title_disabled)
            }

            btn_exposures_toggle.text = if (isScanningEnabled) {
                getString(R.string.dashboard_pause_app)
            } else {
                getString(R.string.dashboard_start_app)
            }

            btn_exposures_toggle.setIconResource(if (isScanningEnabled) R.drawable.ic_pause_btn else 0)

            if (isScanningEnabled) {
                iv_exposures_state.setImageResWithAnimation(R.drawable.ic_success)
            } else {
                iv_exposures_state.setImageResWithAnimation(R.drawable.ic_pause)
            }

            cl_banner_positive.hide(it.lastExposureDate.isNullOrBlank())
            cl_banner_no_encounters.show(it.lastExposureDate?.isEmpty().orTrue())
            it.lastExposureDate?.let {
                tv_banner_title.setPartlyBoldText(StringResource.StringIdResource(R.string.dashboard_banner_contact, "$$${it}$$").getString(requireContext()).orEmpty())
            }
                    ?: tv_banner_title_no_encounters.setPartlyBoldText(getString(R.string.dashboard_banner_no_contact_lately))

            btn_submit.clicksThrottled(lifecycleScope) {
                if (isScanningEnabled) {
                    navigateToSubmit()
                } else {
                    showDisabledExposuresError()
                }
            }
        }
    }

    private fun showDisabledExposuresError() {
        getBaseActivity()?.let {
            BottomSheetDialogBuilder(it)
                    .setTitle(StringResource.StringIdResource(R.string.exposure_activation_restricted_title))
                    .setImageResId(base.base_app.core.R.drawable.ic_dialog_alert)
                    .setSubtitle(StringResource.StringIdResource(R.string.exposure_activation_restricted_body))
                    .setCloseButton(base.base_app.core.R.string.general_close) {}
                    .setOkButton(R.string.general_enable) { dashboardViewModel.toggleExposures() }
                    .build()
                    .show()
        }
    }

    private fun navigateToSubmit() {
        findNavController().navigate(DashboardFragmentDirections.actionDashboardFragmentToSubmitFragment())
    }
}