package sk.erusko.feature_recent_encounters

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.base.vm.renderDefaultEvents
import base.base_app.core.base.vm.renderViewState
import base.base_app.core.helpers.StringResource
import base.base_app.core.helpers.dialog.BottomSheetDialogBuilder
import base.base_app.core.helpers.dialog.binders.DialogItem
import base.base_app.core.helpers.dialog.binders.DialogItemsBinder
import base.base_app.core.helpers.getString
import base.base_app.core.helpers.show
import base.base_app.core.ui.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_recent_encounters.*
import kotlinx.android.synthetic.main.fragment_recent_encounters_banner.*
import kotlinx.android.synthetic.main.item_recent_encounter.view.*

@AndroidEntryPoint
class RecentEncountersFragment : BaseFragment() {

    private val recentEncountersViewModel: RecentEncountersViewModel by viewModels()

    override fun getScrollContentResId(): Int = R.layout.fragment_recent_encounters

    override fun getTopStaticContentResId(): Int? = R.layout.fragment_recent_encounters_banner

    private var toolbarConfig: ToolbarConfig = ToolbarConfig()

    override fun getToolbarConfig(): ToolbarConfig? = toolbarConfig.copy(isToolbarMotionEnabled = false)

    override fun init(view: View, savedInstanceState: Bundle?) {
        view.findViewById<ViewGroup>(base.base_app.core.R.id.static_content_top)?.translationY = -55.dpToPx.toFloat()

        cl_behaviour.clicksThrottled(lifecycleScope) {
            showBehaviourDialog()
        }

        cl_symptoms.clicksThrottled(lifecycleScope) {
            showMainSymptomsDialog()
        }
    }

    override fun render() {
        recentEncountersViewModel.renderDefaultEvents(lifecycleScope) {
            when (it) {
                is DefaultOneTimeEvents.Error -> it.error.show(this)
            }
        }

        recentEncountersViewModel.renderViewState(lifecycleScope) {
            cl_encounters.clicksThrottled(lifecycleScope) {
                showRecentEncountersDialog(it.items)
            }

            val isNotExposed = it.items.isEmpty()

            tv_body.setText(if (isNotExposed) R.string.recent_encounters_no_exposure_body else R.string.recent_encounters_body)

            tv_title_no_encounters.show(isNotExposed)
            cv_exposed_date.show(!isNotExposed)

            if (!isNotExposed) {
                this.toolbarConfig = ToolbarConfig(backgroundResource = R.drawable.ic_rect_red)
                refreshToolbar()
                tv_banner_title.setPartlyBoldText(StringResource.StringIdResource(R.string.dashboard_banner_contact, "$$${it.items.firstOrNull()}$$").getString(requireContext()).orEmpty())
            } else {
                this.toolbarConfig = ToolbarConfig()
                refreshToolbar()
            }
        }
    }

    private fun showBehaviourDialog() {
        val adapter = BetterAdapter({ DialogItemsBinder() })
        getBaseActivity()?.let {
            BottomSheetDialogBuilder(it)
                    .setTitle(R.string.responsible_behaviour_title)
                    .setSubtitle(R.string.responsible_behaviour_subtitle)
                    .setAdapterAndData(adapter, getResponsibleBehaviourItems())
                    .build()
                    .show()
        }
    }

    private fun showMainSymptomsDialog() {
        val adapter = BetterAdapter({ DialogItemsBinder() })
        getBaseActivity()?.let {
            BottomSheetDialogBuilder(it)
                    .setTitle(R.string.main_symptoms_title)
                    .setSubtitle(R.string.main_symptoms_subtitle)
                    .setAdapterAndData(adapter, getMainSymptoms())
                    .build()
                    .show()
        }
    }

    private fun showRecentEncountersDialog(items: List<String>) {
        val adapter = BetterAdapter({ EncounterBinder() })
        getBaseActivity()?.let {
            BottomSheetDialogBuilder(it)
                    .setTitle(R.string.risky_encounters_dialog_title)
                    .setAdapterAndData(adapter, items.map { DialogItem(null, StringResource.StringValueResource(it)) }, RecentEncountersDecorator(activity))
                    .build()
                    .show()
        }
    }

    private fun getMainSymptoms(): List<DialogItem> = listOf(
            DialogItem(R.drawable.ic_fever_high_temperature, StringResource.StringIdResource(R.string.main_symptoms_high_fever)),
            DialogItem(R.drawable.ic_coughing_symptom, StringResource.StringIdResource(R.string.main_symptoms_coughing)),
            DialogItem(R.drawable.ic_sore_throat_symptom, StringResource.StringIdResource(R.string.main_symptoms_sore_throat)),
            DialogItem(R.drawable.ic_body_pain, StringResource.StringIdResource(R.string.main_symptoms_body_pain))
    )

    private fun getResponsibleBehaviourItems(): List<DialogItem> = listOf(
            DialogItem(R.drawable.ic_wash_hand_soap, StringResource.StringIdResource(R.string.responsible_behaviour_wash_hands)),
            DialogItem(R.drawable.ic_disinfect_hands_soap, StringResource.StringIdResource(R.string.responsible_behaviour_desinfect_hands)),
            DialogItem(R.drawable.ic_cough_tissue, StringResource.StringIdResource(R.string.responsible_behaviour_cough_to_cloth)),
            DialogItem(R.drawable.ic_avoid_contact, StringResource.StringIdResource(R.string.responsible_behaviour_keep_distance)),
            DialogItem(R.drawable.ic_facial_mask, StringResource.StringIdResource(R.string.responsible_behaviour_with_face_mask))
    )

    inner class EncounterBinder : EmptyBinderHolder<DialogItem>(R.layout.item_recent_encounter, DialogItem::class.java) {
        override fun bind(position: Int, item: DialogItem) {
            super.bind(position, item)
            itemView.apply {
                tv_recent_event_date.text = item.title.getString(context)
            }
        }
    }
}