package sk.erusko.feature_recent_encounters

import androidx.hilt.lifecycle.ViewModelInject
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.vm.BaseViewModel
import base.base_app.core.base.vm.emitDefaultEvent
import base.base_app.core.base.vm.emitViewState
import base.base_app.core.base.vm.launchOnIo
import base.base_app.core.entities.error_handler.ErrorHandler
import base.base_app.core.features.exposure.ExposuresInteractor
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.removeAllSpaces
import base.base_app.core.helpers.toFormattedDate

data class RecentEncountersViewState(
        val items: List<String> = emptyList()
)

class RecentEncountersViewModel @ViewModelInject constructor(
        private val exposuresInteractor: ExposuresInteractor,
        private val errorHandler: ErrorHandler
) : BaseViewModel<RecentEncountersViewState>(RecentEncountersViewState()) {

    init {
        launchOnIo {
            kotlin.runCatching {
                exposuresInteractor.getDailySummaries().sortedByDescending { it.daysSinceEpoch }.mapNotNull {
                    it.toFormattedDate()?.removeAllSpaces()
                }
            }.onSuccess { dailySummaries ->
                emitViewState { it.copy(items = dailySummaries) }
            }.onFailure {
                Logger.et { it }
                emitDefaultEvent(DefaultOneTimeEvents.Error(errorHandler.mapError(it)))
            }
        }
    }
}