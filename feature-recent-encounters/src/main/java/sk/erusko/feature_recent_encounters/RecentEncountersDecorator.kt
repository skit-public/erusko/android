package sk.erusko.feature_recent_encounters

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class RecentEncountersDecorator(
        context: Context?
) : RecyclerView.ItemDecoration() {
    private val drawable: Drawable? = context?.let {
        ContextCompat.getDrawable(context, R.drawable.recent_encounters_decorator)
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft
        val right = parent.width - parent.paddingRight

        parent.adapter?.itemCount?.let { childCount ->
            for (i in 0 until childCount - 1) {
                parent.getChildAt(i)?.let { child ->
                    val top = child.bottom + (child.layoutParams as RecyclerView.LayoutParams).bottomMargin
                    drawable?.let {
                        val bottom = top + drawable.intrinsicHeight
                        drawable.setBounds(left, top, right, bottom)
                        drawable.draw(c)
                    }
                }
            }
        }
    }
}