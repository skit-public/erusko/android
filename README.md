## Created by

https://slovenskoit.sk/

## Inspiration

https://github.com/covid19cz/erouska-android

## Core

https://developers.google.com/android/exposure-notifications/exposure-notifications-api

## Notes

- a signing key and properties file is required, the properties should be in the following format:

debugCredentials.properties
```
storeFile=../keystore/debugKeystore.keystore
storePassword=abc
keyAlias=abc
keyPassword=abc
```
- project is running on firebase remote config so a proper google-services.json is required
- we are using our security library which is hosted in our private gitlab repository