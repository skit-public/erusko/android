package base.base_app.feature_start_prompt.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import base.base_app.core.base.AppNavigator
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.base.vm.renderCustomEvents
import base.base_app.core.base.vm.renderDefaultEvents
import base.base_app.core.base.vm.renderViewState
import base.base_app.core.helpers.PlayServicesResult
import base.base_app.core.helpers.StringResource
import base.base_app.core.helpers.openPlayStore
import base.base_app.core.helpers.show
import base.base_app.core.navigation.AppNavigationPathsData
import base.base_app.core.ui.clicksThrottled
import base.base_app.core.ui.startup.StartEvents
import base.base_app.core.ui.startup.StartViewModel
import base.base_app.feature_start_prompt.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_start_prompt_scroll.*
import kotlinx.android.synthetic.main.fragment_start_prompt_static.*
import javax.inject.Inject

@AndroidEntryPoint
class StartPromptFragment : BaseFragment() {

    @Inject
    lateinit var appNavigator: AppNavigator

    private val viewModel: StartViewModel by viewModels()

    override fun getStaticContentResId(): Int = R.layout.fragment_start_prompt_static

    override fun getScrollContentResId(): Int? = R.layout.fragment_start_prompt_scroll

    private var startToolbarConfig: ToolbarConfig = ToolbarConfig(
            imageResource = R.drawable.ic_upozornenie
    )

    override fun getToolbarConfig(): ToolbarConfig? = startToolbarConfig

    override fun init(view: View, savedInstanceState: Bundle?) {
        viewModel.refreshStates()
    }

    override fun render() {
        viewModel.renderCustomEvents<StartEvents>(lifecycleScope) {
            when (it) {
                is StartEvents.NavigateToFirstActivation -> {
                    if (it.withRestart) {
                        appNavigator.navigateTo(AppNavigationPathsData.Start, true)
                    } else {
                        findNavController().navigate(StartPromptFragmentDirections.actionStartPromptFragmentToIntroFragment(), FragmentNavigatorExtras(
                                btn_next to getString(R.string.intro_flow_button_transition_name)
                        ))
                    }
                }
                is StartEvents.NavigateToDashboard -> {
                    if (it.withRestart) {
                        appNavigator.navigateTo(AppNavigationPathsData.Start, true)
                    } else {
                        findNavController().navigate(StartPromptFragmentDirections.actionStartPromptFragmentToDashboardFragment())
                    }
                }
            }
        }

        viewModel.renderDefaultEvents(lifecycleScope) {
            when (it) {
                is DefaultOneTimeEvents.Error -> it.error.show(this)
            }
        }

        viewModel.renderViewState(lifecycleScope) {
            showLoading(it.isLoading)
            when {
                it.showRemoteConfigIssue -> renderRemoteConfigFetchIssue()
                it.showPlayServicesDialog == PlayServicesResult.Error -> renderPlayServicesError()
                it.showMandatory -> renderMandatoryUpdate()
            }
        }
    }

    private fun renderPlayServicesError() {
        startToolbarConfig = ToolbarConfig(
                imageResource = R.drawable.ic_upozornenie,
                title = StringResource.StringIdResource(R.string.play_services_error_toolbar_title)
        )
        refreshToolbar()
        tv_prompt_title.setText(R.string.play_services_error_title)
        tv_prompt_body.setText(R.string.play_services_error_body)
        btn_next.setText(R.string.play_services_error_button)
        btn_next.clicksThrottled(lifecycleScope) {
            viewModel.resolvePlayServicesError()
        }
    }

    private fun renderMandatoryUpdate() {
        startToolbarConfig = ToolbarConfig(
                imageResource = R.drawable.ic_upozornenie,
                title = StringResource.StringIdResource(R.string.mandatory_update_toolbar_title)
        )
        refreshToolbar()
        tv_prompt_title.setText(R.string.mandatory_update_title)
        tv_prompt_body.setText(R.string.mandatory_update_body)
        btn_next.setText(R.string.mandatory_update_button)
        btn_next.clicksThrottled(lifecycleScope) {
            getBaseActivity()?.openPlayStore()
        }
    }

    private fun renderRemoteConfigFetchIssue() {
        startToolbarConfig = ToolbarConfig(
                imageResource = R.drawable.ic_upozornenie,
                title = StringResource.StringIdResource(R.string.app_first_start_config_error_toolbar_title)
        )
        refreshToolbar()
        tv_prompt_title.setText(R.string.app_first_start_config_error_title)
        tv_prompt_body.setText(R.string.app_first_start_config_error_body)
        btn_next.setText(R.string.app_first_start_config_error_button)
        btn_next.clicksThrottled(lifecycleScope) {
            viewModel.refreshStates()
        }
    }

}