package sk.erusko.feature_help

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.helpers.getString
import base.base_app.core.ui.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_help.*

@AndroidEntryPoint
class HelpFragment : BaseFragment() {

    private val args: HelpFragmentArgs by navArgs()

    override fun getScrollContentResId(): Int = R.layout.fragment_help

    override fun getToolbarConfig(): ToolbarConfig? = ToolbarConfig(
            title = args.headline,
            imageResource = args.imageResource.let { if (it == 0) null else it }
    )

    override fun init(view: View, savedInstanceState: Bundle?) {}

    override fun render() {
        tv_help_title.show(args.title != null)
        tv_help_title.text = args.title?.getString(requireContext())

        tv_help_body.text = args.body.getString(requireContext())
    }
}