package sk.slovenskoit.locale_change_core

import android.content.Context
import android.content.res.Resources
import dev.b3nedikt.app_locale.AppLocale
import dev.b3nedikt.restring.Restring
import dev.b3nedikt.restring.repository.CachedStringRepository
import dev.b3nedikt.restring.repository.SharedPrefsStringRepository
import dev.b3nedikt.reword.RewordInterceptor
import dev.b3nedikt.viewpump.ViewPump
import dev.b3nedikt.viewpump.ViewPumpContextWrapper
import sk.slovenskoit.locale_change_core.AppLocalePreferencesHelper.STORED_LOCALES_ALIAS
import sk.slovenskoit.locale_change_core.models.Translations
import java.util.*

/**
 * Locale manager config
 *
 * @property brand specifies the strings to be downloaded for each app
 */
data class LocaleManagerConfig(
        val isEnabled: Boolean,
        val brand: String,
        val defaultLocaleIsoCode: String? = null,
        val showStringKeysInsteadOfValues: Boolean = false,
        val stringClass: Class<*>? = null
)

object LocaleManager {

    private var config: LocaleManagerConfig? = null

    /**
     * Init should be called in BaseApplication class with application [Context], and proper [LocaleManagerConfig]
     *
     * @param context
     * @param localeManagerConfig
     */
    fun init(context: Context, localeManagerConfig: LocaleManagerConfig) {
        println("Initializing LocaleManager with config: $localeManagerConfig")
        config = localeManagerConfig

        execute({
            Restring.init(context)
            if (!localeManagerConfig.defaultLocaleIsoCode.isNullOrBlank()) {
                Restring.localeProvider = AppLocaleLocaleProvider
                AppLocale.appLocaleRepository = SharedPrefsSelectedLocale(context)
            }
            Restring.stringRepository = if (localeManagerConfig.showStringKeysInsteadOfValues && localeManagerConfig.stringClass != null) {
                TestRepository(localeManagerConfig.stringClass)
            } else {
                CachedStringRepository(
                        SharedPrefsStringRepository { sharedPreferencesName ->
                            AppLocalePreferencesHelper.create(context, STORED_LOCALES_ALIAS, sharedPreferencesName)
                        }
                )
            }

            if (Restring.localeProvider.isInitial && !localeManagerConfig.defaultLocaleIsoCode.isNullOrBlank()) {
                println("Setting default locale ${localeManagerConfig.defaultLocaleIsoCode}")
                changeLocale(Locale(localeManagerConfig.defaultLocaleIsoCode.toLowerCase(getCurrentLocale())))
            }

            ViewPump.init(ViewPump.builder()
                    .addInterceptor(RewordInterceptor)
                    .build())
        }, {})
    }

    /**
     * Get current locale should be used each time the current [Locale] is required, for example for date formatting
     *
     * @return
     */
    fun getCurrentLocale(): Locale {
        return execute({
            Restring.localeProvider.currentLocale
        }, {
            Locale.getDefault()
        })

    }

    /**
     * Change [Locale] will change the locale for upcoming string requests, if immediate change is needed, activity have to be restarted
     *
     * @param locale
     */
    fun changeLocale(locale: Locale) {
        execute({
            Restring.locale = locale
        }, {})
    }

    /**
     * Get available locales returns all the available locales that were obtained with [updateLocales]
     *
     * @return list of available locales
     */
    fun getAvailableLocales(): List<Locale> {
        return execute({
            Restring.stringRepository.supportedLocales.toList()
        }, {
            emptyList()
        })
    }

    /**
     * Activity wrap context
     *
     * @param context
     * @return
     */
    fun activityWrapContext(context: Context): Context {
        return execute({
            ViewPumpContextWrapper.wrap(Restring.wrapContext(context))
        }, {
            context
        })
    }

    /**
     * Activity wrap resources
     *
     * @param context
     * @return
     */
    fun activityWrapResources(context: Context): Resources {
        return execute({
            Restring.wrapContext(context).resources
        }, {
            context.resources
        })
    }

    /**
     * Fragment on resume
     *
     * @param context
     */
    fun fragmentOnResume(context: Context) {
        execute({
            ViewPump.setOverwriteContext(Restring.wrapContext(context))
        }, {})
    }

    /**
     * Fragment wrap context
     *
     * @param context
     * @return
     */
    fun fragmentWrapContext(context: Context): Context {
        return execute({
            ViewPumpContextWrapper.wrap(Restring.wrapContext(context))
        }, {
            context
        })
    }

    /**
     * Update locales will trigger the API call to get all the available locales for the given [LocaleManagerConfig]
     *
     */
    @Deprecated("no real usage with firebase remote config impl")
    fun updateLocales() {
        execute({
            // this would be used if we would have a custom server
        }, {
            // since we don't provide the strings, the will be loaded from strings.xml based on phones language or SK as default
        })
    }

    fun storeCustomLocales(translations: Translations) {
        execute({
            Restring.putStrings(Locale(translations.locale.toLowerCase(getCurrentLocale())), translations.texts.filter {
                it.key.isNotBlank() && it.value.isNotBlank()
            })
        }, {
            // disabled
        })
    }

    private fun <T> execute(enabled: () -> T, disabled: () -> T): T {
        return if (config?.isEnabled == true) {
            enabled()
        } else {
            println("LocaleManager not initialized. Provided configuration: $config")
            disabled()
        }
    }

}