package sk.slovenskoit.locale_change_core.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Translations")
data class Translations(
        @SerialName("locale") val locale: String,
        @SerialName("texts") val texts: Map<String, String>
)