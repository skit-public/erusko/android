package sk.slovenskoit.locale_change_core

import dev.b3nedikt.app_locale.AppLocale
import dev.b3nedikt.restring.MutableStringRepository
import dev.b3nedikt.restring.PluralKeyword
import java.util.*

class TestRepository(stringClass: Class<*>) : MutableStringRepository {

    private val testStrings by lazy {
        val fields = stringClass.fields
        val stringNames = fields.map { it.name }

        listOf(AppLocale.currentLocale)
                .associateWith { stringNames.associateWith { it as CharSequence }.toMutableMap() }
                .toMutableMap()
                .also {
                    println("$it")
                }
    }

    override val supportedLocales: Set<Locale>
        get() = setOf(AppLocale.currentLocale)

    override val strings: MutableMap<Locale, MutableMap<String, CharSequence>>
        get() = testStrings

    override val quantityStrings: MutableMap<Locale, MutableMap<String, Map<PluralKeyword, CharSequence>>>
        get() = mutableMapOf()

    override val stringArrays: MutableMap<Locale, MutableMap<String, Array<CharSequence>>>
        get() = mutableMapOf()

}