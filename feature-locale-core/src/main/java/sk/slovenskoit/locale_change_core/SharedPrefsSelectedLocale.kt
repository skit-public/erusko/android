package sk.slovenskoit.locale_change_core

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import dev.b3nedikt.app_locale.AppLocaleRepository
import sk.slovenskoit.locale_change_core.AppLocalePreferencesHelper.SELECTED_LOCALE_ALIAS
import sk.slovenskoit.locale_change_core.AppLocalePreferencesHelper.SELECTED_LOCALE_NAME
import java.util.*

class SharedPrefsSelectedLocale(private val context: Context) : AppLocaleRepository {

    private companion object {
        private const val AppLocaleSharedPrefLocaleTag = "4s9q7z281fdd66rq"
    }

    private val sharedPrefs: SharedPreferences by lazy { AppLocalePreferencesHelper.create(context, SELECTED_LOCALE_ALIAS, SELECTED_LOCALE_NAME) }

    override var desiredLocale: Locale?
        get() = loadLocale()
        set(value) =
            if (value != null) saveLocale(value) else resetLocale()

    private fun saveLocale(locale: Locale) =
            sharedPrefs
                    .edit()
                    .putString(AppLocaleSharedPrefLocaleTag, toLanguageTag(locale))
                    .apply()

    private fun loadLocale(): Locale? =
            sharedPrefs
                    .getString(AppLocaleSharedPrefLocaleTag, null)
                    ?.let { fromLanguageTag(it) }

    private fun resetLocale() =
            sharedPrefs
                    .edit()
                    .remove(AppLocaleSharedPrefLocaleTag)
                    .apply()

    private fun toLanguageTag(locale: Locale): String {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return locale.toLanguageTag()
        }

        val language = locale.language
        val country = locale.country

        if (language.isNotEmpty() && country.isNotEmpty()) {
            return "$language-${country}"
        }

        return language
    }

    private fun fromLanguageTag(locale: String): Locale {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return Locale.forLanguageTag(locale)
        }

        if (locale.contains("-")) {
            val language = locale.split("-")[0]
            val country = locale.split("-")[1]

            return Locale(language, country)
        }

        return Locale(locale)
    }

}