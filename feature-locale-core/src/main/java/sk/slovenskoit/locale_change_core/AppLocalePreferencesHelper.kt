package sk.slovenskoit.locale_change_core

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

object AppLocalePreferencesHelper {
    const val SELECTED_LOCALE_NAME = "9c9f2r6qt1dqhdht7"

    const val STORED_LOCALES_ALIAS: String = "7d58ewq5f51g25nh58"
    const val SELECTED_LOCALE_ALIAS = "h4rggf8zdwqubpq1k"

    fun create(context: Context, keyAlias: String, prefsFileName: String): SharedPreferences {
        val masterKey = MasterKey.Builder(context, keyAlias)
                .apply {
                    setRequestStrongBoxBacked(true)
                    setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                }
                .build()
        return EncryptedSharedPreferences
                .create(
                        context,
                        prefsFileName,
                        masterKey,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
    }
}