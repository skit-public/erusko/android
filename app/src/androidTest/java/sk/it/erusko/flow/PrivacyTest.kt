package sk.it.erusko.flow

import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.di.AnalyticsModule
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Test
import sk.erusko.feature_activation.privacy.PrivacyViewModel
import sk.it.erusko.base.BaseTest
import sk.it.erusko.base.expectStateOrEvent
import sk.it.erusko.base.initializeViewModelTest

@UninstallModules(AnalyticsModule::class)
@HiltAndroidTest
class PrivacyFragmentTest : BaseTest() {

    @BindValue
    @JvmField
    val eventTracker: EventTracker = mock()

    @Test
    fun counterButtonTest() {
        initializeViewModelTest<PrivacyViewModel> {
            assert(persistentAppPreferences.isFirstActivationFlow)
            it.nextClicked()
            assert(!persistentAppPreferences.isFirstActivationFlow)
            verify(eventTracker).trackEvent(Events.InitialVerificationSuccess)
            expectStateOrEvent(DefaultOneTimeEvents.NavigateNext())
        }
    }
}