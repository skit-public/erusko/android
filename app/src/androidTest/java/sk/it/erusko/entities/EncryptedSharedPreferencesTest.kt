package sk.it.erusko.entities

import sk.it.security_utils.preferences.EncryptedSharedPrefModel
import sk.it.erusko.base.BaseTest
import com.chibatching.kotpref.Kotpref
import com.chibatching.kotpref.gsonpref.gson
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

@HiltAndroidTest
class SharedPreferencesTest : BaseTest() {

    class TestEncryptedSharedPreferences : EncryptedSharedPrefModel("keyAlias") {
        var token: String? by nullableStringPref()
        var gsonObject: DataClassForGsonTesting? by nullableGsonPref()
    }

    private val testEncryptedSharedPreferences = TestEncryptedSharedPreferences()

    @Before
    fun init() {
        Kotpref.init(application)
        Kotpref.gson = gson
    }

    @After
    fun clean() {
        testEncryptedSharedPreferences.clear()
    }


    @Test
    fun testSimpleValueEncryptedSharedPreferences() {
        assert("tokenSimple")
    }

    @Test
    fun testComplexCharEncryptedSharedPreferences() {
        assert("èàùââêîôûëïç")
        assert("äöüß")
        assert("€£•")
        assert("a")
        assert("Fűkőová")
        assert("b".repeat(15000))
    }

    private fun assert(value: String) {
        testEncryptedSharedPreferences.token = value
        assertTrue("Assertion error value is (\"${testEncryptedSharedPreferences.token}\" and entry was \"${value}\")", testEncryptedSharedPreferences.token == value)
    }

    @Test
    fun testGsonObjectEncryptedSharedPreferences() {
        val dataClassObject = DataClassForGsonTesting(tokenHolder = "someSimpleString", someNonStringValue = 129202)
        testEncryptedSharedPreferences.gsonObject = dataClassObject
        assertTrue(testEncryptedSharedPreferences.gsonObject == dataClassObject)
    }
}

data class DataClassForGsonTesting(
        val tokenHolder: String,
        val someNonStringValue: Int
)
