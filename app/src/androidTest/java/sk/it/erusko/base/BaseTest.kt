package sk.it.erusko.base

import android.app.Application
import androidx.activity.viewModels
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import base.base_app.core.base.EmitterCollector
import base.base_app.core.base.vm.BaseViewModel
import base.base_app.core.entities.preferences.PersistentAppPreferences
import com.chibatching.kotpref.Kotpref
import com.chibatching.kotpref.gsonpref.gson
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import javax.inject.Inject

@AndroidEntryPoint
class TestActivity : FragmentActivity()

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
abstract class BaseTest {

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var persistentAppPreferences: PersistentAppPreferences

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    val testCoroutineDispatcher = TestCoroutineDispatcher()

    val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application

    var emitterCollector: EmitterCollector? = null

    @Before
    fun setup() {
        hiltRule.inject()
        if (!Kotpref.isInitialized) {
            Kotpref.init(application)
            Kotpref.gson = gson
        }
        Dispatchers.setMain(testCoroutineDispatcher)
        emitterCollector = EmitterCollectorImpl()
    }

    @After
    fun reset() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cancel()
        (emitterCollector as? EmitterCollectorImpl?)?.reset()
    }
}

inline fun <reified T : BaseViewModel<*>> BaseTest.initializeViewModelTest(crossinline testingBlock: (T) -> Unit) {
    ActivityScenario.launch(TestActivity::class.java).use {
        it.onActivity {
            it.viewModels<T>().value.let {
                testCoroutineDispatcher.runBlockingTest {
                    it.setIoContext(testCoroutineDispatcher)
                    it.setEmitterCollector(emitterCollector)
                    testingBlock(it)
                }
            }
        }
    }
}

fun <T> BaseTest.expectStateOrEvent(@NonNull expected: T, delay: Long = 300) {
    (this.emitterCollector as? EmitterCollectorImpl?)?.expectState(expected, delay)
            ?: Assert.fail("Emitter not set")
}

class EmitterCollectorImpl : EmitterCollector {
    private val emittedStatesAndEvents: MutableList<Any> = mutableListOf()

    @Synchronized
    fun reset() {
        emittedStatesAndEvents.clear()
    }

    @Synchronized
    override fun collect(state: Any) {
        emittedStatesAndEvents.add(state)
    }

    @Synchronized
    fun <T> expectState(@NonNull expected: T, delay: Long = 300, attempt: Int = 0) {
        val expectedValue = emittedStatesAndEvents.filterIsInstance(expected!!::class.java)
                .firstOrNull { it == expected }

        if (expectedValue == null) {
            if (attempt < 3) {
                Thread.sleep(delay)
                this.expectState<T>(expected, delay, attempt + 1)
            } else {
                Assert.fail("Expected state not found\nExpected state:\n${expected}\nPrevious states:\n${emittedStatesAndEvents.joinToString(separator = "\n")}")
            }
        } else {
            emittedStatesAndEvents.remove(expectedValue)
        }
    }

}