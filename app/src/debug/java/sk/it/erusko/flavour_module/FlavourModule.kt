package sk.it.erusko.flavour_module

import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.config.static_config.ConfigProviderImpl
import base.base_app.core.entities.config.static_config.remote_config.RemoteConfigManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class FlavourModule {

    @Provides
    @Singleton
    fun provideConfigProvider(remoteConfigManager: RemoteConfigManager): ConfigProvider {
        return ConfigProviderImpl(remoteConfigManager, ConfigProvider.Environment.getDefault())
    }

}