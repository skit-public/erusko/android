package sk.it.erusko.di

import base.base_app.core.base.AppNavigator
import sk.it.erusko.navigation.AppNavigatorImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModules {

    @Provides
    @Singleton
    fun provideAppNavigator(impl: AppNavigatorImpl): AppNavigator = impl
}