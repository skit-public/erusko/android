package sk.it.erusko.navigation

import android.content.Intent
import base.base_app.core.base.AppNavigationPaths
import base.base_app.core.base.AppNavigator
import base.base_app.core.base.BaseActivity
import base.base_app.core.base.IntentKeys
import base.base_app.core.entities.activity_manager.AppActivityManager
import base.base_app.core.helpers.Logger
import base.base_app.core.navigation.AppNavigationPathsData
import base.base_app.core.ui.main.MainActivity
import javax.inject.Inject

class AppNavigatorImpl @Inject constructor(
        private val appActivityManager: AppActivityManager
) : AppNavigator {

    override fun navigateTo(navigationPath: AppNavigationPaths, finishAll: Boolean) {
        Logger.dm { "Navigating to $navigationPath" }
        when (navigationPath) {
            is AppNavigationPathsData.Start -> getCurrentActivity()?.let { startActivity(it, Intent(it, MainActivity::class.java), finishAll) }
            is AppNavigationPathsData.StartWithPrompt -> getCurrentActivity()?.let { startActivity(it, Intent(it, MainActivity::class.java).putExtra(IntentKeys.SHOW_PROMPT, true), finishAll) }
            else -> {
                Logger.em { "$navigationPath not found" }
            }
        }
    }

    private fun getCurrentActivity(): BaseActivity? {
        return appActivityManager.getCurrentActivity().also {
            if (it == null) {
                Logger.em { "Activity not set!" }
            }
        }
    }

}
