package sk.it.erusko

import androidx.annotation.Keep
import androidx.hilt.work.HiltWorkerFactory
import androidx.multidex.MultiDexApplication
import androidx.work.Configuration
import androidx.work.WorkManager
import base.base_app.core.entities.activity_manager.AppActivityManager
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.features.workers.ExposuresWorkerScheduler
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.app_helper.AppHelper
import com.chibatching.kotpref.Kotpref
import com.chibatching.kotpref.gsonpref.gson
import com.google.gson.Gson
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.hilt.android.HiltAndroidApp
import sk.slovenskoit.locale_change_core.LocaleManager
import sk.slovenskoit.locale_change_core.LocaleManagerConfig
import javax.inject.Inject


@HiltAndroidApp
class BaseApplication : MultiDexApplication(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    /**
     * Force inject WorkManager to ensure it is initialized when the app was force-stopped and it
     * re-schedule all workers when woken up by Google Play Services.
     */
    @Keep
    @Inject
    lateinit var workManager: WorkManager

    @Inject
    lateinit var exposuresWorkerScheduler: ExposuresWorkerScheduler

    @Inject
    lateinit var appActivityManager: AppActivityManager

    @Inject
    lateinit var configProvider: ConfigProvider

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var persistentAppPreferences: PersistentAppPreferences

    @Inject
    lateinit var appHelper: AppHelper

    override fun onCreate() {
        super.onCreate()
        initKotPref()
        Logger.init()

        persistentAppPreferences.environment?.let {
            configProvider.switchEnvironment(it)
        }

        LocaleManager.init(applicationContext, LocaleManagerConfig(configProvider.provide().localizations().isRemoteLocalizationEnabled, "eRusko", null, persistentAppPreferences.showStringKeysInsteadOfValues, R.string::class.java))
        appActivityManager.registerActivityLifecycleHelper(this)
        appHelper.setupAppHelper(this)
        exposuresWorkerScheduler.scheduleWorkers()

        AndroidThreeTen.init(this)
    }

    private fun initKotPref() {
        Kotpref.init(this)
        Kotpref.gson = gson
    }

    override fun getWorkManagerConfiguration() = Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()

}