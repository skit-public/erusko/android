package sk.erusko.feature_activation.privacy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.base.vm.renderDefaultEvents
import base.base_app.core.helpers.SpanHelper
import base.base_app.core.helpers.StringResource
import base.base_app.core.ui.clicksThrottledNextButton
import base.base_app.core.ui.views.spans.SpanV1
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_privacy_scroll.*
import kotlinx.android.synthetic.main.fragment_privacy_static.*
import sk.erusko.feature_activation.R

@AndroidEntryPoint
class PrivacyFragment : BaseFragment() {

    private val privacyViewModel: PrivacyViewModel by viewModels()

    override fun getScrollContentResId(): Int = R.layout.fragment_privacy_scroll

    override fun getStaticContentResId(): Int? = R.layout.fragment_privacy_static

    override fun getToolbarConfig(): ToolbarConfig? = ToolbarConfig(
            title = StringResource.StringIdResource(R.string.privacy_toolbar_title),
            imageResource = R.drawable.ic_home_office
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState).also {
            sharedElementEnterTransition = TransitionInflater.from(this.context).inflateTransition(R.transition.change_bounds)
            sharedElementReturnTransition = TransitionInflater.from(this.context).inflateTransition(R.transition.change_bounds)
        }
    }

    override fun init(view: View, savedInstanceState: Bundle?) {
        btn_next.clicksThrottledNextButton(lifecycleScope) {
            privacyViewModel.nextClicked()
        }

        tv_privacy_body.text = SpanHelper.replace(getString(R.string.privacy_body),
                SpanHelper.SpanParam(SpanHelper.OnClickSpan(tv_privacy_body, {
                    findNavController().navigate(PrivacyFragmentDirections.actionPrivacyFragmentToHelpFragment(
                            headline = StringResource.StringIdResource(R.string.tac_toolbar_title),
                            title = StringResource.StringIdResource(R.string.tac_title),
                            body = StringResource.StringIdResource(R.string.tac_body)
                    ))
                }), SpanV1(requireContext()))
        )
    }

    override fun render() {
        privacyViewModel.renderDefaultEvents(lifecycleScope) {
            when (it) {
                is DefaultOneTimeEvents.NavigateNext -> findNavController().navigate(PrivacyFragmentDirections.actionPrivacyFragmentToDashboardFragment())
            }
        }
    }
}