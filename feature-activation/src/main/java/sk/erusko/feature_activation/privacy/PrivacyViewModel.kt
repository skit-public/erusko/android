package sk.erusko.feature_activation.privacy

import androidx.hilt.lifecycle.ViewModelInject
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.vm.BaseViewModel
import base.base_app.core.base.vm.emitDefaultEvent
import base.base_app.core.base.vm.launchOnIo
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.entities.preferences.IntroProgress
import base.base_app.core.entities.preferences.PersistentAppPreferences

class PrivacyViewModel @ViewModelInject constructor(
        private val persistentAppPreferences: PersistentAppPreferences,
        private val eventTracker: EventTracker
) : BaseViewModel<Unit>(Unit) {

    init {
        persistentAppPreferences.setProgress(IntroProgress.TERMS_AND_CONDITIONS)
    }

    fun nextClicked() {
        launchOnIo {
            persistentAppPreferences.isFirstActivationFlow = false
            eventTracker.trackEvent(Events.InitialActivationFinished)
            emitDefaultEvent(DefaultOneTimeEvents.NavigateNext())
        }
    }

}