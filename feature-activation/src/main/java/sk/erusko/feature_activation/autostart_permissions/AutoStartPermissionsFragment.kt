package sk.erusko.feature_activation.autostart_permissions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.base.vm.renderDefaultEvents
import base.base_app.core.helpers.StringResource
import base.base_app.core.helpers.show
import base.base_app.core.ui.clicksThrottled
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_autostart_scroll.*
import kotlinx.android.synthetic.main.fragment_autostart_static.*
import sk.erusko.feature_activation.R


@AndroidEntryPoint
class AutoStartPermissionsFragment : BaseFragment() {

    private val viewModel: AutoStartPermissionsViewModel by viewModels()

    override fun getStaticContentResId(): Int? = R.layout.fragment_autostart_static

    override fun getScrollContentResId(): Int? = R.layout.fragment_autostart_scroll

    override fun getToolbarConfig(): ToolbarConfig? = ToolbarConfig(
            title = StringResource.StringIdResource(R.string.android_autostart_permissions_toolbar_title),
            imageResource = R.drawable.ic_desinformation
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState).also {
            sharedElementEnterTransition = TransitionInflater.from(this.context).inflateTransition(R.transition.change_bounds)
            sharedElementReturnTransition = TransitionInflater.from(this.context).inflateTransition(R.transition.change_bounds)
        }
    }

    override fun init(view: View, savedInstanceState: Bundle?) {
        btn_settings.clicksThrottled(lifecycleScope) {
            viewModel.getAutoStartPermissions()
        }

        btn_move_next.clicksThrottled(lifecycleScope) {
            viewModel.moveNext()
        }
    }

    override fun render() {
        tv_autostart_description.text = getString(R.string.android_autostart_permissions_body)

        viewModel.renderDefaultEvents(lifecycleScope) {
            when (it) {
                is DefaultOneTimeEvents.NavigateNext -> findNavController().navigate(
                        AutoStartPermissionsFragmentDirections.actionAautostartFragmentToPrivacyFragment(),
                        FragmentNavigatorExtras(
                                btn_move_next to getString(R.string.intro_flow_button_transition_name)
                        )
                )
                is DefaultOneTimeEvents.Error -> it.error.show(this)
            }
        }
    }
}