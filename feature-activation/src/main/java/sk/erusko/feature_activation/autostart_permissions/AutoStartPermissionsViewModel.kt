package sk.erusko.feature_activation.autostart_permissions

import androidx.hilt.lifecycle.ViewModelInject
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.vm.BaseViewModel
import base.base_app.core.base.vm.emitDefaultEvent
import base.base_app.core.base.vm.launchOnIo
import base.base_app.core.helpers.BatterySaverManager


class AutoStartPermissionsViewModel @ViewModelInject constructor(
        private val batterySaverManager: BatterySaverManager
) : BaseViewModel<Unit>(Unit) {

    fun getAutoStartPermissions() {
        batterySaverManager.getAutoStartPermission()
    }

    fun moveNext() {
        launchOnIo {
            emitDefaultEvent(DefaultOneTimeEvents.NavigateNext())
        }
    }
}