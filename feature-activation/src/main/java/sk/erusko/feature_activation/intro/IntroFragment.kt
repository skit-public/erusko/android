package sk.erusko.feature_activation.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.entities.events.Events
import base.base_app.core.helpers.SpanHelper
import base.base_app.core.helpers.StringResource
import base.base_app.core.ui.clicksThrottledNextButton
import base.base_app.core.ui.views.spans.SpanV1
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_intro_scroll.*
import kotlinx.android.synthetic.main.fragment_intro_static.*
import sk.erusko.feature_activation.R

@AndroidEntryPoint
class IntroFragment : BaseFragment() {

    override fun getToolbarConfig(): ToolbarConfig? = ToolbarConfig(
            title = StringResource.StringIdResource(R.string.intro_toolbar_title),
            imageResource = R.drawable.ic_grouping
    )

    override fun getScrollContentResId(): Int? = R.layout.fragment_intro_scroll

    override fun getStaticContentResId(): Int? = R.layout.fragment_intro_static

    override fun init(view: View, savedInstanceState: Bundle?) {
        btn_next.clicksThrottledNextButton(lifecycleScope) {
            eventTracker.trackEvent(Events.InitialActivationStarted)
            findNavController().navigate(IntroFragmentDirections.actionIntroFragmentToActivationFragment(), FragmentNavigatorExtras(
                    btn_next to getString(R.string.intro_flow_button_transition_name)
            ))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState).also {
            sharedElementEnterTransition = TransitionInflater.from(this.context).inflateTransition(R.transition.change_bounds)
            sharedElementReturnTransition = TransitionInflater.from(this.context).inflateTransition(R.transition.change_bounds)
        }
    }

    override fun render() {
        tv_intro_description.text = SpanHelper.replace(getString(R.string.intro_body),
                *listOf(
                        SpanHelper.SpanParam(SpanHelper.OnClickSpan(tv_intro_description, {
                            findNavController().navigate(IntroFragmentDirections.actionIntroFragmentToHelpFragment(
                                    headline = StringResource.StringIdResource(R.string.about_toolbar_title),
                                    title = StringResource.StringIdResource(R.string.about_title),
                                    body = StringResource.StringIdResource(R.string.about_body),
                                    imageResource = R.drawable.ic_virus
                            ))
                        }), SpanV1(requireContext())),
                        SpanHelper.SpanParam(SpanHelper.OnClickSpan(tv_intro_description, {
                            findNavController().navigate(IntroFragmentDirections.actionIntroFragmentToHelpFragment(
                                    headline = StringResource.StringIdResource(R.string.info_toolbar_title),
                                    body = StringResource.StringIdResource(R.string.info_body)
                            ))
                        }), SpanV1(requireContext()))
                ).toTypedArray()
        )
    }
}