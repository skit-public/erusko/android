package sk.erusko.feature_activation.activate

import androidx.hilt.lifecycle.ViewModelInject
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.vm.BaseViewModel
import base.base_app.core.base.vm.emitDefaultEvent
import base.base_app.core.base.vm.launchOnIo
import base.base_app.core.entities.error_handler.ErrorHandler
import base.base_app.core.entities.preferences.IntroProgress
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.features.exposure.ExposuresInteractor
import base.base_app.core.helpers.BatterySaverManager


class ActivationViewModel @ViewModelInject constructor(
        private val errorHandler: ErrorHandler,
        private val exposuresInteractor: ExposuresInteractor,
        private val batterySaverManager: BatterySaverManager,
        private val persistentAppPreferences: PersistentAppPreferences
) : BaseViewModel<Unit>(Unit) {

    init {
        persistentAppPreferences.setProgress(IntroProgress.PERMISSIONS)
    }

    fun activateClicked() {
        activateNotificationsApi()
    }

    fun recheckActivation() {
        launchOnIo {
            if (kotlin.runCatching { exposuresInteractor.isEnabled() }.getOrElse { false }) {
                moveNext()
            } else {
                activateNotificationsApi()
            }
        }
    }

    private fun activateNotificationsApi() {
        launchOnIo {
            kotlin.runCatching {
                exposuresInteractor.start()
            }.onSuccess {
                moveNext()
            }.onFailure {
                emitDefaultEvent(DefaultOneTimeEvents.Error(errorHandler.mapError(it)))
            }
        }
    }

    private suspend fun moveNext() {
        emitDefaultEvent(DefaultOneTimeEvents.NavigateNext(
                if (batterySaverManager.isAutoStartPermissionsAvailable()) {
                    ActivationFragmentDirections.actionActivationFragmentToAutostartFragment()
                } else {
                    ActivationFragmentDirections.actionActivationFragmentToPrivacyFragment()
                }
        ))
    }
}