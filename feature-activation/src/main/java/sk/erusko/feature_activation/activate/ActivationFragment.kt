package sk.erusko.feature_activation.activate

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.base.vm.renderCustomEvents
import base.base_app.core.base.vm.renderDefaultEvents
import base.base_app.core.entities.events.Events
import base.base_app.core.features.exposure.ExposureResolutionRequestHandler
import base.base_app.core.helpers.StringResource
import base.base_app.core.helpers.dialog.BottomSheetDialogBuilder
import base.base_app.core.helpers.show
import base.base_app.core.ui.clicksThrottled
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_activation_scroll.*
import kotlinx.android.synthetic.main.fragment_activation_static.*
import sk.erusko.feature_activation.R
import javax.inject.Inject

@AndroidEntryPoint
class ActivationFragment : BaseFragment() {

    @Inject
    lateinit var exposureResolutionRequestHandler: ExposureResolutionRequestHandler

    private val viewModel: ActivationViewModel by viewModels()

    override fun getScrollContentResId(): Int? = R.layout.fragment_activation_scroll

    override fun getStaticContentResId(): Int? = R.layout.fragment_activation_static

    override fun getToolbarConfig(): ToolbarConfig? = ToolbarConfig(
            title = StringResource.StringIdResource(R.string.activation_toolbar_title),
            imageResource = R.drawable.ic_desinformation
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState).also {
            sharedElementEnterTransition = TransitionInflater.from(this.context).inflateTransition(R.transition.change_bounds)
            sharedElementReturnTransition = TransitionInflater.from(this.context).inflateTransition(R.transition.change_bounds)
        }
    }

    override fun init(view: View, savedInstanceState: Bundle?) {
        activity?.let {
            lifecycle.addObserver(exposureResolutionRequestHandler.registerListener(it.activityResultRegistry) {
                if (it.resultCode == Activity.RESULT_OK) {
                    viewModel.recheckActivation()
                } else {
                    eventTracker.trackEvent(Events.InitialActivationPermissionDenied)
                }
            })
        }

        btn_activate.clicksThrottled(lifecycleScope, 3000) {
            viewModel.activateClicked()
        }
    }

    override fun render() {
        tv_intro_description.text = getString(R.string.activation_notifications_body)

        viewModel.renderDefaultEvents(lifecycleScope) {
            when (it) {
                is DefaultOneTimeEvents.NavigateNext -> findNavController().navigate((it.data as? ActionOnlyNavDirections)
                        ?: ActivationFragmentDirections.actionActivationFragmentToPrivacyFragment(),
                        FragmentNavigatorExtras(
                                btn_activate to getString(R.string.intro_flow_button_transition_name)
                        )
                )
                is DefaultOneTimeEvents.Error -> it.error.show(this)
            }
        }
    }
}