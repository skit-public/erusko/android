package sk.erusko.feature_exposure_notifications.api

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root
import retrofit2.http.GET

interface ExposureKeyServerFilesApi {
    @GET(".")
    suspend fun getKeyFileUrls(): KeyServerListingResponse
}


@Root(name = "ListBucketResult ", strict = false)
data class KeyServerListingResponse @JvmOverloads constructor(
        @field:ElementList(name = "Contents", required = false, inline = true)
        @param:ElementList(name = "Contents", required = false, inline = true)
        var files: MutableList<Files>? = mutableListOf()
)

@Root(name = "Contents", strict = false)
data class Files @JvmOverloads constructor(
        @field:Element(name = "Key", required = false)
        @param:Element(name = "Key", required = false)
        var filePath: String? = null,

        @field:Element(name = "ETag", required = false)
        @param:Element(name = "ETag", required = false)
        var eTag: String? = null
)