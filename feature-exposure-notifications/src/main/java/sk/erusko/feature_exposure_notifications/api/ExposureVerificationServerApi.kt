package sk.erusko.feature_exposure_notifications.api

import base.base_app.core.features.exposure.models.verification_server.VerifyCertificateRequest
import base.base_app.core.features.exposure.models.verification_server.VerifyCertificateResponse
import base.base_app.core.features.exposure.models.verification_server.VerifyCodeRequest
import base.base_app.core.features.exposure.models.verification_server.VerifyCodeResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface ExposureVerificationServerApi {

    @POST("api/verify")
    suspend fun verifyCode(@Body request: VerifyCodeRequest): VerifyCodeResponse

    @POST("api/certificate")
    suspend fun verifyCertificate(@Body request: VerifyCertificateRequest): VerifyCertificateResponse
}