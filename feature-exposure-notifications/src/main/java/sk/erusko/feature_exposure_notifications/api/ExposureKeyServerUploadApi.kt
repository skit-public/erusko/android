package sk.erusko.feature_exposure_notifications.api

import base.base_app.core.features.exposure.models.key_sever.ExposureRequest
import base.base_app.core.features.exposure.models.key_sever.ExposureResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface ExposureKeyServerUploadApi {

    @POST("v1/publish")
    suspend fun reportExposure(@Body exposureRequest: ExposureRequest): ExposureResponse
}