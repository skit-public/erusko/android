package sk.erusko.feature_exposure_notifications.api

import base.base_app.core.features.exposure.models.verification_server.VerificationResponseException
import base.base_app.core.features.exposure.models.verification_server.VerifyCodeErrorResponse
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class VerificationServerErrorInterceptor(private val json: Json) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return processRequest(chain, chain.request())
    }

    private fun processRequest(chain: Interceptor.Chain, request: Request): Response {
        val response = chain.proceed(request)
        if (!response.isSuccessful) {
            throw VerificationResponseException(handleError(response.body?.string()))
        }
        return response
    }
    private fun handleError(body: String?): VerifyCodeErrorResponse {
        return try {
            json.decodeFromString(body.toString())
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}