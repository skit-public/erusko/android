package sk.erusko.feature_exposure_notifications.business

import android.content.Context
import android.util.Base64
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.features.exposure.ExposuresInteractor
import base.base_app.core.features.exposure.models.key_sever.DownloadedKeys
import base.base_app.core.features.exposure.models.key_sever.ExposureRequest
import base.base_app.core.features.exposure.models.key_sever.TemporaryExposureKeyDto
import base.base_app.core.features.exposure.models.verification_server.VerifyCertificateRequest
import base.base_app.core.features.exposure.models.verification_server.VerifyCodeRequest
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.hoursToMillis
import base.base_app.core.helpers.newRandomPadding
import com.google.android.gms.nearby.exposurenotification.DailySummary
import com.google.android.gms.nearby.exposurenotification.ExposureWindow
import com.google.android.gms.nearby.exposurenotification.TemporaryExposureKey
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import sk.erusko.feature_exposure_notifications.helper.ExposureCryptoHelper
import sk.erusko.feature_exposure_notifications.notifications.ExposuresNotificationsHelper
import sk.erusko.feature_exposure_notifications.preferences.ExposureNotificationsPreferences
import java.io.File
import javax.inject.Inject

class ExposuresInteractorImpl @Inject constructor(
        @ApplicationContext private val context: Context,
        private val exposureNotificationsPreferences: ExposureNotificationsPreferences,
        private val configProvider: ConfigProvider,
        private val exposureVerificationServerInteractor: ExposureVerificationServerInteractor,
        private val exposureKeyServerInteractor: ExposureKeyServerInteractor,
        private val exposureNotificationsSdkWrapper: ExposureNotificationsSdkWrapper,
        private val cryptoTools: ExposureCryptoHelper,
        private val eventTracker: EventTracker
) : ExposuresInteractor {

    override suspend fun start() = exposureNotificationsSdkWrapper.startExposuresTracking()

    override suspend fun stop() = exposureNotificationsSdkWrapper.stopExposuresTracking()

    override suspend fun isEnabled(): Boolean = exposureNotificationsSdkWrapper.isEnabled()

    private suspend fun getTemporaryExposureKeyHistory(): List<TemporaryExposureKey> = exposureNotificationsSdkWrapper.getTemporaryExposureKeyHistory()

    override suspend fun getDailySummaries(): List<DailySummary> = exposureNotificationsSdkWrapper.getDailySummaries()

    override suspend fun getExposureWindows(): List<ExposureWindow> = exposureNotificationsSdkWrapper.getExposureWindows()

    override fun deviceSupportsLocationlessScanning(): Boolean = exposureNotificationsSdkWrapper.deviceSupportsLocationlessScanning()

    private suspend fun downloadKeyExport(): DownloadedKeys = exposureKeyServerInteractor.getKeyFiles()

    override suspend fun downloadKeysAndReportExposures() {
        val previousKeys = exposureNotificationsPreferences.lastKeyImportedFiles
        kotlin.runCatching {
            provideDiagnosisKeys(downloadKeyExport())
            notifyAboutNewExposures(context)
        }.onFailure {
            Logger.et { it }
            exposureNotificationsPreferences.lastKeyImportedFiles = previousKeys
            deleteFiles()
        }.onSuccess {
            Logger.dm { "Successfully downloaded and processed keys." }
            deleteFiles()
        }
    }

    private suspend fun provideDiagnosisKeys(keys: DownloadedKeys) {
        Logger.dm { "Providing diagnosis keys to SDK: $keys" }
        exposureNotificationsSdkWrapper.setDiagnosisKeysMapping()
        if (keys.keys.isNotEmpty()) {
            exposureNotificationsSdkWrapper.provideDiagnosisKeys(keys.keys.map { it.file })
        } else {
            Logger.dm { "Skipping providing diagnosis keys, no new files." }
        }
        exposureNotificationsPreferences.setLastKeyImportTime()
    }

    private suspend fun getLastRiskyExposure(): DailySummary? {
        return getDailySummaries().maxByOrNull { it.daysSinceEpoch }
                .also {
                    if (it == null) {
                        Logger.dm { "Last risky exposure not found" }
                    } else {
                        Logger.dm { "Last risky exposure: $it" }
                    }
                }
    }

    override suspend fun reportExposureWithVerification(code: String): Int {
        val keys = getTemporaryExposureKeyHistory()
        val verifyResponse = exposureVerificationServerInteractor.verifyCode(VerifyCodeRequest(code, newRandomPadding()))

        val hmackey = cryptoTools.newHmacKey()
        val keyHash = cryptoTools.hashedKeys(keys, hmackey)

        val certificateResponse = exposureVerificationServerInteractor.verifyCertificate(
                VerifyCertificateRequest(verifyResponse.token.orEmpty(), keyHash)
        )

        val reportExposuresRequest = ExposureRequest(
                temporaryExposureKeys = keys.map {
                    TemporaryExposureKeyDto(
                            Base64.encodeToString(it.keyData, Base64.NO_WRAP),
                            it.rollingStartIntervalNumber,
                            it.rollingPeriod,
                            it.transmissionRiskLevel
                    )
                },
                verificationPayload = certificateResponse.certificate,
                hmackey = hmackey,
                symptomOnsetInterval = null,
                revisionToken = exposureNotificationsPreferences.revisionToken,
                healthAuthorityID = configProvider.provide().exposuresServerConfig().healthAuthorityID
        )
        val response = exposureKeyServerInteractor.reportExposure(reportExposuresRequest)

        response.revisionToken?.let {
            exposureNotificationsPreferences.revisionToken = it
        }

        return response.insertedExposures ?: 0
    }

    override fun notifyAboutNewExposures(context: Context) {
        GlobalScope.launch {
            kotlin.runCatching {
                val lastExposure = getLastRiskyExposure()?.daysSinceEpoch
                val lastNotifiedExposure = exposureNotificationsPreferences.lastNotifiedExposure
                if (lastExposure != null && lastExposure != lastNotifiedExposure) {
                    eventTracker.trackEvent(Events.UserExposedNotificationShown)
                    ExposuresNotificationsHelper.showRiskyExposureNotification(context)
                    exposureNotificationsPreferences.lastNotifiedExposure = lastExposure
                }
            }.onFailure {
                Logger.et { it }
            }
        }
    }

    override suspend fun isEligibleToDownloadKeys(): Boolean {
        return isEnabled() && System.currentTimeMillis() - exposureNotificationsPreferences.lastKeyImportTime >=
                configProvider.provide().exposuresDataConfig().diagnosisKeysPeriodicFrameHours.hoursToMillis()
    }

    override fun deleteFiles() {
        val extractedDir = File(context.cacheDir.path + "/export/")
        extractedDir.deleteRecursively()
    }
}