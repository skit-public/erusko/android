package sk.erusko.feature_exposure_notifications.business

import android.content.Context
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.features.exposure.models.key_sever.DownloadedKeys
import base.base_app.core.features.exposure.models.key_sever.ExposureRequest
import base.base_app.core.features.exposure.models.key_sever.ExposureResponse
import base.base_app.core.features.exposure.models.key_sever.KeyData
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.asJsonConverterFactory
import base.base_app.core.helpers.copyAndCloseTo
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import sk.erusko.feature_exposure_notifications.api.ExposureKeyServerFilesApi
import sk.erusko.feature_exposure_notifications.api.ExposureKeyServerUploadApi
import sk.erusko.feature_exposure_notifications.api.KeyServerErrorInterceptor
import sk.erusko.feature_exposure_notifications.preferences.Etag
import sk.erusko.feature_exposure_notifications.preferences.ExposureNotificationsPreferences
import java.io.File
import java.net.URL
import javax.inject.Inject


interface ExposureKeyServerInteractor {
    suspend fun reportExposure(request: ExposureRequest): ExposureResponse
    suspend fun getKeyFiles(): DownloadedKeys
}

class ExposureKeyServerInteractorImpl @Inject constructor(
        @ApplicationContext private val context: Context,
        private val configProvider: ConfigProvider,
        private val exposureNotificationsPreferences: ExposureNotificationsPreferences,
        private val eventTracker: EventTracker,
        private val json: Json,
        private val keyServerErrorInterceptor: KeyServerErrorInterceptor,
        private val okHttpClient: OkHttpClient
) : ExposureKeyServerInteractor {

    private val keyServerUploadApiService: ExposureKeyServerUploadApi by lazy {
        Retrofit.Builder()
                .addConverterFactory(json.asJsonConverterFactory())
                .client(okHttpClient.newBuilder().addInterceptor(keyServerErrorInterceptor)
                        .build())
                .baseUrl(configProvider.provide().exposuresServerConfig().keyServerUrl)
                .build()
                .create(ExposureKeyServerUploadApi::class.java)
    }

    private val keyDownloadService by lazy {
        Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(configProvider.provide().exposuresServerConfig().keyExportUrl)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build()
                .create(ExposureKeyServerFilesApi::class.java)
    }

    override suspend fun reportExposure(request: ExposureRequest): ExposureResponse {
        return withContext(Dispatchers.IO) {
            kotlin.runCatching {
                keyServerUploadApiService.reportExposure(request)
            }.onFailure {
                eventTracker.trackEvent(Events.KeyServerError)
            }.getOrThrow()
        }
    }

    override suspend fun getKeyFiles(): DownloadedKeys {
        return withContext(Dispatchers.IO) {
            val response = keyDownloadService.getKeyFileUrls()
                    .also { Logger.dm { "getKeyFiles response: $it" } }
                    .files
                    .orEmpty()
                    .filter { !it.eTag.isNullOrBlank() && !it.filePath.isNullOrBlank() && !it.filePath.orEmpty().contains("index.txt") }

            val newFiles = response.filter { !exposureNotificationsPreferences.lastKeyImportedFiles.orEmpty().map { it.eTag }.contains(it.eTag.orEmpty()) }

            Logger.dm { "getKeyFiles new files: $newFiles" }

            val files = newFiles.map {
                Pair(configProvider.provide().exposuresServerConfig().keyExportUrl + it.filePath, it.eTag)
            }.mapNotNull { (url, tag) ->
                withContext(Dispatchers.Default) { downloadFile(url) }?.let {
                    KeyData(it, tag.orEmpty())
                }
            }

            Logger.dm { "getKeyFiles previous eTags: ${exposureNotificationsPreferences.lastKeyImportedFiles}" }
            exposureNotificationsPreferences.lastKeyImportedFiles = response.mapNotNull { it.eTag }
                    .minus(newFiles.mapNotNull { it.eTag })
                    .plus(files.mapNotNull { it.tag })
                    .map { Etag(it) }
                    .also {
                        Logger.dm { "getKeyFiles storing eTags: $it" }
                    }

            Logger.dm { "getKeyFiles download succeeded: $files key files" }

            DownloadedKeys(files)
        }
    }

    private fun downloadFile(zipFile: String): File? {
        try {
            val defaultPath = context.cacheDir.path + "/export/"
            val dir = File(defaultPath)
            dir.mkdirs()
            val file = File(defaultPath + zipFile.substring(zipFile.lastIndexOf("/") + 1))
            file.createNewFile()
            URL(zipFile).openStream().copyAndCloseTo(file)
            Logger.dm { "Downloaded diagnosis file: $zipFile" }
            return file
        } catch (t: Throwable) {
            Logger.et { t }
        }
        return null
    }
}