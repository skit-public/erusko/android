package sk.erusko.feature_exposure_notifications.business

import android.content.Context
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.daysToMillis
import com.google.android.gms.nearby.exposurenotification.*
import dagger.hilt.android.qualifiers.ApplicationContext
import sk.erusko.feature_exposure_notifications.notifications.ExposuresNotificationsHelper
import sk.erusko.feature_exposure_notifications.preferences.ExposureNotificationsPreferences
import java.io.File
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

interface ExposureNotificationsSdkWrapper {
    suspend fun startExposuresTracking()
    suspend fun stopExposuresTracking()
    suspend fun isEnabled(): Boolean
    suspend fun getTemporaryExposureKeyHistory(): List<TemporaryExposureKey>
    suspend fun getExposureWindows(): List<ExposureWindow>
    suspend fun provideDiagnosisKeys(files: List<File>): Boolean
    suspend fun setDiagnosisKeysMapping()
    suspend fun getDailySummaries(): List<DailySummary>
    fun deviceSupportsLocationlessScanning(): Boolean
}

class ExposureNotificationsSdkWrapperImpl @Inject constructor(
        private val exposureSdk: ExposureNotificationClient,
        private val exposureNotificationsPreferences: ExposureNotificationsPreferences,
        private val configProvider: ConfigProvider,
        @ApplicationContext val context: Context,
        private val persistentAppPreferences: PersistentAppPreferences
) : ExposureNotificationsSdkWrapper {

    override suspend fun startExposuresTracking() = suspendCoroutine<Unit> { cont ->
        exposureSdk.start()
                .addOnSuccessListener {
                    Logger.dm { "Start tracking succeeded" }
                    runCatching {
                        ExposuresNotificationsHelper.closeNotRunningNotification(context)
                    }
                    cont.resume(Unit)
                }.addOnFailureListener {
                    Logger.et { it }
                    cont.resumeWithException(it)
                }
    }

    override suspend fun stopExposuresTracking() = suspendCoroutine<Unit> { cont ->
        exposureSdk.stop()
                .addOnSuccessListener {
                    Logger.dm { "Stop tracking succeeded" }
                    cont.resume(Unit)
                }.addOnFailureListener {
                    Logger.et { it }
                    cont.resumeWithException(it)
                }
    }

    override suspend fun isEnabled(): Boolean = suspendCoroutine { cont ->
        exposureSdk.isEnabled
                .addOnSuccessListener {
                    Logger.dm { "isEnabled succeeded: $it" }
                    cont.resume(it)
                }.addOnFailureListener {
                    Logger.et { it }
                    cont.resumeWithException(it)
                }
    }

    override suspend fun getTemporaryExposureKeyHistory(): List<TemporaryExposureKey> =
            suspendCoroutine { cont ->
                exposureSdk.temporaryExposureKeyHistory.addOnSuccessListener {
                    Logger.dm { "Retrieving temporary exposure key history succeeded: $it" }
                    cont.resume(it)
                }.addOnFailureListener {
                    Logger.et { it }
                    cont.resumeWithException(it)
                }
            }

    override suspend fun getExposureWindows(): List<ExposureWindow> = suspendCoroutine { cont ->
        exposureSdk.exposureWindows
                .addOnSuccessListener {
                    Logger.dm { "Retrieving exposure windows succeeded: $it" }
                    cont.resume(it)
                }.addOnFailureListener {
                    Logger.et { it }
                    cont.resumeWithException(it)
                }
    }

    override suspend fun provideDiagnosisKeys(files: List<File>): Boolean = suspendCoroutine { cont ->
        Logger.dm { "Importing files started: $files" }
        persistentAppPreferences.numberOfNewFiles = files.size
        exposureSdk.provideDiagnosisKeys(DiagnosisKeyFileProvider(files))
                .addOnSuccessListener {
                    Logger.dm { "Importing diagnosis keys succeeded" }
                    cont.resume(true)
                }.addOnFailureListener {
                    Logger.et { it }
                    cont.resumeWithException(it)
                }
    }

    override suspend fun setDiagnosisKeysMapping(): Unit = suspendCoroutine { cont ->
        if (System.currentTimeMillis() - exposureNotificationsPreferences.lastSetDiagnosisKeysDataMapping > configProvider.provide().exposuresDataConfig().diagnosisKeysDataMappingLimitDays.daysToMillis()) {
            val daysList = configProvider.provide().exposuresDataConfig().daysSinceOnsetToInfectiousness
            val daysToInfectiousness = mutableMapOf<Int, Int>()
            (-14..14).forEach {
                daysList.getOrNull(it + 14)?.let { day ->
                    daysToInfectiousness[it] = day
                }
            }
            val mapping = DiagnosisKeysDataMapping.DiagnosisKeysDataMappingBuilder()
                    .setDaysSinceOnsetToInfectiousness(daysToInfectiousness)
                    .setInfectiousnessWhenDaysSinceOnsetMissing(Infectiousness.NONE)
                    .setReportTypeWhenMissing(configProvider.provide().exposuresDataConfig().reportTypeWhenMissing)
                    .build()

            Logger.dm { "Setting mapping: $mapping" }

            exposureSdk.setDiagnosisKeysDataMapping(mapping)
                    .addOnSuccessListener {
                        Logger.dm { "Setting diagnosis keys mapping succeeded" }
                        exposureNotificationsPreferences.lastSetDiagnosisKeysDataMapping = System.currentTimeMillis()
                    }.addOnFailureListener {
                        Logger.et { it }
                    }.addOnCompleteListener {
                        cont.resume(Unit)
                    }
        } else {
            cont.resume(Unit)
            Logger.dm { "Skipping providing diagnosis mapping" }
        }
    }

    override suspend fun getDailySummaries(): List<DailySummary> = suspendCoroutine { cont ->
        val reportTypeWeights = configProvider.provide().exposuresDataConfig().reportTypeWeights
        val attenuationBucketThresholdDb = configProvider.provide().exposuresDataConfig().attenuationBucketThresholdDb
        val attenuationBucketWeights = configProvider.provide().exposuresDataConfig().attenuationBucketWeights
        val infectiousnessWeights = configProvider.provide().exposuresDataConfig().infectiousnessWeights

        val dailySummariesConfig = DailySummariesConfig.DailySummariesConfigBuilder()
                .apply {
                    (1..4).forEach {
                        reportTypeWeights.elementAtOrNull(it - 1)?.let { value ->
                            setReportTypeWeight(it, value)
                        }
                    }
                    setAttenuationBuckets(attenuationBucketThresholdDb, attenuationBucketWeights)
                    // TODO sync with iOS if we can remove first value form remote config
                    (1..2).forEach {
                        setInfectiousnessWeight(it, infectiousnessWeights[it])
                    }
                    setMinimumWindowScore(configProvider.provide().exposuresDataConfig().minimumWindowScore)
                }.build()

        exposureSdk.getDailySummaries(dailySummariesConfig)
                .addOnSuccessListener {
                    Logger.dm { "Retrieving daily summaries succeeded: $it" }
                    cont.resume(it)
                }.addOnFailureListener {
                    Logger.et { it }
                    cont.resumeWithException(it)
                }
    }

    override fun deviceSupportsLocationlessScanning(): Boolean {
        return exposureSdk.deviceSupportsLocationlessScanning()
    }

}