package sk.erusko.feature_exposure_notifications.business

import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.features.exposure.models.verification_server.*
import base.base_app.core.helpers.asJsonConverterFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import sk.erusko.feature_exposure_notifications.api.ExposureVerificationServerApi
import sk.erusko.feature_exposure_notifications.api.VerificationServerErrorInterceptor
import javax.inject.Inject

interface ExposureVerificationServerInteractor {
    suspend fun verifyCode(request: VerifyCodeRequest): VerifyCodeResponse
    suspend fun verifyCertificate(request: VerifyCertificateRequest): VerifyCertificateResponse
}

class ExposureVerificationServerInteractorImpl @Inject constructor(
        private val eventTracker: EventTracker,
        private val json: Json,
        private val configProvider: ConfigProvider,
        private val okHttpClient: OkHttpClient,
        private val verificationServerErrorInterceptor: VerificationServerErrorInterceptor
) : ExposureVerificationServerInteractor {

    private val verificationServerApiService: ExposureVerificationServerApi by lazy {
        Retrofit.Builder()
                .addConverterFactory(json.asJsonConverterFactory())
                .client(okHttpClient)
                .client(okHttpClient.newBuilder()
                        .addInterceptor {
                            val request = it.request().newBuilder()
                                    .addHeader("X-API-Key", configProvider.provide().exposuresServerConfig().verificationServerApiKey)
                                    .build()
                            it.proceed(request)
                        }
                        .addInterceptor(verificationServerErrorInterceptor)
                        .build())
                .baseUrl(configProvider.provide().exposuresServerConfig().verificationServerUrl)
                .build()
                .create(ExposureVerificationServerApi::class.java)
    }

    override suspend fun verifyCode(request: VerifyCodeRequest): VerifyCodeResponse {
        return withContext(Dispatchers.IO) {
            kotlin.runCatching {
                verificationServerApiService.verifyCode(request)
            }.onSuccess {
                eventTracker.trackEvent(Events.VerificationServerCodeVerified)
            }.onFailure {
                eventTracker.trackEvent(Events.VerificationServerError((it as? VerificationResponseException?)?.errorResponse?.errorCode?.name?.toLowerCase()
                        ?: "unknown"))
            }.getOrThrow()
        }
    }

    override suspend fun verifyCertificate(request: VerifyCertificateRequest): VerifyCertificateResponse {
        return withContext(Dispatchers.IO) {
            verificationServerApiService.verifyCertificate(request)
        }
    }
}