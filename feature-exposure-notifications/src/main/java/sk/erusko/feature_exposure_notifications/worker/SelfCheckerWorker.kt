package sk.erusko.feature_exposure_notifications.worker

import android.content.Context
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.features.exposure.ExposuresInteractor
import base.base_app.core.helpers.Logger
import sk.erusko.feature_exposure_notifications.notifications.ExposuresNotificationsHelper
import sk.erusko.feature_exposure_notifications.preferences.ExposureNotificationsPreferences
import java.util.*

class SelfCheckerWorker @WorkerInject constructor(
        @Assisted val context: Context,
        @Assisted workerParams: WorkerParameters,
        private val exposureNotificationsPreferences: ExposureNotificationsPreferences,
        private val exposuresInteractor: ExposuresInteractor,
        private val configProvider: ConfigProvider,
        private val persistentAppPreferences: PersistentAppPreferences
) : CoroutineWorker(context, workerParams) {

    companion object {
        const val TAG = "SELF_CHECKER"
    }

    override suspend fun doWork(): Result {
        if (persistentAppPreferences.isFirstActivationFlow) {
            return Result.success()
        }
        val hour = Calendar.getInstance(Locale.getDefault()).get(Calendar.HOUR_OF_DAY)
        val isUserSleeping = hour in (configProvider.provide().exposuresDataConfig().selfCheckerHours.getOrNull(0)
                ?: 9)..(configProvider.provide().exposuresDataConfig().selfCheckerHours.getOrNull(1)
                ?: 19)

        return if (isUserSleeping) {
            try {
                val isEnabled = exposuresInteractor.isEnabled()
                val hasOutdatedData = exposureNotificationsPreferences.hasOutdatedKeyData()

                if (!isEnabled) {
                    ExposuresNotificationsHelper.showExposureApiPausedNotification(context)
                }

                if (hasOutdatedData) {
                    ExposuresNotificationsHelper.showOutdatedDataNotification(context)
                }

                exposuresInteractor.notifyAboutNewExposures(context)

                ExposuresNotificationsHelper.createDebugNotification(context, persistentAppPreferences, "Self checker", "isEnabled: $isEnabled, hasOutdatedData: $hasOutdatedData")
                Result.success()
            } catch (e: Exception) {
                Logger.et { e }
                Result.failure()
            }
        } else {
            Result.success()
        }
    }
}