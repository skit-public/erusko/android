package sk.erusko.feature_exposure_notifications.worker

import android.content.Context
import androidx.work.*
import base.base_app.core.entities.config.static_config.ConfigProvider
import base.base_app.core.features.workers.ExposuresWorkerScheduler
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ExposuresWorkerSchedulerImpl @Inject constructor(
        @ApplicationContext private val context: Context,
        private val configProvider: ConfigProvider
) : ExposuresWorkerScheduler {

    override fun scheduleWorkers() {
        scheduleDownloadKeysWorker()
        scheduleSelfCheckWorker()
    }

    private fun scheduleDownloadKeysWorker() {
        val worker = PeriodicWorkRequestBuilder<DownloadKeysWorker>(
                configProvider.provide().exposuresDataConfig().diagnosisKeysPeriodicFrameHours,
                TimeUnit.HOURS
        ).setConstraints(
                Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build()
        ).addTag(DownloadKeysWorker.TAG)
                .build()

        WorkManager.getInstance(context)
                .enqueueUniquePeriodicWork(
                        DownloadKeysWorker.TAG,
                        ExistingPeriodicWorkPolicy.REPLACE,
                        worker
                )
    }

    private fun scheduleSelfCheckWorker() {
        val worker = PeriodicWorkRequestBuilder<SelfCheckerWorker>(
                configProvider.provide().exposuresDataConfig().selfCheckerPeriodHours,
                TimeUnit.HOURS)
                .addTag(SelfCheckerWorker.TAG)
                .build()

        WorkManager.getInstance(context)
                .enqueueUniquePeriodicWork(
                        SelfCheckerWorker.TAG,
                        ExistingPeriodicWorkPolicy.REPLACE,
                        worker
                )
    }
}