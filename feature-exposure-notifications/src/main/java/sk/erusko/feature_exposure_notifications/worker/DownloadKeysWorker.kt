package sk.erusko.feature_exposure_notifications.worker

import android.content.Context
import androidx.hilt.Assisted
import androidx.hilt.work.WorkerInject
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.features.exposure.ExposuresInteractor
import base.base_app.core.helpers.Logger
import sk.erusko.feature_exposure_notifications.notifications.ExposuresNotificationsHelper

class DownloadKeysWorker @WorkerInject constructor(
        @Assisted val context: Context,
        @Assisted workerParams: WorkerParameters,
        private val exposuresInteractor: ExposuresInteractor,
        private val persistentAppPreferences: PersistentAppPreferences,
        private val eventTracker: EventTracker
) : CoroutineWorker(context, workerParams) {

    companion object {
        const val TAG = "DOWNLOAD_KEYS"
        const val DOWNLOAD_KEYS_NOTIFICATION_ID = 47911
    }

    override suspend fun doWork(): Result {
        if (persistentAppPreferences.isFirstActivationFlow) {
            return Result.success()
        }

        setForeground(getForegroundInfo())
        return try {
            val isAllowedToDownloadNewKeys = exposuresInteractor.isEligibleToDownloadKeys().also {
                Logger.dm { "isEligibleToDownloadKeys $it" }
            }

            if (isAllowedToDownloadNewKeys) {
                eventTracker.trackEvent(Events.DownloadKeysStarted)
                exposuresInteractor.downloadKeysAndReportExposures()
                eventTracker.trackEvent(Events.DownloadKeysFinished(persistentAppPreferences.numberOfNewFiles))
            } else {
                persistentAppPreferences.numberOfNewFiles = 0
            }
            val message = if (isAllowedToDownloadNewKeys) "New keys were successfully downloaded, in ${persistentAppPreferences.numberOfNewFiles} files, and provided to API" else "Not eligible to download new keys"

            ExposuresNotificationsHelper.createDebugNotification(context, persistentAppPreferences, "Download new keys", message)
            Result.success()
        } catch (t: Throwable) {
            Logger.et { t }
            ExposuresNotificationsHelper.createDebugNotification(context, persistentAppPreferences, "Download new keys", "Downloading failed ${t.localizedMessage ?: t.message.orEmpty()}")
            eventTracker.trackEvent(Events.DownloadKeysFailure)
            Result.failure()
        }
    }

    private fun getForegroundInfo(): ForegroundInfo {
        val notification = ExposuresNotificationsHelper.createDownloadWorkerNotification(context)
        return ForegroundInfo(DOWNLOAD_KEYS_NOTIFICATION_ID, notification)
    }
}