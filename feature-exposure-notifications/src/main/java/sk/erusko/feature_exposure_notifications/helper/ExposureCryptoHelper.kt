package sk.erusko.feature_exposure_notifications.helper

import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.decodeBase64
import base.base_app.core.helpers.encodeBase64
import com.google.android.gms.nearby.exposurenotification.TemporaryExposureKey
import java.nio.charset.StandardCharsets
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList
import kotlin.random.Random

@Singleton
class ExposureCryptoHelper @Inject constructor() {

    fun hashedKeys(keys: List<TemporaryExposureKey>, hmacKey: String): String {
        val cleartextSegments = ArrayList<HashedKeyData>()
        for (k in keys) {
            val base64key = k.keyData.encodeBase64()
            cleartextSegments.add(
                    HashedKeyData(
                            base64key,
                            String.format(
                                    Locale.ENGLISH,
                                    "%s.%d.%d",
                                    base64key,
                                    k.rollingStartIntervalNumber,
                                    k.rollingPeriod
                            )
                    )
            )
        }
        val cleartext = cleartextSegments.sortedBy { it.base64key }.joinToString(",") { it.data }
        Logger.dm { "${keys.size} keys for hashing prior to verification: [$cleartext]" }
        val mac = Mac.getInstance(HMAC_ALGO)
        mac.init(SecretKeySpec(hmacKey.decodeBase64(), HMAC_ALGO))
        return mac.doFinal(cleartext.toByteArray(StandardCharsets.UTF_8)).encodeBase64()
    }

    fun newHmacKey(): String {
        return Random.nextBytes(ByteArray(16)).encodeBase64()
    }

    private data class HashedKeyData(val base64key: String, val data: String)

    companion object {
        private const val HMAC_ALGO = "HmacSHA256"
    }
}
