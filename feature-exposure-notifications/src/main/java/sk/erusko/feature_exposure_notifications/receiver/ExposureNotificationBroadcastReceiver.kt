package sk.erusko.feature_exposure_notifications.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.features.exposure.ExposuresInteractor
import com.google.android.gms.nearby.exposurenotification.ExposureNotificationClient
import dagger.hilt.android.AndroidEntryPoint
import sk.erusko.feature_exposure_notifications.notifications.ExposuresNotificationsHelper
import javax.inject.Inject

@AndroidEntryPoint(BroadcastReceiver::class)
class ExposureNotificationBroadcastReceiver : Hilt_ExposureNotificationBroadcastReceiver() {

    @Inject
    internal lateinit var exposureNotificationsInteractor: ExposuresInteractor

    @Inject
    internal lateinit var persistentAppPreferences: PersistentAppPreferences

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        ExposuresNotificationsHelper.createDebugNotification(context, persistentAppPreferences, "Broadcast received", intent.action
                ?: "unknown action")
        if (intent.action == ExposureNotificationClient.ACTION_EXPOSURE_STATE_UPDATED) {
            exposureNotificationsInteractor.notifyAboutNewExposures(context)
        }
    }
}