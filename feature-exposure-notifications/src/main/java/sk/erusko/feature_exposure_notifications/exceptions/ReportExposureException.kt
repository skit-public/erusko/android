package sk.erusko.feature_exposure_notifications.exceptions

class ReportExposureException(errorMessage: String) : Throwable(errorMessage)
