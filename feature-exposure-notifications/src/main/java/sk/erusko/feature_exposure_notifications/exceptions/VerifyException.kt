package sk.erusko.feature_exposure_notifications.exceptions

class VerifyException(message: String) : Throwable(message)
