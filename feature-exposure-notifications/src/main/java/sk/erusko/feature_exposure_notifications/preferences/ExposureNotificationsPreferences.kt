package sk.erusko.feature_exposure_notifications.preferences

import base.base_app.core.entities.config.static_config.ConfigProvider
import sk.it.security_utils.preferences.EncryptedSharedPrefModel
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

interface ExposureNotificationsPreferences {
    var numberOfNewFiles: Int
    var lastNotifiedExposure: Int
    var revisionToken: String?
    var lastSetDiagnosisKeysDataMapping: Long
    var lastKeyImportTime: Long
    var lastKeyImportedFiles: List<Etag>

    fun setLastKeyImportTime()
    fun hasOutdatedKeyData(): Boolean
}

class ExposureNotificationsPreferencesImpl @Inject constructor(
        private val configProvider: ConfigProvider
) : EncryptedSharedPrefModel("62917cb17ersd4qq"), ExposureNotificationsPreferences {

    override var lastKeyImportedFiles: List<Etag> by gsonPref(listOf(), commitByDefault = true)
    override var numberOfNewFiles: Int by intPref(0, commitByDefault = true)
    override var lastNotifiedExposure: Int by intPref()
    override var revisionToken: String? by nullableStringPref()
    override var lastSetDiagnosisKeysDataMapping: Long by longPref(0L)
    override var lastKeyImportTime: Long by longPref(0L)

    override fun setLastKeyImportTime() {
        lastKeyImportTime = System.currentTimeMillis()
    }

    override fun hasOutdatedKeyData(): Boolean {
        return lastKeyImportTime != 0L && (System.currentTimeMillis() - lastKeyImportTime) / (1000 * 60 * 60) > configProvider.provide().exposuresDataConfig().keyImportDataOutdatedHours
    }
}

data class Etag(
        @SerializedName("eTag")
        val eTag: String
)