package sk.erusko.feature_exposure_notifications.di

import android.content.Context
import base.base_app.core.features.exposure.ExposuresInteractor
import base.base_app.core.features.workers.ExposuresWorkerScheduler
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.exposurenotification.ExposureNotificationClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.serialization.json.Json
import sk.erusko.feature_exposure_notifications.api.KeyServerErrorInterceptor
import sk.erusko.feature_exposure_notifications.api.VerificationServerErrorInterceptor
import sk.erusko.feature_exposure_notifications.business.*
import sk.erusko.feature_exposure_notifications.preferences.ExposureNotificationsPreferences
import sk.erusko.feature_exposure_notifications.preferences.ExposureNotificationsPreferencesImpl
import sk.erusko.feature_exposure_notifications.worker.ExposuresWorkerSchedulerImpl
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class ExposureNotificationsModule {

    @Provides
    @Singleton
    fun provideExposuresInteractor(impl: ExposuresInteractorImpl): ExposuresInteractor = impl

    @Provides
    @Singleton
    fun provideExposurePreferences(impl: ExposureNotificationsPreferencesImpl): ExposureNotificationsPreferences = impl

    @Provides
    @Singleton
    fun provideVerificationServerErrorInterceptor(json: Json): VerificationServerErrorInterceptor = VerificationServerErrorInterceptor(json)

    @Provides
    @Singleton
    fun provideKeyServerErrorInterceptor(json: Json): KeyServerErrorInterceptor = KeyServerErrorInterceptor(json)

    @Provides
    @Singleton
    fun provideExposureNotificationClient(@ApplicationContext context: Context): ExposureNotificationClient {
        return Nearby.getExposureNotificationClient(context)
    }

    @Provides
    @Singleton
    fun provideExposureVerificationServerInteractor(impl: ExposureVerificationServerInteractorImpl): ExposureVerificationServerInteractor = impl

    @Provides
    @Singleton
    fun provideExposureKeyServerInteractor(impl: ExposureKeyServerInteractorImpl): ExposureKeyServerInteractor = impl

    @Provides
    @Singleton
    fun provideExposureNotificationsSdkWrapper(impl: ExposureNotificationsSdkWrapperImpl): ExposureNotificationsSdkWrapper = impl

    @Provides
    fun provideExposuresWorkerScheduler(impl: ExposuresWorkerSchedulerImpl): ExposuresWorkerScheduler = impl
}