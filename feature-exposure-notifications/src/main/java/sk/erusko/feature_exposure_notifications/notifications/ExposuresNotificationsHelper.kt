package sk.erusko.feature_exposure_notifications.notifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import base.base_app.core.entities.preferences.PersistentAppPreferences
import base.base_app.core.ui.main.MainActivity
import base.base_app.core.ui.startup.StartupActivity
import sk.erusko.feature_exposure_notifications.BuildConfig
import sk.erusko.feature_exposure_notifications.R
import java.util.*

object ExposuresNotificationsHelper {

    private const val CHANNEL_ID_EXPOSURE = "EXPOSURE_SK"
    private const val CHANNEL_ID_OUTDATED_DATA = "OUTDATED_DATA_SL"
    private const val CHANNEL_ID_NOT_RUNNING = "NOT_RUNNING_SK"
    private const val CHANNEL_ID_DOWNLOAD_WORKER = "DOWNLOAD_WORKER"

    private const val REQ_ID_EXPOSURE = 103
    private const val REQ_ID_OUTDATED_DATA = 104
    private const val REQ_ID_NOT_RUNNING = 105
    private const val REQ_ID_DOWNLOAD_WORKER = 106

    fun closeNotRunningNotification(context: Context) {
        (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(REQ_ID_NOT_RUNNING)
    }

    fun createDebugNotification(context: Context, persistentAppPreferences: PersistentAppPreferences, title: String, message: String) {
        if (BuildConfig.DEBUG && persistentAppPreferences.showWorkerDebugNotifications) {
            val debugNotification = createNotification(
                    0,
                    0,
                    CHANNEL_ID_EXPOSURE,
                    context,
                    false,
                    titleText = "DEBUG: $title",
                    messageText = message
            )
            (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(Random().nextInt(), debugNotification)
        }
    }

    fun showExposureApiPausedNotification(context: Context) {
        createNotification(
                R.string.notification_scanning_paused_title,
                R.string.notification_scanning_paused_body,
                CHANNEL_ID_NOT_RUNNING,
                context
        ).showNotification(context, CHANNEL_ID_NOT_RUNNING)
    }

    fun showRiskyExposureNotification(context: Context) {
        createNotification(
                R.string.notification_exposed_title,
                R.string.notification_exposed_body,
                CHANNEL_ID_EXPOSURE,
                context,
                customPendingIntent = NavDeepLinkBuilder(context)
                        .setComponentName(MainActivity::class.java)
                        .setGraph(R.navigation.main_nav_graph)
                        .setDestination(R.id.encountersFragment)
                        .createPendingIntent()
        ).showNotification(context, CHANNEL_ID_EXPOSURE)
    }

    fun showOutdatedDataNotification(context: Context) {
        createNotification(
                R.string.notification_data_outdated_title,
                R.string.notification_data_outdated_body,
                CHANNEL_ID_OUTDATED_DATA,
                context
        ).showNotification(context, CHANNEL_ID_OUTDATED_DATA)
    }

    fun createDownloadWorkerNotification(context: Context): Notification {
        return createNotification(
                R.string.notification_downloading_keys_title,
                R.string.notification_downloading_keys_body,
                CHANNEL_ID_DOWNLOAD_WORKER,
                context,
                true
        )
    }

    private fun Notification.showNotification(context: Context, channelId: String) {
        (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(
                when (channelId) {
                    CHANNEL_ID_EXPOSURE -> REQ_ID_EXPOSURE
                    CHANNEL_ID_OUTDATED_DATA -> REQ_ID_OUTDATED_DATA
                    CHANNEL_ID_NOT_RUNNING -> REQ_ID_NOT_RUNNING
                    CHANNEL_ID_DOWNLOAD_WORKER -> REQ_ID_DOWNLOAD_WORKER
                    else -> 0
                }, this
        )
    }

    private fun createNotification(
            @StringRes title: Int,
            @StringRes text: Int,
            channelId: String,
            context: Context,
            isOnGoing: Boolean = false,
            @DrawableRes smallIcon: Int = R.drawable.app_icon,
            titleText: String? = null,
            messageText: String? = null,
            customPendingIntent: PendingIntent? = null
    ): Notification {
        createNotificationChannels(context)

        val contentIntent = PendingIntent.getActivity(
                context,
                0,
                Intent(context, StartupActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                },
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        return NotificationCompat.Builder(context, channelId)
                .setTicker(titleText ?: context.getString(title))
                .setContentTitle(titleText ?: context.getString(title))
                .setContentText(messageText ?: context.getString(text))
                .setContentIntent(customPendingIntent ?: contentIntent)
                .setOngoing(isOnGoing)
                .setSmallIcon(smallIcon)
                .setStyle(NotificationCompat.BigTextStyle().bigText(messageText
                        ?: context.getString(text)))
                .build()
    }

    private fun createNotificationChannels(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(
                    CHANNEL_ID_EXPOSURE,
                    context.getString(R.string.notifications_channel_exposures),
                    NotificationManager.IMPORTANCE_HIGH,
                    context
            )
            createNotificationChannel(
                    CHANNEL_ID_NOT_RUNNING,
                    context.getString(R.string.notifications_channel_disabled),
                    NotificationManager.IMPORTANCE_DEFAULT,
                    context
            )
            createNotificationChannel(
                    CHANNEL_ID_OUTDATED_DATA,
                    context.getString(R.string.notifications_channel_outdated_data),
                    NotificationManager.IMPORTANCE_DEFAULT,
                    context
            )
            createNotificationChannel(
                    CHANNEL_ID_DOWNLOAD_WORKER,
                    context.getString(R.string.notifications_channel_worker),
                    NotificationManager.IMPORTANCE_LOW,
                    context
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(id: String, name: String, importance: Int, context: Context): NotificationChannel {
        return NotificationChannel(id, name, importance).also {
            (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(it)
        }
    }
}