package sk.erusko.feature_submit

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.OneTimeEvents
import base.base_app.core.base.vm.*
import base.base_app.core.entities.error_handler.ErrorData
import base.base_app.core.entities.error_handler.ErrorHandler
import base.base_app.core.entities.error_handler.VerificationErrorCommonType
import base.base_app.core.entities.events.EventTracker
import base.base_app.core.entities.events.Events
import base.base_app.core.features.exposure.ExposuresInteractor
import base.base_app.core.features.exposure.models.verification_server.VerifyCodeErrorEnum
import base.base_app.core.helpers.ConnectionHelper
import base.base_app.core.helpers.Logger
import base.base_app.core.helpers.StringResource
import dagger.hilt.android.qualifiers.ApplicationContext

sealed class SubmitEvents : OneTimeEvents {
    object Success : SubmitEvents()
}

class SubmitViewModel @ViewModelInject constructor(
        @ApplicationContext private val context: Context,
        private val errorHandler: ErrorHandler,
        private val exposuresInteractor: ExposuresInteractor,
        private val eventTracker: EventTracker
) : BaseViewModel<Unit>(Unit) {

    init {
        registerCustomEvent<SubmitEvents>()
    }

    fun nextClicked(verificationCode: VerificationCode, trackSubmit: Boolean = true) {
        launchOnIo {
            if (ConnectionHelper.isNetworkAvailable(context)) {
                if (trackSubmit) {
                    eventTracker.trackEvent(Events.VerificationCodeSubmitted)
                }
                kotlin.runCatching {
                    emitDefaultEvent(DefaultOneTimeEvents.Loading(true))
                    exposuresInteractor.reportExposureWithVerification(verificationCode.code)
                }.onSuccess {
                    if (verificationCode.isFromDeeplink) {
                        eventTracker.trackEvent(Events.VerificationDeeplinkCodeUsed)
                    } else {
                        eventTracker.trackEvent(Events.DiagnosisKeysSharedSuccess)
                    }
                    emitDefaultEvent(DefaultOneTimeEvents.Loading(false))
                    emitCustomEvent(SubmitEvents.Success)
                }.onFailure {
                    Logger.et { it }
                    emitDefaultEvent(DefaultOneTimeEvents.Loading(false))
                    emitDefaultEvent(DefaultOneTimeEvents.Error(mapError(it)))
                }
            } else {
                emitDefaultEvent(DefaultOneTimeEvents.Error(ErrorHandler.getInternetError()))
            }
        }
    }

    fun retryNextClicked(code: VerificationCode) {
        nextClicked(code, false)
    }

    private fun mapError(throwable: Throwable): ErrorData? {
        return errorHandler.mapError(
                throwable,
                VerificationErrorCommonType(VerifyCodeErrorEnum.CODE_EXPIRED) to { ErrorData(StringResource.StringIdResource(R.string.submit_keys_error_code_expired_title), StringResource.StringIdResource(R.string.submit_keys_error_code_expired_body)) },
                VerificationErrorCommonType(VerifyCodeErrorEnum.CODE_INVALID) to { ErrorData(StringResource.StringIdResource(R.string.submit_keys_error_code_invalid_title), StringResource.StringIdResource(R.string.submit_keys_error_code_invalid_body)) },
                VerificationErrorCommonType(VerifyCodeErrorEnum.CODE_NOT_FOUND) to { ErrorData(StringResource.StringIdResource(R.string.submit_keys_error_code_invalid_title), StringResource.StringIdResource(R.string.submit_keys_error_code_invalid_body)) },
        )
    }
}