package sk.erusko.feature_submit

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import base.base_app.core.base.BaseFragment
import base.base_app.core.base.DefaultOneTimeEvents
import base.base_app.core.base.ToolbarConfig
import base.base_app.core.base.vm.renderCustomEvents
import base.base_app.core.base.vm.renderDefaultEvents
import base.base_app.core.entities.error_handler.ErrorData
import base.base_app.core.entities.events.Events
import base.base_app.core.features.exposure.ExposureResolutionRequestHandler
import base.base_app.core.helpers.StringResource
import base.base_app.core.helpers.dialog.BottomSheetDialogBuilder
import base.base_app.core.helpers.removeAllSpaces
import base.base_app.core.helpers.show
import base.base_app.core.ui.show
import base.base_app.validator.form
import base.base_app.validator.form.Form
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_submit.*
import javax.inject.Inject

@AndroidEntryPoint
class SubmitFragment : BaseFragment() {

    @Inject
    lateinit var exposureResolutionRequestHandler: ExposureResolutionRequestHandler

    lateinit var form: Form

    private val args: SubmitFragmentArgs by navArgs()

    override fun disableScreenshots(): Boolean = true

    private val submitViewModel: SubmitViewModel by viewModels()

    override fun getStaticContentResId(): Int = R.layout.fragment_submit

    override fun getToolbarConfig(): ToolbarConfig? = ToolbarConfig(
            title = StringResource.StringIdResource(R.string.submit_keys_toolbar_title),
            imageResource = R.drawable.ic_grouping
    )

    override fun init(view: View, savedInstanceState: Bundle?) {
        form = createForm()

        activity?.activityResultRegistry?.let {
            lifecycle.addObserver(exposureResolutionRequestHandler.registerListener(it) {
                if (it.resultCode == Activity.RESULT_OK) {
                    submitViewModel.retryNextClicked(getCode())
                } else {
                    eventTracker.trackEvent(Events.ShareKeysDenied)
                }
            })
        }

        if (!args.verificationCode.isNullOrBlank()) {
            il_submit.editText?.setText(args.verificationCode)
        }
    }

    override fun render() {
        submitViewModel.renderDefaultEvents(lifecycleScope) {
            when (it) {
                is DefaultOneTimeEvents.Error -> {
                    it.error?.let { error ->
                        if (error.type == ErrorData.Type.INTERNET) {
                            getBaseActivity()?.let {
                                BottomSheetDialogBuilder(it)
                                        .setTitle(error.title)
                                        .setImageResId(base.base_app.core.R.drawable.ic_dialog_alert)
                                        .setSubtitle(error.message)
                                        .setOkButton(base.base_app.core.R.string.submit_keys_try_again) {
                                            submitViewModel.retryNextClicked(getCode())
                                        }
                                        .setCloseButton(base.base_app.core.R.string.general_close) {}
                                        .build()
                                        .show()
                            }
                        } else {
                            it.error.show(this)
                        }
                    }
                }
                is DefaultOneTimeEvents.Loading -> pb_submit.show(it.isLoading)
            }
        }
        submitViewModel.renderCustomEvents<SubmitEvents>(lifecycleScope) {
            when (it) {
                SubmitEvents.Success -> showSuccessSubmitDialog()
            }
        }
    }

    private fun showSuccessSubmitDialog() {
        getBaseActivity()?.let { activity ->
            BottomSheetDialogBuilder(activity)
                    .setImageResId(R.drawable.ic_success)
                    .setDialogDismissListener { activity.onBackPressed() }
                    .setCloseButton(R.string.general_ok) { }
                    .setTitle(R.string.submit_keys_success_title)
                    .setSubtitle(R.string.submit_keys_success_body)
                    .build()
                    .show()
        }
    }

    private fun createForm(): Form {
        return form {
            useRealTimeValidation(debounce = 300, disableSubmit = true)
            inputLayout(R.id.il_submit, name = INPUT_TAG) {
                isNotEmpty().description(R.string.submit_keys_error_code_invalid)
            }
            submitWith(R.id.btn_submit_next) {
                submitViewModel.nextClicked(getCode())
            }
        }
    }

    private fun getCode(): VerificationCode {
        val code = form.validate(true)[INPUT_TAG]?.asString().orEmpty().removeAllSpaces()
        return VerificationCode(code, code == args.verificationCode.orEmpty())
    }

    companion object {
        private const val INPUT_TAG = "input_code"
    }
}

data class VerificationCode(val code: String, val isFromDeeplink: Boolean)